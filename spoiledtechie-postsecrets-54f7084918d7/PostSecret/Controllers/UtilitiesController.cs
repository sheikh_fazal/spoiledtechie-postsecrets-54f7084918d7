﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.IO;
using PostSecret.ViewModels;

namespace PostSecret.Controllers
{
    public class UtilitiesController : Controller
    {
        public ActionResult FavoriteOnAction(string id)
        {
            if (id.Length != 36)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.PSDataContext db = new Code.PSDataContext();
            Code.PS_Favorite v = new Code.PS_Favorite();
            v.Item_Guid = new Guid(id);
            v.User_ID = SupportFramework.DataAccessGlobal.UserID();
            db.PS_Favorites.InsertOnSubmit(v);

            var getItem = (from xx in db.PS_Cards
                           where xx.uid == new Guid(id)
                           select xx).FirstOrDefault();
            if (getItem.Favorites == null)
                getItem.Favorites = 0;
            getItem.Favorites += 1;
            db.SubmitChanges();
            return Json(new { guid = id.Replace("-", ""), answer = true, type = "turnon", favoriteCount = getItem.Favorites }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FavoriteOffAction(string id)
        {
            if (id.Length != 36)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.PSDataContext db = new Code.PSDataContext();
            var getFav = (from xx in db.PS_Favorites
                          where xx.User_ID == SupportFramework.DataAccessGlobal.UserID()
                          select xx).FirstOrDefault();
            db.PS_Favorites.DeleteOnSubmit(getFav);

            var getItem = (from xx in db.PS_Cards
                           where xx.uid == new Guid(id)
                           select xx).FirstOrDefault();
            if (getItem.Favorites == null)
                getItem.Favorites = 0;
            getItem.Favorites -= 1;
            db.SubmitChanges();
            return Json(new { guid = id.Replace("-", ""), answer = true, type = "turnoff", favoriteCount = getItem.Favorites }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddContentSurvey(string id, string content)
        {
            Code.PSDataContext db = new Code.PSDataContext();
            Code.PS_Comment c = new Code.PS_Comment();
            c.Comment = content;
            c.Comment_Html = content;
            c.Content_To_Id = new Guid(id);
            c.DateTime_Submitted = DateTime.UtcNow;
            c.uid = Guid.NewGuid();
            c.User_ID = SupportFramework.DataAccessGlobal.UserID();
            db.PS_Comments.InsertOnSubmit(c);

            var getContents = (from xx in db.PS_Comments
                               where xx.Content_To_Id == new Guid(id)
                               select xx).ToList();

            if (getContents != null && getContents.Count > 0)
            {
                int votes = 0;
                for (int i = 0; i < getContents.Count; i++)
                    if (getContents[i].Votes_Up>votes)
                        votes = getContents[i].Votes_Up;
                if (votes == 0)
                {
                    var getCard = (from xx in db.PS_Cards
                                   where xx.uid == new Guid(id)
                                   select xx).FirstOrDefault();
                    getCard.Title = content;
                    getCard.Content_Info = content;
                }
            }
            else
            {
                var getCard = (from xx in db.PS_Cards
                               where xx.uid == new Guid(id)
                               select xx).FirstOrDefault();
                getCard.Title = content;
                getCard.Content_Info = content;
            }
            db.SubmitChanges();
            return Json(new { guid = id.Replace("-", ""), answer = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddComment(string id, string comment)
        {
            Code.PSDataContext db = new Code.PSDataContext();
            Code.PS_Comment c = new Code.PS_Comment();
            c.Comment = comment;
            c.Comment_Html = comment;
            c.Comment_To_ID = new Guid(id);
            c.DateTime_Submitted = DateTime.UtcNow;
            c.uid = Guid.NewGuid();
            c.User_ID = SupportFramework.DataAccessGlobal.UserID();
            db.PS_Comments.InsertOnSubmit(c);
            db.SubmitChanges();
            return Json(new { guid = id.Replace("-", ""), answer = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateViewCount(string id)
        {
            Code.PSDataContext db = new Code.PSDataContext();
            var update = (from xx in db.PS_Cards
                          where xx.uid == new Guid(id)
                          select xx).FirstOrDefault();
            if (update.Views == null)
                update.Views = 0;
            update.Views += 1;
            db.SubmitChanges();
            return Json(new { guid = id.Replace("-", ""), answer = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VoteDown(string id)
        {
            if (id.Length != 36)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.PSDataContext db = new Code.PSDataContext();
            var getVotes = (from xx in db.PS_Votes
                            where xx.Item_Guid == new Guid(id)
                            select xx).ToList();
            int DirtyCheckBit = 0;
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                foreach (var item in getVotes)
                    if (item.User_ID == SupportFramework.DataAccessGlobal.UserID())
                        DirtyCheckBit = 1;
            }
            else
            {
                foreach (var item in getVotes)
                    if (item.User_IP_Address == Boomers.Utilities.Web.Network.IPAddress)
                        DirtyCheckBit = 1;
            }
            if (DirtyCheckBit == 1)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.PS_Vote v = new Code.PS_Vote();
            v.DateTime_Submitted = DateTime.UtcNow;
            v.Item_Guid = new Guid(id);
            v.Vote = -1;
            if (HttpContext.User.Identity.IsAuthenticated)
                v.User_ID = SupportFramework.DataAccessGlobal.UserID();

            v.User_IP_Address = Boomers.Utilities.Web.Network.IPAddress;
            db.PS_Votes.InsertOnSubmit(v);

            var getItem = (from xx in db.PS_Cards
                           where xx.uid == new Guid(id)
                           select xx).FirstOrDefault();
            getItem.Votes_Down += 1;

            db.SubmitChanges();
            return Json(new { guid = id.Replace("-", ""), answer = true, voteCount = getItem.Votes_Down }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VoteUp(string id)
        {
            if (id.Length != 36)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.PSDataContext db = new Code.PSDataContext();
            var getVotes = (from xx in db.PS_Votes
                            where xx.Item_Guid == new Guid(id)
                            select xx).ToList();
            int DirtyCheckBit = 0;
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                foreach (var item in getVotes)
                    if (item.User_ID == SupportFramework.DataAccessGlobal.UserID())
                        DirtyCheckBit = 1;
            }
            else
            {
                foreach (var item in getVotes)
                    if (item.User_IP_Address == Boomers.Utilities.Web.Network.IPAddress)
                        DirtyCheckBit = 1;
            }
            if (DirtyCheckBit == 1)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.PS_Vote v = new Code.PS_Vote();
            v.DateTime_Submitted = DateTime.UtcNow;
            v.Item_Guid = new Guid(id);
            v.Vote = 1;
            if (HttpContext.User.Identity.IsAuthenticated)
                v.User_ID = SupportFramework.DataAccessGlobal.UserID();

            v.User_IP_Address = Boomers.Utilities.Web.Network.IPAddress;
            db.PS_Votes.InsertOnSubmit(v);

            var getItem = (from xx in db.PS_Cards
                           where xx.uid == new Guid(id)
                           select xx).FirstOrDefault();
            getItem.Votes_Up += 1;
            try
            {
                db.SubmitChanges();
            }
            catch { }
            return Json(new { guid = id, answer = true, voteCount = getItem.Votes_Up }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult VoteUpContent(string id)
        {
            if (id.Length != 36)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.PSDataContext db = new Code.PSDataContext();
            var getVotes = (from xx in db.PS_Votes
                            where xx.Item_Guid == new Guid(id)
                            select xx).ToList();
            int DirtyCheckBit = 0;
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                foreach (var item in getVotes)
                    if (item.User_ID == SupportFramework.DataAccessGlobal.UserID())
                        DirtyCheckBit = 1;
            }
            else
            {
                foreach (var item in getVotes)
                    if (item.User_IP_Address == Boomers.Utilities.Web.Network.IPAddress)
                        DirtyCheckBit = 1;
            }
            if (DirtyCheckBit == 1)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.PS_Vote v = new Code.PS_Vote();
            v.DateTime_Submitted = DateTime.UtcNow;
            v.Item_Guid = new Guid(id);
            v.Vote = 1;
            if (HttpContext.User.Identity.IsAuthenticated)
                v.User_ID = SupportFramework.DataAccessGlobal.UserID();

            v.User_IP_Address = Boomers.Utilities.Web.Network.IPAddress;
            db.PS_Votes.InsertOnSubmit(v);

            var getItem = (from xx in db.PS_Comments
                           where xx.uid == new Guid(id)
                           select xx).FirstOrDefault();
            getItem.Votes_Up += 1;

            var getContents = (from xx in db.PS_Comments
                               where xx.Content_To_Id == getItem.Content_To_Id
                               select xx).ToList();

            if (getContents != null && getContents.Count > 0)
            {
                int votes = 0;
                for (int i = 0; i < getContents.Count; i++)
                    if (getContents[i].Votes_Up>votes )
                        votes = getContents[i].Votes_Up;
                if (getItem.Votes_Up >= votes)
                {
                    var getCard = (from xx in db.PS_Cards
                                   where xx.uid == getItem.Content_To_Id
                                   select xx).FirstOrDefault();
                    getCard.Title = getItem.Comment_Html;
                    getCard.Content_Info = getItem.Comment_Html;
                }
            }
            else
            {
                var getCard = (from xx in db.PS_Cards
                               where xx.uid == getItem.Content_To_Id
                               select xx).FirstOrDefault();
                getCard.Title = getItem.Comment_Html;
                getCard.Content_Info = getItem.Comment_Html;
            }
            
            try
            {
                db.SubmitChanges();
            }
            catch { }
            return Json(new { guid = id, answer = true, voteCount = getItem.Votes_Up }, JsonRequestBehavior.AllowGet);
        }

        public static List<CardViewModel> searchTags(string searchText)
        {
            Code.PSDataContext db = new Code.PSDataContext();
            searchText = HttpUtility.UrlDecode(searchText);

            var results = (from tgs in db.PS_Tags_Pulls
                           from tgl in db.PS_Tags
                           from dfg in db.PS_Cards
                           where (tgs.uid == tgl.Tag_ID)
                           where (dfg.uid == tgl.Item_ID)
                           where (tgs.Tag_Name.Contains(searchText) || dfg.Title.Contains(searchText))
                           select new CardViewModel { Title = dfg.Title, uid = dfg.uid }).ToList();

            if (searchText.Contains(" "))
            {
                string[] searchwords = searchText.Split(' ');

                for (int x = 0; x < searchwords.Length; x++)
                {
                    results.AddRange((from tgs in db.PS_Tags_Pulls
                                      from tgl in db.PS_Tags
                                      from dfg in db.PS_Cards
                                      where (tgs.uid == tgl.Tag_ID)
                                      where (dfg.uid == tgl.Item_ID)
                                      where (tgs.Tag_Name.Contains(searchwords[x]) || dfg.Title.Contains(searchwords[x]))
                                      select new CardViewModel { Title = dfg.Title, uid = dfg.uid }).ToList());
                }
            }

            return results.Distinct().ToList();
        }
        public ActionResult Search(string q)
        {
            return Content(string.Join("\n", searchTags(q).Select(x => x.Title)));
        }

        /// <summary>
        /// This action method looks up the tags.
        /// </summary>
        /// <param name="q">The query that contains the user input.</param>
        /// <param name="limit">The number of tags return.</param>
        public ActionResult LookupTags(string q, int limit)
        {
            // We can get a list of tags from the database, but 
            // for this example, I will populate a list with values.
            Code.PSDataContext db = new Code.PSDataContext();
            var getTags = (from xx in db.PS_Tags_Pulls
                           where xx.Tag_Name.StartsWith(q)
                           orderby xx.Tag_Name
                           select xx.Tag_Name).Take(limit);
            //return Json(getTags, JsonRequestBehavior.AllowGet);
            return Content(string.Join("\n", getTags));
        }
        public ActionResult AddNodeToSiteMap(string url, bool modified)
        {
            if (!url.Contains("?name=") && !url.Contains("/admin") && !url.Contains("/Orders") && !url.Contains("/IPN") && !url.Contains("/Error?"))
                SupportFramework.HttpExtensions.SiteMap.AddNode(url, modified);

            return Json(new { answer = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReportAction(string id)
        {
            if (id.Length != 36)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);
            Code.SupportFramework.GlobalDataContext db = new Code.SupportFramework.GlobalDataContext();
            Code.SupportFramework.Global_Flagged r = new Code.SupportFramework.Global_Flagged();
            r.DateTime_Flagged = DateTime.UtcNow;
            r.Application_Id = SupportFramework.DataAccessGlobal.ApplicationId();
            r.Flagged_Id = new Guid(id);
            r.IP_Address = Boomers.Utilities.Web.Network.IPAddress;
            if (HttpContext.User.Identity.IsAuthenticated)
                r.User_ID = SupportFramework.DataAccessGlobal.UserID();
            db.Global_Flaggeds.InsertOnSubmit(r);
            db.SubmitChanges();
            return Json(new { guid = id.Replace("-", ""), answer = true }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReportDataAction(string id, string data)
        {
            if (id.Length != 36)
                return Json(new { guid = id.Replace("-", ""), answer = false }, JsonRequestBehavior.AllowGet);

            Code.SupportFramework.GlobalDataContext db = new Code.SupportFramework.GlobalDataContext();
            Code.SupportFramework.Global_Flagged r = new Code.SupportFramework.Global_Flagged();
            r.DateTime_Flagged = DateTime.UtcNow;
            r.Application_Id = SupportFramework.DataAccessGlobal.ApplicationId();
            r.Flagged_Id = new Guid(id);
            r.IP_Address = Boomers.Utilities.Web.Network.IPAddress;
            if (HttpContext.User.Identity.IsAuthenticated)
                r.User_ID = SupportFramework.DataAccessGlobal.UserID();
            r.Data = data;
            r.Type = data;
            db.Global_Flaggeds.InsertOnSubmit(r);
            db.SubmitChanges();

            return Json(new { guid = id.Replace("-", ""), answer = true }, JsonRequestBehavior.AllowGet);
        }

    }
}
