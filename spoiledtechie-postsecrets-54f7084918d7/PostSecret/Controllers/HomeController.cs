﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Web;
using System.Web.Mvc;
using PostSecret.ViewModels;
using Boomers.Utilities.Guids;
using Boomers.Utilities.Text;
using Boomers.Utilities.MVCHelpers.Paging;
using Boomers.Utilities.Communications;
using System.ServiceModel.Syndication;
using System.Xml.Linq;
using Newtonsoft.Json; //added by ahmad

namespace PostSecret.Controllers
{
    public class HomeController : Controller
    {
        //public ActionResult secretRun()
        //{
        //    CardViewModel cards = new CardViewModel();
        //    cards.RenameAllNonPostSecretNamedCards();
        //    return View();
        //}
        Regex rgxCards = new Regex(@"http[a-z:/0-9\._A-Z-]*(jpg|jpeg)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        public void DownloadNewSecrets123France()
        {
            XmlReader reader = XmlReader.Create("http://feeds.feedburner.com/Postsecretfrance");
            SyndicationFeed feed = SyndicationFeed.Load(reader);

            var franceQuery = (from item in feed.Items
                               where item.PublishDate > DateTime.Parse("2011-01-11")
                               select new
                               {
                                   item.Id,
                                   item.Title.Text,
                                   item.PublishDate,
                                   item.Summary,
                                   item.Content
                               }).ToList();
            Code.PSDataContext db = new Code.PSDataContext();

            foreach (var item in franceQuery)
            {
                var checkID = (from xx in db.PS_Rss_Downloadeds
                               where xx.Id == item.Id
                               select xx).FirstOrDefault();
                if (checkID == null)
                {
                    Code.PS_Rss_Downloaded rss = new Code.PS_Rss_Downloaded();
                    rss.Id = item.Id;
                    rss.TimeStamp = DateTime.UtcNow;
                    rss.Url = "http://feeds.feedburner.com/Postsecretfrance";
                    db.PS_Rss_Downloadeds.InsertOnSubmit(rss);


                    //StreamReader sr = new StreamReader(@"c:\temp\spain.txt");
                    //string text = sr.ReadToEnd();
                    //Code.PSDataContext db = new Code.PSDataContext();
                    //Regex rgxCards = new Regex(@"http[a-z:/0-9\._A-Z-]*(jpg|jpeg)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

                    DateTime dt = DateTime.UtcNow;
                    string fileSave = @"C:\HostingSpaces\pio.scott@gmail.com\images.codingforcharity.org\wwwroot\ps\france\" + dt.Year + @"\" + dt.Month + @"\" + dt.Day + @"\";
                    if (!Directory.Exists(fileSave))
                        Directory.CreateDirectory(fileSave);
                    string urlPath = "http://images.codingforcharity.org/ps/france/" + dt.Year + "/" + dt.Month + "/" + dt.Day + "/";

                    var matches = new List<string>();
                    if (item.Content != null)
                        foreach (Match match in rgxCards.Matches((item.Content as TextSyndicationContent).Text))
                            matches.Add(match.Value);
                    if (item.Summary != null)
                        foreach (Match match in rgxCards.Matches((item.Summary as TextSyndicationContent).Text))
                            matches.Add(match.Value);

                    foreach (string match in matches.Distinct())
                    {
                        var checkUrl = (from xx in db.PS_Postcards
                                        where xx.PostCard_Url == match
                                        select xx).FirstOrDefault();
                        if (checkUrl == null)
                        {
                            try
                            {
                                string dt1 = "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssFFF");

                                WebClient pwc = new WebClient();
                                pwc.DownloadFile(match, fileSave + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match));

                                Code.PS_Postcard pc = new Code.PS_Postcard();
                                pc.DateTime_Added = DateTime.UtcNow;
                                pc.DateTime_From = dt;
                                pc.PostCard_Url = match;
                                pc.PostCard_UrlNew = urlPath + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match);
                                pc.Save_Location = fileSave + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match);
                                pc.uid = Guid.NewGuid();

                                db.PS_Postcards.InsertOnSubmit(pc);

                                Code.PS_Card c = new Code.PS_Card();
                                c.uid = Guid.NewGuid();
                                c.Card_Id = pc.uid;
                                c.DateTime_Added = dt;

                                Random r = new Random();
                                c.Views = r.Next(100, 500);
                                c.Votes_Up = r.Next(50, c.Views);
                                c.Favorites = r.Next(25, c.Views);

                                db.PS_Cards.InsertOnSubmit(c);
                            }
                            catch { }

                            db.SubmitChanges();
                        }
                    }

                    //Email.EmailPassword = "cheetah1";
                    //Email.EmailUsername = "spoiledtechie@gmail.com";
                    //Email.FromEmail = "spoiledtechie@gmail.com";
                    //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "France's Postcards Downloaded", matches.Count + " postcards have been downloaded for this Sunday.", "PostSecretCollection.com");
                }
            }

        }
        public void DownloadNewSecrets123UK()
        {
            XmlReader readeruk = XmlReader.Create("http://postsecret-uk.blogspot.com/feeds/posts/default");
            SyndicationFeed feeduk = SyndicationFeed.Load(readeruk);

            var ukQuery = (from item in feeduk.Items
                           where item.PublishDate > DateTime.Parse("2011-01-11")
                           select new
                           {
                               item.Id,
                               item.Title.Text,
                               item.PublishDate,
                               item.Summary,
                               item.Content
                           }).ToList();
            Code.PSDataContext db = new Code.PSDataContext();

            Regex rgxCards = new Regex(@"http[a-z:/0-9\._A-Z-]*(jpg|jpeg)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            DateTime dt = DateTime.UtcNow;
            string fileSave = @"C:\HostingSpaces\pio.scott@gmail.com\images.codingforcharity.org\wwwroot\ps\uk\" + dt.Year + @"\" + dt.Month + @"\" + dt.Day + @"\";

            foreach (var ukF in ukQuery)
            {
                var checkID = (from xx in db.PS_Rss_Downloadeds
                               where xx.Id == ukF.Id
                               select xx).FirstOrDefault();
                if (checkID == null)
                {

                    Code.PS_Rss_Downloaded rss = new Code.PS_Rss_Downloaded();
                    rss.Id = ukF.Id;
                    rss.TimeStamp = DateTime.UtcNow;
                    rss.Url = "http://postsecret-uk.blogspot.com/feeds/posts/default";
                    db.PS_Rss_Downloadeds.InsertOnSubmit(rss);


                    if (!Directory.Exists(fileSave))
                        Directory.CreateDirectory(fileSave);
                    string urlPath = "http://images.codingforcharity.org/ps/uk/" + dt.Year + "/" + dt.Month + "/" + dt.Day + "/";

                    var matches = new List<string>();

                    if (ukF.Content != null)
                        foreach (Match match in rgxCards.Matches((ukF.Content as TextSyndicationContent).Text))
                            matches.Add(match.Value);
                    if (ukF.Summary != null)
                        foreach (Match match in rgxCards.Matches((ukF.Summary as TextSyndicationContent).Text))
                            matches.Add(match.Value);


                    foreach (string match in matches.Distinct())
                    {
                        var checkUrl = (from xx in db.PS_Postcards
                                        where xx.PostCard_Url == match
                                        select xx).FirstOrDefault();
                        if (checkUrl == null)
                        {
                            try
                            {
                                string dt1 = "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssFFF");

                                WebClient pwc = new WebClient();
                                pwc.DownloadFile(match, fileSave + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match));

                                Code.PS_Postcard pc = new Code.PS_Postcard();
                                pc.DateTime_Added = DateTime.UtcNow;
                                pc.DateTime_From = dt;
                                pc.PostCard_Url = match;
                                pc.PostCard_UrlNew = urlPath + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match);
                                pc.Save_Location = fileSave + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match);
                                pc.uid = Guid.NewGuid();
                                db.PS_Postcards.InsertOnSubmit(pc);

                                Code.PS_Card c = new Code.PS_Card();
                                c.uid = Guid.NewGuid();
                                c.Card_Id = pc.uid;
                                c.DateTime_Added = dt;
                                db.PS_Cards.InsertOnSubmit(c);
                            }
                            catch { }

                            db.SubmitChanges();
                        }
                    }

                    //Email.EmailPassword = "cheetah1";
                    //Email.EmailUsername = "spoiledtechie@gmail.com";
                    //Email.FromEmail = "spoiledtechie@gmail.com";
                    //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "Uks Postcards Downloaded", matches.Count + " postcards have been downloaded for this Sunday.", "PostSecretCollection.com");
                }
            }
        }
        public void DownloadNewSecrets123Germany()
        {
            XmlReader reader = XmlReader.Create("http://postsecretdeutsch.blogspot.com/feeds/posts/default");
            SyndicationFeed feed = SyndicationFeed.Load(reader);

            var germanyQuery = (from item in feed.Items
                                where item.PublishDate > DateTime.Parse("2011-01-11")
                                select new
                                {
                                    item.Id,
                                    item.Title.Text,
                                    item.PublishDate,
                                    item.Summary,
                                    item.Content
                                }).ToList();
            Code.PSDataContext db = new Code.PSDataContext();

            foreach (var item in germanyQuery)
            {
                var checkID = (from xx in db.PS_Rss_Downloadeds
                               where xx.Id == item.Id
                               select xx).FirstOrDefault();
                if (checkID == null)
                {

                    Code.PS_Rss_Downloaded rss = new Code.PS_Rss_Downloaded();
                    rss.Id = item.Id;
                    rss.TimeStamp = DateTime.UtcNow;
                    rss.Url = "http://postsecretdeutsch.blogspot.com/feeds/posts/default";
                    db.PS_Rss_Downloadeds.InsertOnSubmit(rss);

                    //StreamReader sr = new StreamReader(@"c:\temp\germany.txt");
                    //string text = sr.ReadToEnd();

                    DateTime dt = DateTime.UtcNow;
                    string fileSave = @"C:\HostingSpaces\pio.scott@gmail.com\images.codingforcharity.org\wwwroot\ps\germany\" + dt.Year + @"\" + dt.Month + @"\" + dt.Day + @"\";
                    if (!Directory.Exists(fileSave))
                        Directory.CreateDirectory(fileSave);
                    string urlPath = "http://images.codingforcharity.org/ps/germany/" + dt.Year + "/" + dt.Month + "/" + dt.Day + "/";

                    var matches = new List<string>();

                    if (item.Content != null)
                        foreach (Match match in rgxCards.Matches((item.Content as TextSyndicationContent).Text))
                            matches.Add(match.Value);
                    if (item.Summary != null)
                        foreach (Match match in rgxCards.Matches((item.Summary as TextSyndicationContent).Text))
                            matches.Add(match.Value);

                    foreach (string match in matches.Distinct())
                    {
                        var checkUrl = (from xx in db.PS_Postcards
                                        where xx.PostCard_Url == match
                                        select xx).FirstOrDefault();
                        if (checkUrl == null)
                        {
                            try
                            {
                                string dt1 = "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssFFF");

                                WebClient pwc = new WebClient();
                                pwc.DownloadFile(match, fileSave + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match));

                                Code.PS_Postcard pc = new Code.PS_Postcard();
                                pc.DateTime_Added = DateTime.UtcNow;
                                pc.DateTime_From = dt;
                                pc.PostCard_Url = match;
                                pc.PostCard_UrlNew = urlPath + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match);
                                pc.Save_Location = fileSave + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match);
                                pc.uid = Guid.NewGuid();
                                db.PS_Postcards.InsertOnSubmit(pc);

                                Code.PS_Card c = new Code.PS_Card();
                                c.uid = Guid.NewGuid();
                                c.Card_Id = pc.uid;
                                c.DateTime_Added = dt;
                                db.PS_Cards.InsertOnSubmit(c);
                            }
                            catch { }

                            db.SubmitChanges();
                        }
                    }
                    //Email.EmailPassword = "cheetah1";
                    //Email.EmailUsername = "spoiledtechie@gmail.com";
                    //Email.FromEmail = "spoiledtechie@gmail.com";
                    //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "Germanys Postcards Downloaded", matches.Count + " postcards have been downloaded for this Sunday.", "PostSecretCollection.com");
                }
            }
        }
        public ActionResult DownloadNewSecrets123USA()
        {
            XmlReader reader = XmlReader.Create("http://www.postsecret.com/feeds/posts/default?alt=rss");
            SyndicationFeed feed = SyndicationFeed.Load(reader);



            var usaQuery = (from item in feed.Items
                            //where item.PublishDate > DateTime.Parse("2011-01-11")
                            select new
                            {
                                item.Id,
                                item.Title.Text,
                                item.PublishDate,
                                item.Summary,
                                item.Content,
                                item.ElementExtensions
                            }).ToList();
            Code.PSDataContext db = new Code.PSDataContext();

            foreach (var item in usaQuery)
            {
                var checkID = (from xx in db.PS_Rss_Downloadeds
                               where xx.Id == item.Id
                               select xx).FirstOrDefault();
                if (checkID == null)
                {

                    Code.PS_Rss_Downloaded rss = new Code.PS_Rss_Downloaded();
                    rss.Id = item.Id;
                    rss.TimeStamp = DateTime.UtcNow;
                    rss.Url = "http://www.postsecret.com/feeds/posts/default?alt=rss";
                    db.PS_Rss_Downloadeds.InsertOnSubmit(rss);

                    DateTime dt = DateTime.UtcNow;
                    string fileSave = @"C:\HostingSpaces\pio.scott@gmail.com\images.codingforcharity.org\wwwroot\ps\usa\" + dt.Year + @"\" + dt.Month + @"\" + dt.Day + @"\";
                    if (!Directory.Exists(fileSave))
                        Directory.CreateDirectory(fileSave);
                    string urlPath = "http://images.codingforcharity.org/ps/usa/" + dt.Year + "/" + dt.Month + "/" + dt.Day + "/";

                    var matches = new List<string>();

                    string content = "";
                    foreach (SyndicationElementExtension ext in item.ElementExtensions)
                    {
                        if (ext.GetObject<XElement>().Name.LocalName == "encoded")
                            content = ext.GetObject<XElement>().Value;
                    }

                    if (item.Content != null)
                        foreach (Match match in rgxCards.Matches((item.Content as TextSyndicationContent).Text))
                            matches.Add(match.Value);
                    if (item.Summary != null)
                        foreach (Match match in rgxCards.Matches((item.Summary as TextSyndicationContent).Text))
                            matches.Add(match.Value);
                    if (!String.IsNullOrEmpty(content))
                        foreach (Match match in rgxCards.Matches(content))
                            matches.Add(match.Value);

                    foreach (string match in matches.Distinct())
                    {
                        var checkUrl = (from xx in db.PS_Postcards
                                        where xx.PostCard_Url == match
                                        select xx).FirstOrDefault();
                        if (checkUrl == null)
                        {
                            try
                            {
                                string dt1 = "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssFFF");

                                WebClient pwc = new WebClient();
                                pwc.DownloadFile(match, fileSave + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match));

                                Code.PS_Postcard pc = new Code.PS_Postcard();
                                pc.DateTime_Added = DateTime.UtcNow;
                                pc.DateTime_From = dt;
                                pc.PostCard_Url = match;
                                pc.PostCard_UrlNew = urlPath + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match);
                                pc.Save_Location = fileSave + "postsecret_postcard_" + Path.GetFileNameWithoutExtension(match) + dt1 + Path.GetExtension(match);
                                pc.uid = Guid.NewGuid();
                                db.PS_Postcards.InsertOnSubmit(pc);

                                Code.PS_Card c = new Code.PS_Card();
                                c.uid = Guid.NewGuid();
                                c.Card_Id = pc.uid;
                                c.DateTime_Added = dt;
                                db.PS_Cards.InsertOnSubmit(c);
                            }
                            catch { }

                            db.SubmitChanges();
                        }
                    }

                    //Email.EmailPassword = "cheetah1";
                    //Email.EmailUsername = "spoiledtechie@gmail.com";
                    //Email.FromEmail = "spoiledtechie@gmail.com";
                    //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "USA Postcards Downloaded", matches.Count + " postcards have been downloaded for this Sunday.", "PostSecretCollection.com");
                }
            }

            DownloadNewSecrets123UK();
            DownloadNewSecrets123Germany();
            DownloadNewSecrets123France();
            return View();
        }

        public ActionResult Error()
        {
            return View("Error");
        }
        public ActionResult Random()
        {
            Code.PSDataContext db = new Code.PSDataContext();

            var co = (from xx in db.PS_Cards
                      select xx).Count();
            var poster = (from xx in db.PS_Cards
                          select new CardViewModel
                          {
                              uid = xx.uid,
                              Title = xx.Title,

                          }).Skip(new Random().Next(co)).FirstOrDefault();

            return Redirect(Url.Content("~/PostCards/" + poster.uid.RemoveDashes() + "/" + poster.Title.ToSearchEngineFriendly()));
        }
        public ActionResult SearchHidden(string tbSearch)
        {
            return RedirectToAction("Search", new { q = tbSearch.ToSearchFriendly() });
        }
        public ActionResult Search(string q, int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            List<Guid> found = UtilitiesController.searchTags(q).Select(x => x.uid).ToList();
            q = q.ToLower();

            ViewData["Title"] = "PostSecretCollection.com | All PostCards searched with '" + q + "'";
            ViewData["PageTitle"] = "All PostCards searched with '" + q + "'";

            Code.PSDataContext db = new Code.PSDataContext();
            var getTags = (from xx in db.PS_Tags
                           from zz in db.PS_Tags_Pulls
                           where xx.Tag_ID == zz.uid
                           select new TagViewModel
                           {
                               TagName = zz.Tag_Name,
                               TagID = xx.Tag_ID,
                               TagCount = (from yy in db.PS_Tags
                                           where yy.Tag_ID == xx.Tag_ID
                                           select yy.Tag_ID).Count()
                           }).Distinct().Take(20).ToList();

            var getTitles = (from xx in db.PS_Cards
                             from aa in db.PS_Postcards
                             orderby xx.orderby_id
                             where xx.Card_Id == aa.uid
                             select new CardViewModel
                             {
                                 uid = xx.uid,
                                 FavoriteCount = xx.Favorites,
                                 Favorites = (from nn in db.PS_Favorites
                                              where nn.Item_Guid == xx.uid
                                              select new FavoriteViewModel
                                              {
                                                  Item_Guid = nn.Item_Guid,
                                                  uid = nn.uid,
                                                  User_Id = nn.User_ID
                                              }).ToList(),
                                 Title = xx.Title,
                                 Url = aa.PostCard_UrlNew,
                                 NickName = xx.NickName,
                                 PostCard_Id = xx.Card_Id,
                                 UserId = xx.User_ID,
                                 IPAddress = xx.IP_Address,
                                 DateTimeAdded = (DateTime)xx.DateTime_Added,
                                 VotesUp = xx.Votes_Up,
                                 VotesDown = xx.Votes_Down,
                                 Views = xx.Views,
                                 Votes = (from nn in db.PS_Votes
                                          where nn.Item_Guid == xx.uid
                                          select new VoteViewModel
                                          {
                                              IPAddress = nn.User_IP_Address,
                                              Vote = nn.Vote,
                                              ItemGuid = nn.Item_Guid,
                                              UserID = nn.User_ID,
                                          }).ToList(),
                                 Tags = (from zz in db.PS_Tags
                                         from cc in db.PS_Tags_Pulls
                                         where zz.Tag_ID == cc.uid
                                         where zz.Item_ID == xx.uid
                                         select new TagViewModel
                                         {
                                             TagName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cc.Tag_Name).ToSearchEngineFriendly(),
                                             TagID = zz.Tag_ID,
                                             TagCount = (from yy in db.PS_Tags
                                                         where yy.Tag_ID == zz.Tag_ID
                                                         select yy.Tag_ID).Count()
                                         }).Distinct().ToList(),
                                 CommentCount = (from cc in db.PS_Comments
                                                 where cc.Comment_To_ID == xx.uid
                                                 select cc).Count(),
                             }).ToPagedList(currentPageIndex, defaultPageSize);

            ViewData["Tags"] = getTags;
            ViewData["Titles"] = getTitles;

            return View(ViewData);
        }

        //
        // GET: /Home/
        public ActionResult FAQ()
        {
            return View();
        }

        //public ActionResult PrivateTest()
        //{
        //    CardViewModel cardViewModel = new CardViewModel();
        //    cardViewModel.RenameAllNonPostSecretNamedCards();
        //    return View();
        //}

        private const int defaultPageSize = 30;
        private IList<CardViewModel> allTitles = new List<CardViewModel>();
        public ActionResult Index(int? page)
        {


            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            Code.PSDataContext db = new Code.PSDataContext();
            var getTags = (from xx in db.PS_Tags
                           from zz in db.PS_Tags_Pulls
                           where xx.Tag_ID == zz.uid
                           select new TagViewModel
                           {
                               TagName = zz.Tag_Name,
                               TagID = xx.Tag_ID,
                               TagCount = (from yy in db.PS_Tags
                                           where yy.Tag_ID == xx.Tag_ID
                                           select yy.Tag_ID).Count()
                           }).Distinct().Take(20).ToList();

            var getTitles = (from xx in db.PS_Cards
                             from aa in db.PS_Postcards
                             orderby xx.orderby_id descending
                             where xx.Card_Id == aa.uid
                             select new CardViewModel
                             {
                                 uid = xx.uid,
                                 FavoriteCount = xx.Favorites,
                                 Favorites = (from nn in db.PS_Favorites
                                              where nn.Item_Guid == xx.uid
                                              select new FavoriteViewModel
                                              {
                                                  Item_Guid = nn.Item_Guid,
                                                  uid = nn.uid,
                                                  User_Id = nn.User_ID,

                                              }).ToList(),
                                 Title = xx.Title,
                                 Url = aa.PostCard_UrlNew,
                                 NickName = xx.NickName,
                                 PostCard_Id = xx.Card_Id,
                                 UserId = xx.User_ID,
                                 IPAddress = xx.IP_Address,
                                 DateTimeAdded = (DateTime)xx.DateTime_Added,
                                 VotesUp = xx.Votes_Up,
                                 VotesDown = xx.Votes_Down,
                                 Views = xx.Views,
                                 Votes = (from rr in db.PS_Votes
                                          where rr.Item_Guid == xx.uid
                                          select new VoteViewModel
                                          {
                                              IPAddress = rr.User_IP_Address,
                                              ItemGuid = rr.Item_Guid,
                                              UserID = rr.User_ID,
                                              Vote = rr.Vote
                                          }).ToList(),
                                 Tags = (from zz in db.PS_Tags
                                         from cc in db.PS_Tags_Pulls
                                         where zz.Tag_ID == cc.uid
                                         where zz.Item_ID == xx.uid
                                         select new TagViewModel
                                         {
                                             TagName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cc.Tag_Name).ToSearchEngineFriendly(),
                                             TagID = zz.Tag_ID,
                                             TagCount = (from yy in db.PS_Tags
                                                         where yy.Tag_ID == zz.Tag_ID
                                                         select yy.Tag_ID).Count()
                                         }).Distinct().ToList(),
                                 CommentCount = (from cc in db.PS_Comments
                                                 where cc.Comment_To_ID == xx.uid
                                                 select cc).Count(),
                             }).ToPagedList(currentPageIndex, defaultPageSize);

            ViewData["Titles"] = getTitles;
            ViewData["Tags"] = getTags;
            return View(ViewData);
        }
        public ActionResult SiteMap()
        {
            using (TextWriter textWriter = new StreamWriter(HttpContext.Response.OutputStream, System.Text.Encoding.UTF8))
            {
                XmlTextWriter writer = new XmlTextWriter(textWriter);
                writer.Formatting = System.Xml.Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("urlset");
                writer.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                writer.WriteAttributeString("xsi:schemaLocation", "http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");
                writer.WriteAttributeString("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");


                PostSecret.Code.SupportFramework.GlobalDataContext db = new PostSecret.Code.SupportFramework.GlobalDataContext();
                var getItem = (from xx in db.Global_Sitemaps
                               where xx.Application_ID == SupportFramework.DataAccessGlobal.ApplicationId()
                               select xx).ToList();

                foreach (var item in getItem)
                {
                    writer.WriteStartElement("url");
                    writer.WriteElementString("loc", item.URL);
                    writer.WriteElementString("changefreq", item.Change_Frequency);
                    writer.WriteElementString("lastmod", item.Last_Modified.ToString("yyyy-MM-dd"));
                    writer.WriteEndElement(); // url
                }
                writer.WriteEndElement(); // urlset
                return Content("");
            }
        }
        public ActionResult RSS()
        {
            using (TextWriter textWriter = new StreamWriter(HttpContext.Response.OutputStream, System.Text.Encoding.UTF8))
            {
                XmlTextWriter writer = new XmlTextWriter(textWriter);
                writer.Formatting = System.Xml.Formatting.Indented;
                writer.WriteStartDocument();
                writer.WriteStartElement("rss");
                writer.WriteAttributeString("version", "2.0");
                writer.WriteStartElement("channel");
                writer.WriteElementString("title", "PostSecretCollection.com PostSecret.com's Archived Collection");
                writer.WriteElementString("link", "http://PostSecretCollection.com");
                writer.WriteElementString("description", "PostCards of PostSecret.com's Archived Collection");
                writer.WriteElementString("language", "en-US");

                Code.PSDataContext db = new Code.PSDataContext();
                var getItems = (from xx in db.PS_Cards
                                orderby xx.DateTime_Added descending
                                select new CardViewModel
                                {
                                    Title = xx.Title,
                                    uid = xx.uid,
                                    DateTimeAdded = xx.DateTime_Added,
                                }).ToList();
                foreach (var item in getItems)
                {
                    writer.WriteStartElement("item");
                    writer.WriteElementString("title", item.Title);
                    writer.WriteElementString("link", "http://PostSecretCollection.com/PostCards/" + item.uid.RemoveDashes());
                    writer.WriteElementString("description", item.Title);
                    writer.WriteElementString("pubDate", item.DateTimeAdded.ToString());
                    writer.WriteElementString("guid", "http://PostSecretCollection.com/PostCards/" + item.uid.RemoveDashes());
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                return Content("");
            }
        }


        #region  Demo Methods

        public ActionResult Test()
        {
            int incrementalId = 15;
            List<Models.TestModels> listHome = new List<Models.TestModels>();
            for (int i = 0; i <= incrementalId; i++)
            {
                Models.TestModels home = new Models.TestModels()
                {
                    id = i,
                    title = "Page Name",
                    image = "../../img/pak.jpg",
                    likes = 10,
                    dislikes = 5,
                    comments = 15,
                    votes = 10,
                    shareLinks = 14
                };

                listHome.Add(home);
            }

            return View(listHome);
        }

        public string nextRecords(int id)
        {
            int incrementalId = id + 15;
            List<Models.TestModels> listHome = new List<Models.TestModels>();
            for (int i = id; i <= incrementalId; i++)
            {
                Models.TestModels home = new Models.TestModels()
                {
                    id = i,
                    title = "Page Name",
                    image = "../../img/cena.jpg",
                    likes = 10,
                    dislikes = 5,
                    comments = 15,
                    votes = 10,
                    shareLinks = 14
                };

                listHome.Add(home);
            }
            string serializeObject = JsonConvert.SerializeObject(listHome, Newtonsoft.Json.Formatting.Indented);
            return serializeObject;
        }

        #endregion
    }
}
