﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PostSecret.ViewModels;
using Boomers.Utilities.Guids;
using Boomers.Utilities.Text;

namespace PostSecret.Controllers
{
    public class TagsController : Controller
    {
        //
        // GET: /Tags/

        public ActionResult Tag(int id)
        {
            Code.PSDataContext db = new Code.PSDataContext();
            var getTag = (from tn in db.PS_Tags_Pulls
                          where tn.uid == id
                          select new TagViewModel
                          {
                              TagName = tn.Tag_Name,
                              Poster = (from xx in db.PS_Cards
                                        from tg in db.PS_Tags
                                        where tg.Tag_ID == tn.uid
                                        where tg.Item_ID == xx.uid
                                        select new CardViewModel
                                        {
                                            uid = xx.uid,
                                            Title = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(xx.Title),
                                            Views = xx.Views,
                                            DateTimeAdded = xx.DateTime_Added,
                                            IPAddress = xx.IP_Address,
                                            NickName = xx.NickName,
                                            UserId = xx.User_ID.GetValueOrDefault(),
                                            VotesDown = xx.Votes_Down,
                                            VotesUp = xx.Votes_Up,
                                            Votes = (from nn in db.PS_Votes
                                                     where nn.Item_Guid == xx.uid
                                                     select new VoteViewModel
                                                     {
                                                         IPAddress = nn.User_IP_Address,
                                                         Vote = nn.Vote,
                                                         ItemGuid = nn.Item_Guid,
                                                         UserID = nn.User_ID,
                                                     }).ToList(),
                                            Tags = (from zz in db.PS_Tags
                                                    from cc in db.PS_Tags_Pulls
                                                    where zz.Tag_ID == cc.uid
                                                    where zz.Item_ID == xx.uid
                                                    select new TagViewModel
                                                    {
                                                        TagName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cc.Tag_Name).ToSearchEngineFriendly(),
                                                        TagID = zz.Tag_ID,
                                                        TagCount = (from yy in db.PS_Tags
                                                                    where yy.Tag_ID == zz.Tag_ID
                                                                    select yy.Tag_ID).Count()
                                                    }).Distinct().ToList(),
                                        }).ToList(),
                              TagCount = (from tt in db.PS_Tags
                                          where tt.Tag_ID == id
                                          select tt.uid).Count()
                          }).FirstOrDefault();

            var getTags = (from xx in db.PS_Tags
                           from zz in db.PS_Tags_Pulls
                           where xx.Tag_ID == zz.uid
                           select new TagViewModel
                           {
                               TagName = zz.Tag_Name,
                               TagID = xx.Tag_ID,
                               TagCount = (from yy in db.PS_Tags
                                           where yy.Tag_ID == xx.Tag_ID
                                           select yy.Tag_ID).Count()
                           }).Distinct().Take(10).ToList();
            ViewData["Tags"] = getTags;
            ViewData["Tag"] = getTag;
            SupportFramework.HttpExtensions.SiteMap.AddNode(Request.Url.ToString(), false);
            return View(ViewData);
        }

    }
}
