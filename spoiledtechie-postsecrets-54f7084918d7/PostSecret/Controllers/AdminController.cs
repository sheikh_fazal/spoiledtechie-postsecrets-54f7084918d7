﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Text;
using Boomers.Utilities.DatesTimes;
using System.Text.RegularExpressions;
using PostSecret.Code.SupportFramework;
using Boomers.Utilities.MVCHelpers.Paging;
using PostSecret.ViewModels;

namespace PostSecret.Controllers
{
    public class AdminController : Controller
    {
        private const int defaultPageSize = 10;
        private IList<ErrorViewModel> allErrors = new List<ErrorViewModel>();
        //private string[] categories = new string[3] { "Shoes", "Electronics", "Food" };

            

        public ActionResult Errors(int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            GlobalDataContext db = new GlobalDataContext();
            var getErrors = (from xx in db.Global_Errors_Logs
                             where xx.Reviewed == 0
                             where xx.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                             select new ErrorViewModel
                             {
                                 Application_Id = (Guid)xx.Application_Id,
                                 Date_Time = xx.Date_Time,
                                 Error_Message = xx.Error_Message,
                                 Error_Previous_URL = xx.Error_Previous_URL,
                                 Error_Source = xx.Error_Source,
                                 Error_Target = xx.Error_Target,
                                 Error_Trace = xx.Error_Trace,
                                 Error_URL = xx.Error_URL,
                                 Last_Exception = xx.Last_Exception,
                                 Load_Date = xx.Load_Date,
                                 Reviewed = xx.Reviewed,
                                 Session_Data = xx.Session_Data,
                                 Trace_Error = xx.Trace_Error,
                                 uid = xx.uid,
                                 User_Email = xx.User_Email,
                                 User_ID = xx.User_ID,
                                 Version = xx.Version,
                                 User_Name =  SupportFramework.DataAccessGlobal.UserName(xx.User_ID.GetValueOrDefault().ToString())
                                                              }).ToPagedList(currentPageIndex, defaultPageSize);
            
            
            return View(getErrors);
        }
        public ActionResult GetError(string errorId)
        { 
        GlobalDataContext db = new GlobalDataContext();
        var getErrors = (from xx in db.Global_Errors_Logs
                         where xx.uid == Convert.ToInt32(errorId)
                         where xx.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                         select new ErrorViewModel
                         {
                             Application_Id = (Guid)xx.Application_Id,
                             Date_Time = xx.Date_Time,
                             Error_Message = xx.Error_Message,
                             Error_Previous_URL = xx.Error_Previous_URL,
                             Error_Source = xx.Error_Source,
                             Error_Target = xx.Error_Target,
                             Error_Trace = xx.Error_Trace,
                             Error_URL = xx.Error_URL,
                             Last_Exception = xx.Last_Exception,
                             Load_Date = xx.Load_Date,
                             Reviewed = xx.Reviewed,
                             Session_Data = xx.Session_Data,
                             Trace_Error = xx.Trace_Error,
                             uid = xx.uid,
                             User_Email = xx.User_Email,
                             User_ID = xx.User_ID,
                             Version = xx.Version,
                             User_Name = SupportFramework.DataAccessGlobal.UserName(xx.User_ID.GetValueOrDefault().ToString())
                         }).FirstOrDefault();
        return Json(new { Error = getErrors}, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RemoveError(string errorId)
        {
            GlobalDataContext db = new GlobalDataContext();
            var getErrors = (from xx in db.Global_Errors_Logs
                             where xx.uid == Convert.ToInt32(errorId)
                             where xx.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                                                          select xx).FirstOrDefault();
            getErrors.Reviewed = 1;
            db.SubmitChanges(); 
            return Json(new { message ="Error Removed"}, JsonRequestBehavior.AllowGet);
        
        }
        public ActionResult SetUserApproval(string userName)
        {   
            string text, message;
            MembershipUser mu = Membership.GetUser(userName);
            if (mu.IsApproved)
            {
                mu.IsApproved = false;
                text = "App";
                message = "User " + userName + " has been UN-Approved";
            }
            else
            {
                mu.IsApproved = true;
                text = "Un-App";
                message = "User " + userName + " has been Approved";
            }
            Membership.UpdateUser(mu);
            return Json(new { message = message, spnText = text, username = userName }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRoleToUser(string userName, string role)
        {
            System.Web.Security.Roles.AddUserToRole(userName, role);
            return Json(new { message = "User " + userName + " Added to " + role + " role" }, JsonRequestBehavior.AllowGet);
        }
        //TODO:
        //public ActionResult GetUsersNonRoles(string userName)
        //{
        //    List<RolesViewModel> ur = new List<RolesViewModel>();
        //    foreach (string role in System.Web.Security.Roles.GetAllRoles())
        //    {
        //        if (!System.Web.Security.Roles.IsUserInRole(userName, role))
        //        {
        //            RolesViewModel urs = new RolesViewModel();
        //            urs.Role = role;
        //            ur.Add(urs);
        //        }
        //    }
        //    return Json(new { Roles = ur }, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult Users()
        {
            ViewData["regUsersCount"] = Membership.GetAllUsers().Count;
            return View(ViewData);
        }

        public ActionResult SearchUsersByUserName(string userName)
        {
            Code.AdminDataContext db = new Code.AdminDataContext();
            var ui = (from yy in db.aspnet_Users
                      from xx in db.aspnet_Memberships
                      where yy.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                      where yy.UserName.ToLower().Contains(userName.ToLower())
                      where xx.UserId == yy.UserId
                      where xx.ApplicationId == yy.ApplicationId
                      select new UserViewModel
                      {
                          UserName = yy.UserName,
                          Approved = xx.IsApproved ? "Un-App" : "App",
                          Comments = xx.Comment,
                          CreateDate = xx.CreateDate.ToShortDateString(),
                          Email = xx.Email,
                          LastLogin = xx.LastLoginDate.ToShortDateString(),
                          Locked = xx.IsLockedOut ? "Unlock" : "",
                          PasswordQuestion = xx.PasswordQuestion,
                          //UserOnline = xx.LastLoginDate> DateTime.UtcNow.AddMinutes(-5) ? true : false,
                          Roles = System.Web.Security.Roles.GetRolesForUser(yy.UserName),
                      }).ToList();
            return Json(new { User = ui }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchUsersByEmail(string email)
        {
            Code.AdminDataContext db = new Code.AdminDataContext();
            var ui = (from yy in db.aspnet_Users
                      from xx in db.aspnet_Memberships
                      where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                      where xx.LoweredEmail.Contains(email.ToLower())
                      where xx.UserId == yy.UserId
                      where xx.ApplicationId == yy.ApplicationId
                      select new UserViewModel
                      {
                          UserName = yy.UserName,
                          Approved = xx.IsApproved ? "Un-App" : "App",
                          Comments = xx.Comment,
                          CreateDate = xx.CreateDate.ToShortDateString(),
                          Email = xx.Email,
                          LastLogin = xx.LastLoginDate.ToShortDateString(),
                          Locked = xx.IsLockedOut ? "Unlock" : "",
                          PasswordQuestion = xx.PasswordQuestion,
                          //UserOnline = xx.LastLoginDate> DateTime.UtcNow.AddMinutes(-5) ? true : false,
                          Roles = System.Web.Security.Roles.GetRolesForUser(yy.UserName),
                      }).ToList();
            return Json(new { User = ui }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult RemoveUserFromRole(string user, string role)
        {
            System.Web.Security.Roles.RemoveUserFromRole(user, role);
            return Json(new { message = "User " + user + " removed from " + role }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUsersInRole(string role)
        {
            var roles = System.Web.Security.Roles.GetUsersInRole(role.Trim());
            StringBuilder sb = new StringBuilder();
            sb.Append("<table id=\"tblUsersInRoles\">");
            foreach (string item in roles)
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append(item);
                sb.Append("</td><td>");
                sb.Append("<span class='spanLink' onclick=\"javascript:RemoveUserFromRole('" + item + "', '" + role + "');\">Remove User<span>");
                sb.Append("</td></tr>");
            }
            sb.Append("</table>");
            return Json(new { table = sb.ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteRole(string role)
        {
            bool check = System.Web.Security.Roles.DeleteRole(role.Trim());
            if (check == true)
                return Json(new { message = "Role " + role + " Deleted" }, JsonRequestBehavior.AllowGet);

            return Json(new { message = "Role " + role + " NOT DELETED" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Roles()
        {

            return View(ViewData);
        }

        public ActionResult CreateRoles(string role)
        {
            try
            {
                System.Web.Security.Roles.CreateRole(role.Trim());
            }
            catch (Exception e) { }
            return Json(new { message = "Role " + role + " created" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Database()
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("<table id=\"tableInfoSite\" class=\"tbl\">");
            sb.Append("<thead><tr>");
            sb.Append("<th>Table Name</th><th class=\"{sorter: 'fancyNumber'}\">Rows</th><th class=\"{sorter: 'fancyNumber'}\">Reserved Space</th><th class=\"{sorter: 'fancyNumber'}\">Data</th><th class=\"{sorter: 'fancyNumber'}\">Index Size</th><th class=\"{sorter: 'fancyNumber'}\">Unused Space</th><th>Truncate Table</th>");
            sb.Append("</tr></thead>");

            Code.PSDataContext adb = new Code.PSDataContext();
            IEnumerable<DataTables> query = adb.ExecuteQuery<DataTables>("SELECT * FROM sysobjects WHERE type = 'U'");
            List<DataTables> tables = query.OrderBy(t => t.name).ToList();
            int counter = 0;
            int rowsTotal = 0;
            int dbDataSize = 0;
            int dbReserved = 0;
            int dbIndexed = 0;
            int dbUnusued = 0;
            Regex numbers = new Regex(@"[\d,]+", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            for (int i = 0; i < tables.Count; i++)
            {
                switch (i % 2)
                {
                    case 1:
                        sb.Append("<tr class=\"d0\">");
                        break;
                    case 0:
                        sb.Append("<tr class=\"d1\">");
                        break;
                }

                IEnumerable<TableInfo> queryTables = adb.ExecuteQuery<TableInfo>("sp_spaceused '" + tables[i].name + "'");
                foreach (var table in queryTables)
                {
                    sb.Append("<td>" + table.name + "</td>");
                    sb.Append("<td>" + table.rows + "</td>");
                    sb.Append("<td>" + table.reserved + "</td>");
                    sb.Append("<td>" + table.data + "</td>");
                    sb.Append("<td>" + table.index_size + "</td>");
                    sb.Append("<td>" + table.unused + "</td>");
                    sb.Append("<td><input id=\"truncate" + counter + "\" type=\"button\" value=\"Truncate\" onclick=\"return Confirm_Truncate_Table('" + table.name + "','site')\" /></td>");
                    rowsTotal += Convert.ToInt32(table.rows);
                    dbDataSize += Convert.ToInt32(numbers.Match(table.data).Value);
                    dbReserved += Convert.ToInt32(numbers.Match(table.reserved).Value);
                    dbIndexed += Convert.ToInt32(numbers.Match(table.index_size).Value);
                    dbUnusued += Convert.ToInt32(numbers.Match(table.unused).Value);
                }
                sb.Append("</tr>");
                counter += 1;
            }
            sb.Append("</table>");
            ViewData["SiteInformation"] += sb.ToString();

            //Database View
            IEnumerable<DatabaseInfos> queryDB = adb.ExecuteQuery<DatabaseInfos>("sp_spaceused");
            PostSecret.ViewModels.DatabaseViewModel2 dbSite = new DatabaseViewModel2();
            foreach (var item in queryDB)
            {
                dbSite.database_name = item.database_name;
                dbSite.database_size = item.database_size + " -- " + ((dbReserved + dbDataSize) * .0009).ToString("N2") + " MB";
                dbSite.reserved = item.reserved + " -- " + dbReserved.ToString("N0");
                dbSite.data = dbDataSize.ToString("N0");
                dbSite.index_size = item.index_size + " -- " + dbIndexed.ToString("N0");
                dbSite.unallocated_space = item.unused + " -- " + dbUnusued.ToString("N0");
            }
            dbSite.TotalTables = tables.Count.ToString("N0");
            dbSite.TotalRows = rowsTotal.ToString("N0");
            ViewData["DBInfo"] = dbSite;



            StringBuilder sb1 = new StringBuilder();
            sb1.Append("<table id=\"tableInfo\" class=\"tbl\">");
            sb1.Append("<thead><tr>");
            sb1.Append("<th>Table Name</th><th class=\"{sorter: 'fancyNumber'}\">Rows</th><th class=\"{sorter: 'fancyNumber'}\">Reserved Space</th><th class=\"{sorter: 'fancyNumber'}\">Data</th><th class=\"{sorter: 'fancyNumber'}\">Index Size</th><th class=\"{sorter: 'fancyNumber'}\">Unused Space</th><th>Truncate Table</th>");
            sb1.Append("</tr></thead>");
            Code.AdminDataContext db = new Code.AdminDataContext();

            IEnumerable<DataTables> queryAdmin = db.ExecuteQuery<DataTables>("SELECT * FROM sysobjects WHERE type = 'U'");
            List<DataTables> tablesAdmin = queryAdmin.OrderBy(t => t.name).ToList();
            int counterAdmin = 0;
            int rowsTotalAdmin = 0;
            int dbDataSizeAdmin = 0;
            int dbReservedAdmin = 0;
            int dbIndexedAdmin = 0;
            int dbUnusuedAdmin = 0;

            for (int i = 0; i < tablesAdmin.Count; i++)
            {
                switch (i % 2)
                {
                    case 1:
                        sb1.Append("<tr class=\"d0\">");
                        break;
                    case 0:
                        sb1.Append("<tr class=\"d1\">");
                        break;
                }

                IEnumerable<TableInfo> queryTablesAdmin = db.ExecuteQuery<TableInfo>("sp_spaceused '" + tables[i].name + "'");
                foreach (var table in queryTablesAdmin)
                {
                    sb1.Append("<td>" + table.name + "</td>");
                    sb1.Append("<td>" + table.rows + "</td>");
                    sb1.Append("<td>" + table.reserved + "</td>");
                    sb1.Append("<td>" + table.data + "</td>");
                    sb1.Append("<td>" + table.index_size + "</td>");
                    sb1.Append("<td>" + table.unused + "</td>");
                    sb1.Append("<td><input id=\"truncate" + counter + "\" type=\"button\" value=\"Truncate\" onclick=\"return Confirm_Truncate_Table('" + table.name + "', 'membership')\" /></td>");
                    rowsTotal += Convert.ToInt32(table.rows);
                    dbDataSize += Convert.ToInt32(numbers.Match(table.data).Value);
                    dbReserved += Convert.ToInt32(numbers.Match(table.reserved).Value);
                    dbIndexed += Convert.ToInt32(numbers.Match(table.index_size).Value);
                    dbUnusued += Convert.ToInt32(numbers.Match(table.unused).Value);
                }
                sb.Append("</tr>");
                counter += 1;
            }
            sb.Append("</table>");
            ViewData["MemInfo"] += sb.ToString();

            //Database View
            IEnumerable<DatabaseInfos> queryDBAdmin = db.ExecuteQuery<DatabaseInfos>("sp_spaceused");
            PostSecret.ViewModels.DatabaseViewModel2 dbAdmin = new DatabaseViewModel2();
            foreach (var item in queryDBAdmin)
            {
                dbAdmin.database_name = item.database_name;
                dbAdmin.database_size = item.database_size + " -- " + ((dbReserved + dbDataSize) * .0009).ToString("N2") + " MB";
                dbAdmin.reserved = item.reserved + " -- " + dbReserved.ToString("N0");
                dbAdmin.data = dbDataSize.ToString("N0");
                dbAdmin.index_size = item.index_size + " -- " + dbIndexed.ToString("N0");
                dbAdmin.unallocated_space = item.unused + " -- " + dbUnusued.ToString("N0");
            }
            dbAdmin.TotalTables = tables.Count.ToString("N0");
            dbAdmin.TotalRows = rowsTotal.ToString("N0");
            ViewData["DBSiteInfo"] = dbAdmin;
            return View(ViewData);
        }


        public ActionResult Views()
        {
            PostSecret.Code.SupportFramework.GlobalDataContext gdb = new PostSecret.Code.SupportFramework.GlobalDataContext();
            var OSTypes = (from xx in gdb.Global_User_Audits
                           where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                           group xx by xx.Windows_Platform into gg
                           select new
                           {
                               gg.Key,
                               userCount = (from yy in gg
                                            select yy.User_ID).Distinct().Count()
                           }).OrderBy(p => p.Key).ToList();
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"divDetails\"><div>");
            sb.Append(OSTypes.Count);
            sb.Append(" OS Types</div>");
            sb.Append("<ul class=\"ulList\">");
            for (int i = 0; i < OSTypes.Count(); i++)
            {
                sb.Append("<li><b>");
                sb.Append(OSTypes[i].userCount);
                sb.Append("</b> users; using ");
                sb.Append(OSTypes[i].Key);
                sb.Append("</li>");
            }
            sb.Append("<ul>");
            sb.Append("</div>");
            ViewData["OSTypes"] = sb.ToString();


            var getBrowserTypes = (from xx in gdb.Global_User_Audits
                                   where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                                   group xx by xx.Browser_String into gg
                                   select new
                                   {
                                       gg.Key,
                                       UserCount = (from yy in gg
                                                    select yy.User_ID).Distinct().Count(),
                                       lastUsed = (from yy in gg
                                                   orderby yy.DateTime descending
                                                   select yy.DateTime).FirstOrDefault(),
                                   }).OrderBy(p => p.lastUsed).ToList();
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("<div class=\"divDetails\"><div>");
            sb1.Append(getBrowserTypes.Count);
            sb1.Append(" Browser Types</div>");
            sb1.Append("<ul class=\"ulList\">");
            for (int i = 0; i < getBrowserTypes.Count(); i++)
            {
                sb1.Append("<li>Last Used: ");
                sb1.Append(getBrowserTypes[i].lastUsed.Value.ToyyyyMMdd());
                sb1.Append("; <b>");
                sb1.Append(getBrowserTypes[i].UserCount);
                sb1.Append("</b> users; using ");
                sb1.Append(getBrowserTypes[i].Key);
                sb1.Append("</li>");
            }
            sb.Append("<ul>");
            sb.Append("</div>");
            ViewData["BrowserTypes"] = sb.ToString();


            return View(ViewData);
        }

        public ActionResult Default()
        {
            int userCount = Membership.GetAllUsers().Count;
            Code.AdminDataContext adb = new Code.AdminDataContext();
            PostSecret.Code.SupportFramework.GlobalDataContext gdb = new PostSecret.Code.SupportFramework.GlobalDataContext();
            GlobalDataContext db = new GlobalDataContext();

            //User information
            ViewData["RegUsers"] = userCount.ToString("N0");
            ViewData["UsersOnline"] = Membership.GetNumberOfUsersOnline() > 1 ? Membership.GetNumberOfUsersOnline().ToString("N0") + " Online" : Membership.GetNumberOfUsersOnline().ToString("N0") + " Online";
            ViewData["LockedUsers"] = (from spm in adb.vw_aspnet_MembershipUsers
                                       where spm.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                                       where spm.IsLockedOut == true
                                       select spm.UserName).Count().ToString("N0");

            ViewData["RolesCount"] = System.Web.Security.Roles.GetAllRoles().Count().ToString();
            ViewData["TotalErrors"] = (from tel in gdb.Global_Errors_Logs
                                       where tel.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                                       select tel.uid).Count().ToString("N0");

            ViewData["UnsolvedErrors"] = (from tel in gdb.Global_Errors_Logs
                                          where tel.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                                          where tel.Reviewed == 0
                                          select tel.uid).Count().ToString("N0");

            ViewData["7DayErrors"] = (from tel in gdb.Global_Errors_Logs
                                      where tel.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                                      where tel.Date_Time > DateTime.UtcNow.AddDays(-7)
                                      select tel.uid).Count().ToString("N0");

            ViewData["365DayErrors"] = (from tel in gdb.Global_Errors_Logs
                                        where tel.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                                        where tel.Date_Time > DateTime.UtcNow.AddDays(-365)
                                        select tel.uid).Count().ToString("N0");

            int getApprovedUserCount = (from xx in adb.vw_aspnet_MembershipUsers
                                        where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                                        where xx.IsApproved == true
                                        select xx.UserId).Count();
            ViewData["ApprovedUsers"] = getApprovedUserCount.ToString("N0");
            ViewData["UnApprovedUsers"] = (from xx in adb.vw_aspnet_MembershipUsers
                                           where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                                           where xx.IsApproved == false
                                           select xx.UserId).Count().ToString();

            ViewData["LoggedIn5Days"] = (from xx in adb.vw_aspnet_MembershipUsers
                                         where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                                         where xx.LastLoginDate > DateTime.UtcNow.AddDays(-5)
                                         select xx).Count().ToString("N0");
            ViewData["LoggedIn10Days"] = (from xx in adb.vw_aspnet_MembershipUsers
                                          where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                                          where xx.LastLoginDate > DateTime.UtcNow.AddDays(-10)
                                          select xx).Count().ToString("N0");
            ViewData["LoggedIn30Days"] = (from xx in adb.vw_aspnet_MembershipUsers
                                          where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                                          where xx.LastLoginDate > DateTime.UtcNow.AddDays(-30)
                                          select xx).Count().ToString("N0");

            //Views information
            ViewData["UsersToday"] = (from xx in adb.vw_aspnet_MembershipUsers
                                      where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
                                      where xx.LastLoginDate > DateTime.UtcNow.AddHours(-24)
                                      select xx).Count().ToString("N0");
            //ViewData["PageViewsToday"] = (from xx in gdb.Global_User_Audits
            //                              where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
            //                              where xx.DateTime > DateTime.UtcNow.AddHours(-24)
            //                              select xx).Count().ToString("N0");
            //ViewData["PageViews5"] = (from xx in gdb.Global_User_Audits
            //                          where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
            //                          where xx.DateTime > DateTime.UtcNow.AddDays(-5)
            //                          select xx).Count().ToString("N0");
            //ViewData["PageViews10"] = (from xx in gdb.Global_User_Audits
            //                           where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
            //                           where xx.DateTime > DateTime.UtcNow.AddDays(-10)
            //                           select xx).Count().ToString("N0");
            //ViewData["PageViews30"] = (from xx in gdb.Global_User_Audits
            //                           where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
            //                           where xx.DateTime > DateTime.UtcNow.AddDays(-30)
            //                           select xx).Count().ToString("N0");
            //ViewData["TotalPageViews"] = (from xx in gdb.Global_User_Audits
            //                              where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
            //                              select xx).Count().ToString("N0");
            //ViewData["BrowserTypes"] = (from xx in gdb.Global_User_Audits
            //                            where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
            //                            group xx by xx.Browser_String into g
            //                            select g).Count().ToString("N0");
            //ViewData["OsTypes"] = (from xx in gdb.Global_User_Audits
            //                       where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
            //                       group xx by xx.Windows_Platform into g
            //                       select g).Count().ToString("N0");

            //var javaScript = (from xx in gdb.Global_User_Audits
            //                  where xx.ApplicationId == SupportFramework.DataAccessGlobal.ApplicationId()
            //                  group xx by xx.JavaScript into g
            //                  select new
            //                  {
            //                      JScript = g.Key,
            //                      Users = (from yy in g
            //                               where yy.DateTime > DateTime.UtcNow.AddDays(-30)
            //                               select yy.User_ID).Distinct().Count()
            //                  });
            //foreach (var item in javaScript)
            //    ViewData["JavaMonth"] += item.Users.ToString() + (item.JScript.Value == 1 ? " Users JavaScript Enabled" : " Users JavaScript Disabled") + "<br />";

            //Database View
            IEnumerable<DatabaseViewModel> query = adb.ExecuteQuery<DatabaseViewModel>("sp_spaceused");
            foreach (var item in query)
            {
                ViewData["DBName"] = item.database_name;
                ViewData["DBSize"] = item.database_size;
                ViewData["DBReserved"] = item.reserved;
                ViewData["DBData"] = item.data;
                ViewData["DBIndexSize"] = item.index_size;
                ViewData["DBUnusedSpace"] = item.unused;
            }
            IEnumerable<DatabaseViewModel> query1 = db.ExecuteQuery<DatabaseViewModel>("sp_spaceused");
            foreach (var item in query1)
            {
                ViewData["DBSiteName"] = item.database_name;
                ViewData["DBSiteSize"] = item.database_size;
                ViewData["DBSiteReserved"] = item.reserved;
                ViewData["DBSiteData"] = item.data;
                ViewData["DBSiteIndexSize"] = item.index_size;
                ViewData["DBSiteUnusedSpace"] = item.unused;
            }

            return View(ViewData);
        }

    }
}
