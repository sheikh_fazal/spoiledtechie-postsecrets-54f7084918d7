﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OpenId;
using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using PostSecret.Models;

namespace PostSecret.Controllers
{

    //[HandleError]
    //public class AccountController : Controller
    //{
    //    private static OpenIdRelyingParty openid = new OpenIdRelyingParty();

    //    public IFormsAuthenticationService FormsService { get; set; }
    //    public IMembershipService MembershipService { get; set; }

    //    protected override void Initialize(RequestContext requestContext)
    //    {
    //        if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
    //        if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

    //        base.Initialize(requestContext);
    //    }

    //    // **************************************
    //    // URL: /Account/LogOn
    //    // **************************************

    //    public ActionResult Login()
    //    {
    //        return View();
    //    }

    //    [ValidateInput(false)]
    //    public ActionResult Authenticate(string returnUrl)
    //    {
    //        var response = openid.GetResponse();
    //        if (response == null)
    //        {
    //            // Stage 2: user submitting Identifier
    //            Identifier id;
    //            if (Identifier.TryParse(Request.Form["openid_identifier"], out id))
    //            {
    //                try
    //                {
    //                    var request = openid.CreateRequest(Request.Form["openid_identifier"]);

    //                    //Ask user for their email address
    //                    ClaimsRequest fields = new ClaimsRequest();
    //                    fields.Email = DemandLevel.Request;
    //                    request.AddExtension(fields);

    //                    return request.RedirectingResponse.AsActionResult();
    //                }
    //                catch (ProtocolException ex)
    //                {
    //                    ViewData["Message"] = ex.Message;
    //                    return View("Login");
    //                }
    //            }

    //            ViewData["Message"] = "Invalid identifier";
    //            return View("Login");
    //        }

    //        // Stage 3: OpenID Provider sending assertion response
    //        switch (response.Status)
    //        {
    //            case AuthenticationStatus.Authenticated:
    //                Session["FriendlyIdentifier"] = response.ClaimedIdentifier;
    //                MembershipUser user = MembershipService.GetUser(response.ClaimedIdentifier);
    //                if (user == null)
    //                {
    //                    MembershipCreateStatus membershipCreateStatus;

    //                    //Get custom fields for user
    //                    var sreg = response.GetExtension<ClaimsResponse>();
    //                    if (sreg != null)
    //                    {
    //                        membershipCreateStatus = MembershipService.CreateUser(response.ClaimedIdentifier, "123456", sreg.Email);
    //                    }
    //                    else
    //                    {
    //                        membershipCreateStatus = MembershipService.CreateUser(response.ClaimedIdentifier, "123456", "john@doe.com");
    //                    }
    //                    if (membershipCreateStatus == MembershipCreateStatus.Success)
    //                    {
    //                        Code.AdminDataContext db = new Code.AdminDataContext();
    //                        Code.user_Information ui = new Code.user_Information();
    //                        ui.user_ID = SupportFramework.DataAccessGlobal.UserID(response.ClaimedIdentifier);
    //                        ui.Points += 15;
    //                        ui.Nick_Name = response.ClaimedIdentifier;
    //                        ui.Email_Allowed = 1;
    //                        ui.Application_ID = SupportFramework.DataAccessGlobal.ApplicationId();
    //                        db.user_Informations.InsertOnSubmit(ui);
    //                        db.SubmitChanges();

    //                        FormsService.SignIn(response.ClaimedIdentifier, false /* createPersistentCookie */);
    //                        return RedirectToAction("Index", "home");
    //                    }
    //                    ViewData["Message"] = "Error creating new user";
    //                    return View("Login");
    //                }

    //                FormsAuthentication.SetAuthCookie(user.UserName, false);
    //                if (!string.IsNullOrEmpty(returnUrl))
    //                {
    //                    return Redirect(returnUrl);
    //                }

    //                return RedirectToAction("Index", "Home");
    //            case AuthenticationStatus.Canceled:
    //                ViewData["Message"] = "Canceled at provider";
    //                return View("Login");
    //            case AuthenticationStatus.Failed:
    //                ViewData["Message"] = response.Exception.Message;
    //                return View("Login");
    //        }

    //        return new EmptyResult();
    //    }


    //    // **************************************
    //    // URL: /Account/LogOff
    //    // **************************************

    //    public ActionResult LogOff()
    //    {
    //        FormsService.SignOut();

    //        return RedirectToAction("Index", "Home");
    //    }

    //    // **************************************
    //    // URL: /Account/Register
    //    // **************************************

    //    public ActionResult Register()
    //    {
    //        ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
    //        return View();
    //    }

    //    [HttpPost]
    //    public ActionResult Register(RegisterModel model)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            // Attempt to register the user
    //            MembershipCreateStatus createStatus = MembershipService.CreateUser(model.UserName, model.Password, model.Email);

    //            if (createStatus == MembershipCreateStatus.Success)
    //            {
    //                FormsService.SignIn(model.UserName, false /* createPersistentCookie */);
    //                return RedirectToAction("Index", "Home");
    //            }
    //            else
    //            {
    //                ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
    //            }
    //        }

    //        // If we got this far, something failed, redisplay form
    //        ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
    //        return View(model);
    //    }

    //    // **************************************
    //    // URL: /Account/ChangePassword
    //    // **************************************

    //    [Authorize]
    //    public ActionResult ChangePassword()
    //    {
    //        ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
    //        return View();
    //    }

    //    [Authorize]
    //    [HttpPost]
    //    public ActionResult ChangePassword(ChangePasswordModel model)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
    //            {
    //                return RedirectToAction("ChangePasswordSuccess");
    //            }
    //            else
    //            {
    //                ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
    //            }
    //        }

    //        // If we got this far, something failed, redisplay form
    //        ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
    //        return View(model);
    //    }

    //    // **************************************
    //    // URL: /Account/ChangePasswordSuccess
    //    // **************************************

    //    public ActionResult ChangePasswordSuccess()
    //    {
    //        return View();
    //    }

    //}
}
