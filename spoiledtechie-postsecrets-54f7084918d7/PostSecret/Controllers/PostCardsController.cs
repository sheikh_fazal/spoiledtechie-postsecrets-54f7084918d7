﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PostSecret.ViewModels;
using Twitterizer;
using Boomers.Utilities.Guids;
using Boomers.Utilities.Text;
using Boomers.Utilities.Web;
using Boomers.Utilities.DatesTimes;
using Boomers.Utilities.MVCHelpers.Paging;

namespace PostSecret.Controllers
{
    public class PostCardsController : Controller
    {
        [ValidateInput(false)]
        [AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult AddPoster(FormCollection form)
        //{
        //    string name = form["nickName"].Trim();
        //    string email = form["email"].Trim();
        //    var file = Request.Files["filePicture"];
        //    string fileLink = form["tbFileLink"].Trim();
        //    string title = form["tbTitle"].Trim();
        //    string description = form["tbDescription"].Trim();
        //    string tags = form["tagsRecall"].Trim();
        //    string savePath = string.Empty;
        //    string fileName = string.Empty;
        //    string extension = string.Empty;
        //    string thumbSaved = string.Empty;
        //    double resolution = 0;
        //    DateTime dt = DateTime.UtcNow;
        //    string imageLocation = @"C:\HostingSpaces\pio.scott@gmail.com\images.codingforcharity.org\wwwroot\dmp\" + dt.ToyyyyMMdd() + @"\";
        //    if (!Directory.Exists(imageLocation))
        //        Directory.CreateDirectory(imageLocation);
        //    string salt = "_" + DateExt.ToyyyyMMddHHmmss(dt);

        //    Code.PSDataContext db = new Code.PSDataContext();

        //    if (fileLink.Length > 5)
        //    {
        //        WebClient Client = new WebClient();
        //        Client.DownloadFile(fileLink, imageLocation + Path.GetFileNameWithoutExtension(fileLink) + salt + Path.GetExtension(fileLink));

        //        savePath = imageLocation + Path.GetFileNameWithoutExtension(fileLink) + salt + Path.GetExtension(fileLink);
        //        var thumb = Boomers.Utilities.Imaging.ImageFunctions.CreateThumbnail(new Bitmap(savePath), 200, 150, true);
        //        fileName = Path.GetFileNameWithoutExtension(fileLink);
        //        extension = Path.GetExtension(fileLink);
        //        thumb.Save(imageLocation + fileName + salt + "_thumb" + extension);
        //        thumb.Dispose();
        //        thumbSaved = imageLocation + fileName + salt + "_thumb" + extension;
        //        resolution = new Bitmap(savePath).HorizontalResolution;
        //    }
        //    if (file.ContentLength > 0)
        //    {
        //        savePath = Boomers.Utilities.Documents.Files.SaveDocuments(file, Path.GetFileNameWithoutExtension(file.FileName) + salt + Path.GetExtension(file.FileName), imageLocation);
        //        fileName = Path.GetFileNameWithoutExtension(file.FileName);
        //        extension = Path.GetExtension(file.FileName);
        //        var thumb = Boomers.Utilities.Imaging.ImageFunctions.CreateThumbnail(new Bitmap(savePath), 200, 150, true);
        //        thumb.Save(imageLocation + fileName + salt + "_thumb" + extension);
        //        thumb.Dispose();
        //        thumbSaved = imageLocation + fileName + salt + "_thumb" + extension;
        //        resolution = new Bitmap(savePath).HorizontalResolution;
        //    }

        //    Code.PS_Postcard im = new Code.PS_Postcard();
        //    im.DateTime_Added = DateTime.UtcNow;
        //    im.uid = Guid.NewGuid();
        //    im.Ip_Address_Added = Request.IPAddress();
        //    im.Name = fileName;
        //    im.Save_Location= savePath;
        //    im.SavePathThumb = thumbSaved;
        //    im.Resolution = (int)resolution;
        //    im.PostCard_Url= "http://images.codingforcharity.org/dmp/" + dt.ToyyyyMMdd() + "/" + fileName + salt + extension;
        //    im.UrlThumb = "http://images.codingforcharity.org/dmp/" + dt.ToyyyyMMdd() + "/" + fileName + salt + "_thumb" + extension;
        //    if (Request.IsAuthenticated)
        //        im.User_ID_Added = SupportFramework.DataAccessGlobal.UserID();
        //    db.PS_Postcards.InsertOnSubmit(im);

        //    savePath = Code.Imaging.CreateMotPoster(savePath, imageLocation + title.ToSearchEngineFriendly() + salt + extension, title, description, "DePostSecret.com");

        //    fileName = Path.GetFileNameWithoutExtension(savePath);
        //    extension = Path.GetExtension(savePath);
        //    var thumbPost = Boomers.Utilities.Imaging.ImageFunctions.CreateThumbnail(new Bitmap(savePath), 200, 150, true);
        //    thumbPost.Save(imageLocation + fileName + "_thumb" + extension);
        //    thumbPost.Dispose();

        //    thumbSaved = imageLocation + fileName + "_thumb" + extension;
        //    resolution = new Bitmap(savePath).HorizontalResolution;

        //    Code.PS_Image imPos = new Code.PS_Image();
        //    imPos.DateTime_Added = DateTime.UtcNow;
        //    imPos.Id = Guid.NewGuid();
        //    imPos.Ip_Address_Added = Request.IPAddress();
        //    imPos.Name = fileName;
        //    imPos.SavePathPosterSize = savePath;
        //    imPos.SavePathThumb = thumbSaved;
        //    imPos.Resolution = (int)resolution;
        //    imPos.UrlPosterSize = "http://images.codingforcharity.org/dmp/" + dt.ToyyyyMMdd() + "/" + title.ToSearchEngineFriendly() + salt + extension;
        //    imPos.UrlThumb = "http://images.codingforcharity.org/dmp/" + dt.ToyyyyMMdd() + "/" + fileName + "_thumb" + extension;
        //    if (Request.IsAuthenticated)
        //        imPos.User_ID_Added = SupportFramework.DataAccessGlobal.UserID();

        //    var smallImage = Boomers.Utilities.Imaging.ImageFunctions.ImageShrink(720, 720, new Bitmap(savePath), 72);
        //    smallImage.Save(imageLocation + title.ToSearchEngineFriendly() + salt + "_reg" + extension);
        //    smallImage.Dispose();
        //    imPos.SavePath = imageLocation + title.ToSearchEngineFriendly() + salt + "_reg" + extension;
        //    imPos.Url = "http://images.codingforcharity.org/dmp/" + dt.ToyyyyMMdd() + "/" + title.ToSearchEngineFriendly() + salt + "_reg" + extension;
        //    db.PS_Postcards.InsertOnSubmit(imPos);

        //    Code.PS_Poster po = new Code.PS_Poster();
        //    po.Content_Info = description;
        //    po.DateTime_Added = DateTime.UtcNow;
        //    po.IP_Address = Request.IPAddress();
        //    po.NickName = name;
        //    po.Card_Id = imPos.Id;
        //    po.Title = title;
        //    po.uid = Guid.NewGuid();
        //    if (Request.IsAuthenticated)
        //        po.User_ID = SupportFramework.DataAccessGlobal.UserID();
        //    db.PS_Cards.InsertOnSubmit(po);

        //    var ts = tags.Split(',');
        //    foreach (string tag in ts)
        //    {
        //        int tagID = 0;
        //        var findTag = (from xx in db.PS_Tags_Pulls
        //                       where xx.Tag_Name == tag.Trim()
        //                       select xx).FirstOrDefault();
        //        if (findTag == null)
        //        {
        //            Code.PS_Tags_Pull tp = new Code.PS_Tags_Pull();
        //            tp.DateTime_Submitted = DateTime.UtcNow;
        //            tp.Tag_Name = tag.Trim();
        //            if (Request.IsAuthenticated)
        //                tp.User_ID = SupportFramework.DataAccessGlobal.UserID();

        //            db.PS_Tags_Pulls.InsertOnSubmit(tp);

        //            db.SubmitChanges();
        //            tagID = tp.uid;
        //        }
        //        else
        //            tagID = findTag.uid;

        //        Code.PS_Tag tg = new Code.PS_Tag();
        //        tg.DateTime_Submitted = DateTime.UtcNow;
        //        tg.Item_ID = po.uid;
        //        tg.Tag_ID = tagID;
        //        db.PS_Tags.InsertOnSubmit(tg);
        //    }
        //    db.SubmitChanges();

        //    string titleTweet = (" #DMP: " + title + "; " + description);
        //    if (titleTweet.Length > 75)
        //        titleTweet = titleTweet.Remove(74);

        //    OAuthTokens tok = new OAuthTokens();
        //    tok.AccessToken = "215857944-Yn9WtDpnfSjBnNHjzqyq31Ok8vzYfC4zwPGXQYU8";
        //    tok.AccessTokenSecret = "wWs1Z2dUYMZo1qGRDOkOSMZjEWIilGFK5STcljRde3U";
        //    tok.ConsumerKey = "WCkNDLlguZDN2zzzX1plQ";
        //    tok.ConsumerSecret = "I5V2Jrc0irKKpEGcCS26kd9Rjnz3xDnwOetHFXiqY";
        //    var stat = TwitterStatus.Update(tok, "http://DePostSecret.com/PostCards/" + po.uid.RemoveDashes() + titleTweet);

        //    return RedirectToAction("Posters", new { id = po.uid.RemoveDashes(), title = po.Title.ToSearchEngineFriendly() });
        //}
        public ActionResult PostCards(Guid id)
        {
            Code.PSDataContext db = new Code.PSDataContext();
            var getTitle = (from xx in db.PS_Cards
                            from aa in db.PS_Postcards
                            where xx.Card_Id == aa.uid
                            where xx.uid == id
                            select new CardViewModel
                            {
                                orderbyId = xx.orderby_id,
                                CardCount = (from qq in db.PS_Cards
                                             select qq.uid).Count(),
                                uid = xx.uid,
                                Number = (from qq in db.PS_Cards
                                          where qq.orderby_id > xx.orderby_id
                                          select qq.uid).Count(),
                                FavoriteCount = xx.Favorites,
                                Favorites = (from nn in db.PS_Favorites
                                             where nn.Item_Guid == xx.uid
                                             select new FavoriteViewModel
                                             {
                                                 Item_Guid = nn.Item_Guid,
                                                 uid = nn.uid,
                                                 User_Id = nn.User_ID
                                             }).ToList(),
                                Title = xx.Title,
                                Url = aa.PostCard_UrlNew,
                                NickName = xx.NickName,
                                PostCard_Id = xx.Card_Id,
                                UserId = xx.User_ID,
                                IPAddress = xx.IP_Address,
                                DateTimeAdded = (DateTime)xx.DateTime_Added,
                                VotesUp = xx.Votes_Up,
                                VotesDown = xx.Votes_Down,
                                Views = xx.Views,
                                Votes = (from nn in db.PS_Votes
                                         where nn.Item_Guid == xx.uid
                                         select new VoteViewModel
                                         {
                                             IPAddress = nn.User_IP_Address,
                                             Vote = nn.Vote,
                                             ItemGuid = nn.Item_Guid,
                                             UserID = nn.User_ID,
                                         }).ToList(),
                                Tags = (from zz in db.PS_Tags
                                        from cc in db.PS_Tags_Pulls
                                        where zz.Tag_ID == cc.uid
                                        where zz.Item_ID == xx.uid
                                        select new TagViewModel
                                        {
                                            TagName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cc.Tag_Name).ToSearchEngineFriendly(),
                                            TagID = zz.Tag_ID,
                                            TagCount = (from yy in db.PS_Tags
                                                        where yy.Tag_ID == zz.Tag_ID
                                                        select yy.Tag_ID).Count()
                                        }).Distinct().ToList(),
                                Comments = (from cc in db.PS_Comments
                                            where cc.Comment_To_ID == xx.uid
                                            orderby cc.DateTime_Submitted
                                            select new CommentViewModel
                                            {
                                                VotesUp = cc.Votes_Up,
                                                VotesDown = cc.Votes_Down,
                                                CommentID = cc.uid,
                                                CommentHTML = cc.Comment_Html,
                                                DateTimeSubmitted = cc.DateTime_Submitted
                                            }).ToList(),
                                ContentSurvey = (from cc in db.PS_Comments
                                                 where cc.Content_To_Id == xx.uid
                                                 orderby cc.Votes_Up descending
                                                 select new CommentViewModel
                                                 {
                                                     VotesUp = cc.Votes_Up,
                                                     VotesDown = cc.Votes_Down,
                                                     ContentID = cc.uid,
                                                     CommentHTML = cc.Comment_Html,
                                                     DateTimeSubmitted = cc.DateTime_Submitted
                                                 }).ToList(),
                            }).FirstOrDefault();

            if (getTitle != null)
            {
                var getNext = (from xx in db.PS_Cards
                               where xx.orderby_id < getTitle.orderbyId
                               orderby xx.orderby_id descending
                               select new CardViewModel
                               {
                                   orderbyId = xx.orderby_id,
                                   uid = xx.uid,
                                   Title = xx.Title,
                                   DateTimeAdded = (DateTime)xx.DateTime_Added,
                               }).FirstOrDefault();

                if (getNext == null)
                {
                    getNext = (from xx in db.PS_Cards
                               where xx.orderby_id > getTitle.orderbyId
                               orderby xx.orderby_id descending
                               select new CardViewModel
                               {
                                   orderbyId = xx.orderby_id,
                                   uid = xx.uid,
                                   Title = xx.Title,
                                   DateTimeAdded = (DateTime)xx.DateTime_Added,
                               }).FirstOrDefault();
                }

                var getPrev = (from xx in db.PS_Cards
                               where xx.orderby_id > getTitle.orderbyId
                               orderby xx.orderby_id
                               select new CardViewModel
                               {
                                   orderbyId = xx.orderby_id,
                                   uid = xx.uid,
                                   Title = xx.Title,
                                   DateTimeAdded = (DateTime)xx.DateTime_Added,
                               }).FirstOrDefault();
                if (getPrev == null)
                {
                    getPrev = (from xx in db.PS_Cards
                               where xx.orderby_id < getTitle.orderbyId
                               orderby xx.orderby_id
                               select new CardViewModel
                               {
                                   orderbyId = xx.orderby_id,
                                   uid = xx.uid,
                                   Title = xx.Title,
                                   DateTimeAdded = (DateTime)xx.DateTime_Added,
                               }).FirstOrDefault();
                }

                var getFav = (from xx in db.PS_Favorites
                              where xx.Item_Guid == id
                              where xx.User_ID == SupportFramework.DataAccessGlobal.UserID()
                              select xx.uid).FirstOrDefault();

                ViewData["Favorite"] = getFav;

                ViewData["Next"] = getNext;
                ViewData["Previous"] = getPrev;
                ViewData["Poster"] = getTitle;
                ViewData["Tags"] = getTitle.Tags;
                return View(ViewData);
            }
            return Redirect(Url.RouteUrl(new { controller = "Home", action = "Random" }));
        }


        public ActionResult PostCards(Guid id, string title)
        {
            Code.PSDataContext db = new Code.PSDataContext();
            var getTitle = (from xx in db.PS_Cards
                            from aa in db.PS_Postcards
                                                        where xx.Card_Id == aa.uid
                            where xx.uid == id
                            select new CardViewModel
                            {
                                orderbyId = xx.orderby_id,
                                CardCount = (from qq in db.PS_Cards
                                             select qq.uid).Count(),
                                uid = xx.uid,
                                Number = (from qq in db.PS_Cards
                                          where qq.orderby_id > xx.orderby_id
                                          select qq.uid).Count(),
                                FavoriteCount = xx.Favorites,
                                Favorites = (from nn in db.PS_Favorites
                                             where nn.Item_Guid == xx.uid
                                             select new FavoriteViewModel
                                             {
                                                 Item_Guid = nn.Item_Guid,
                                                 uid = nn.uid,
                                                 User_Id = nn.User_ID
                                             }).ToList(),
                                Title = xx.Title,
                                Url = aa.PostCard_UrlNew,
                                NickName = xx.NickName,
                                PostCard_Id = xx.Card_Id,
                                UserId = xx.User_ID,
                                IPAddress = xx.IP_Address,
                                DateTimeAdded = (DateTime)xx.DateTime_Added,
                                VotesUp = xx.Votes_Up,
                                VotesDown = xx.Votes_Down,
                                Views = xx.Views,
                                Votes = (from nn in db.PS_Votes
                                         where nn.Item_Guid == xx.uid
                                         select new VoteViewModel
                                         {
                                             IPAddress = nn.User_IP_Address,
                                             Vote = nn.Vote,
                                             ItemGuid = nn.Item_Guid,
                                             UserID = nn.User_ID,
                                         }).ToList(),
                                Tags = (from zz in db.PS_Tags
                                        from cc in db.PS_Tags_Pulls
                                        where zz.Tag_ID == cc.uid
                                        where zz.Item_ID == xx.uid
                                        select new TagViewModel
                                        {
                                            TagName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cc.Tag_Name).ToSearchEngineFriendly(),
                                            TagID = zz.Tag_ID,
                                            TagCount = (from yy in db.PS_Tags
                                                        where yy.Tag_ID == zz.Tag_ID
                                                        select yy.Tag_ID).Count()
                                        }).Distinct().ToList(),
                                Comments = (from cc in db.PS_Comments
                                            where cc.Comment_To_ID == xx.uid
                                            orderby cc.DateTime_Submitted
                                            select new CommentViewModel
                                            {
                                                VotesUp = cc.Votes_Up,
                                                VotesDown = cc.Votes_Down,
                                                CommentID = cc.uid,
                                                CommentHTML = cc.Comment_Html,
                                                DateTimeSubmitted = cc.DateTime_Submitted
                                            }).ToList(),
                                ContentSurvey = (from cc in db.PS_Comments
                                                 where cc.Content_To_Id == xx.uid
                                                 orderby cc.Votes_Up descending
                                                 select new CommentViewModel
                                                 {
                                                     VotesUp = cc.Votes_Up,
                                                     VotesDown = cc.Votes_Down,
                                                     ContentID = cc.uid,
                                                     CommentHTML = cc.Comment_Html,
                                                     DateTimeSubmitted = cc.DateTime_Submitted
                                                 }).ToList(),
                            }).FirstOrDefault();

            if (getTitle != null)
            {
                var getNext = (from xx in db.PS_Cards
                               where xx.orderby_id < getTitle.orderbyId
                               orderby xx.orderby_id descending
                               select new CardViewModel
                               {
                                   orderbyId = xx.orderby_id,
                                   uid = xx.uid,
                                   Title = xx.Title,
                                   DateTimeAdded = (DateTime)xx.DateTime_Added,
                               }).FirstOrDefault();

                if (getNext == null)
                {
                    getNext = (from xx in db.PS_Cards
                               where xx.orderby_id > getTitle.orderbyId
                               orderby xx.orderby_id descending
                               select new CardViewModel
                               {
                                   orderbyId = xx.orderby_id,
                                   uid = xx.uid,
                                   Title = xx.Title,
                                   DateTimeAdded = (DateTime)xx.DateTime_Added,
                               }).FirstOrDefault();
                }

                var getPrev = (from xx in db.PS_Cards
                               where xx.orderby_id > getTitle.orderbyId
                               orderby xx.orderby_id
                               select new CardViewModel
                               {
                                   orderbyId = xx.orderby_id,
                                   uid = xx.uid,
                                   Title = xx.Title,
                                   DateTimeAdded = (DateTime)xx.DateTime_Added,
                               }).FirstOrDefault();
                if (getPrev == null)
                {
                    getPrev = (from xx in db.PS_Cards
                               where xx.orderby_id < getTitle.orderbyId
                               orderby xx.orderby_id 
                               select new CardViewModel
                               {
                                   orderbyId = xx.orderby_id,
                                   uid = xx.uid,
                                   Title = xx.Title,
                                   DateTimeAdded = (DateTime)xx.DateTime_Added,
                               }).FirstOrDefault();
                }

                var getFav = (from xx in db.PS_Favorites
                              where xx.Item_Guid == id
                              where xx.User_ID == SupportFramework.DataAccessGlobal.UserID()
                              select xx.uid).FirstOrDefault();

                ViewData["Favorite"] = getFav;

                ViewData["Next"] = getNext;
                ViewData["Previous"] = getPrev;
                ViewData["Poster"] = getTitle;
                ViewData["Tags"] = getTitle.Tags;
                return View(ViewData);
            }
            return Redirect(Url.RouteUrl(new { controller = "Home", action = "Random" }));

        }

        public ActionResult Cards(int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            Code.PSDataContext db = new Code.PSDataContext();
            var getTags = (from xx in db.PS_Tags
                           from zz in db.PS_Tags_Pulls
                           where xx.Tag_ID == zz.uid
                           select new TagViewModel
                           {
                               TagName = zz.Tag_Name,
                               TagID = xx.Tag_ID,
                               TagCount = (from yy in db.PS_Tags
                                           where yy.Tag_ID == xx.Tag_ID
                                           select yy.Tag_ID).Count()
                           }).Distinct().Take(20).ToList();

            var getTitles = (from xx in db.PS_Cards
                             from aa in db.PS_Postcards
                             orderby xx.orderby_id
                             where xx.Card_Id == aa.uid
                             select new CardViewModel
                             {
                                 uid = xx.uid,
                                 FavoriteCount = xx.Favorites,
                                 Favorites = (from nn in db.PS_Favorites
                                              where nn.Item_Guid == xx.uid
                                              select new FavoriteViewModel
                                              {
                                                  Item_Guid = nn.Item_Guid,
                                                  uid = nn.uid,
                                                  User_Id = nn.User_ID,

                                              }).ToList(),
                                 Title = xx.Title,
                                 Url = aa.PostCard_UrlNew,
                                 NickName = xx.NickName,
                                 PostCard_Id = xx.Card_Id,
                                 UserId = xx.User_ID,
                                 IPAddress = xx.IP_Address,
                                 DateTimeAdded = (DateTime)xx.DateTime_Added,
                                 VotesUp = xx.Votes_Up,
                                 VotesDown = xx.Votes_Down,
                                 Views = xx.Views,
                                 Votes = (from rr in db.PS_Votes
                                          where rr.Item_Guid == xx.uid
                                          select new VoteViewModel
                                          {
                                              IPAddress = rr.User_IP_Address,
                                              ItemGuid = rr.Item_Guid,
                                              UserID = rr.User_ID,
                                              Vote = rr.Vote
                                          }).ToList(),
                                 Tags = (from zz in db.PS_Tags
                                         from cc in db.PS_Tags_Pulls
                                         where zz.Tag_ID == cc.uid
                                         where zz.Item_ID == xx.uid
                                         select new TagViewModel
                                         {
                                             TagName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cc.Tag_Name).ToSearchEngineFriendly(),
                                             TagID = zz.Tag_ID,
                                             TagCount = (from yy in db.PS_Tags
                                                         where yy.Tag_ID == zz.Tag_ID
                                                         select yy.Tag_ID).Count()
                                         }).Distinct().ToList(),
                                 CommentCount = (from cc in db.PS_Comments
                                                 where cc.Comment_To_ID == xx.uid
                                                 select cc).Count(),
                             }).ToPagedList(currentPageIndex, defaultPageSize);


            ViewData["Titles"] = getTitles;
            ViewData["Tags"] = getTags;
            ViewData["Page"] = page;
            return View(ViewData);
        }

        private const int defaultPageSize = 30;
        private IList<CardViewModel> allTitles = new List<CardViewModel>();
        public ActionResult Top(int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            Code.PSDataContext db = new Code.PSDataContext();
            var getTags = (from xx in db.PS_Tags
                           from zz in db.PS_Tags_Pulls
                           where xx.Tag_ID == zz.uid
                           select new TagViewModel
                           {
                               TagName = zz.Tag_Name,
                               TagID = xx.Tag_ID,
                               TagCount = (from yy in db.PS_Tags
                                           where yy.Tag_ID == xx.Tag_ID
                                           select yy.Tag_ID).Count()
                           }).Distinct().Take(20).ToList();

            var getTitles = (from xx in db.PS_Cards
                             from aa in db.PS_Postcards
                             orderby xx.Votes_Up descending
                             where xx.Card_Id == aa.uid
                             select new CardViewModel
                             {
                                 uid = xx.uid,
                                 FavoriteCount = xx.Favorites,
                                 Favorites = (from nn in db.PS_Favorites
                                              where nn.Item_Guid == xx.uid
                                              select new FavoriteViewModel
                                              {
                                                  Item_Guid = nn.Item_Guid,
                                                  uid = nn.uid,
                                                  User_Id = nn.User_ID,

                                              }).ToList(),
                                 Title = xx.Title,
                                 Url = aa.PostCard_UrlNew,
                                 NickName = xx.NickName,
                                 PostCard_Id = xx.Card_Id,
                                 UserId = xx.User_ID,
                                 IPAddress = xx.IP_Address,
                                 DateTimeAdded = (DateTime)xx.DateTime_Added,
                                 VotesUp = xx.Votes_Up,
                                 VotesDown = xx.Votes_Down,
                                 Views = xx.Views,
                                 Votes = (from rr in db.PS_Votes
                                          where rr.Item_Guid == xx.uid
                                          select new VoteViewModel
                                          {
                                              IPAddress = rr.User_IP_Address,
                                              ItemGuid = rr.Item_Guid,
                                              UserID = rr.User_ID,
                                              Vote = rr.Vote
                                          }).ToList(),
                                 Tags = (from zz in db.PS_Tags
                                         from cc in db.PS_Tags_Pulls
                                         where zz.Tag_ID == cc.uid
                                         where zz.Item_ID == xx.uid
                                         select new TagViewModel
                                         {
                                             TagName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cc.Tag_Name).ToSearchEngineFriendly(),
                                             TagID = zz.Tag_ID,
                                             TagCount = (from yy in db.PS_Tags
                                                         where yy.Tag_ID == zz.Tag_ID
                                                         select yy.Tag_ID).Count()
                                         }).Distinct().ToList(),
                                 CommentCount = (from cc in db.PS_Comments
                                                 where cc.Comment_To_ID == xx.uid
                                                 select cc).Count(),
                             }).ToPagedList(currentPageIndex, defaultPageSize);


            ViewData["Titles"] = getTitles;
            ViewData["Tags"] = getTags;
            return View(ViewData);
        }
        public ActionResult Add()
        {
            Random r = new Random();

            Code.PSDataContext db = new Code.PSDataContext();
            var getImages = (from xx in db.PS_Postcards
                             select new ImagesViewModel
                             {
                                 DateTimeAdded = xx.DateTime_Added,
                                 Id = xx.uid,
                                 IpAddress = xx.Ip_Address_Added,
                                 Name = xx.Name,
                                 SavePath = xx.Save_Location,
                                 Url = xx.PostCard_Url,
                                 UserIDAdded = xx.User_ID_Added,
                             }).OrderBy(x => r.NextDouble()).Take(10);
            ViewData["Images"] = getImages;

            return View(ViewData);
        }


    }
}
