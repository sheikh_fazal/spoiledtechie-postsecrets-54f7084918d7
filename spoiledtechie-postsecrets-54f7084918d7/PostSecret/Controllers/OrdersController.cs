﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.IO;

using PostSecret.Code.SupportFramework;
using PostSecret.ViewModels;

using Boomers.Utilities.Text;
using Boomers.Utilities.Guids;
using Boomers.Paypal;


namespace PostSecret.Controllers
{
    public class OrdersController : Controller
    {
        public void PostPaypal(FormCollection form)
        {
            GlobalDataContext db = new GlobalDataContext();

            Global_Entity entity = new Global_Entity();
            Global_Transaction transaction = new Global_Transaction();
            Global_Entity_Address address = new Global_Entity_Address();
            Global_Entity_Email email = new Global_Entity_Email();

            entity.First_Name = form["first_name"];
            entity.Entity_Id = Guid.NewGuid();
            entity.Last_Name = form["last_name"];
            entity.DateTime_Added = DateTime.UtcNow;

            address.Address_Id = Guid.NewGuid();
            address.Address1 = form["address1"];
            address.Address2 = form["address2"];
            address.City = form["city"];
            address.Country = form["country"];
            address.DateTime_Added = DateTime.UtcNow;
            address.Entity_Id = entity.Entity_Id;
            address.State = form["state"];
            address.Zip = Convert.ToInt32(form["zip"]);


            email.DateTime_Added = DateTime.UtcNow;
            email.Email_Address = form["email"];
            email.Email_Id = Guid.NewGuid();
            email.Entity_Id = entity.Entity_Id;


            transaction.Address_Id_Shipped_To = address.Address_Id;
            transaction.Application_Id = SupportFramework.DataAccessGlobal.ApplicationId();
            transaction.Application_Name = System.Web.Security.Membership.ApplicationName.ToLower();
            transaction.Costs = Convert.ToDecimal(1.97);
            transaction.DateTime_Ordered = DateTime.UtcNow;
            transaction.Entity_Id = entity.Entity_Id;
            transaction.Transaction_Id = Guid.NewGuid();
            transaction.Shipping_Handling = Convert.ToDecimal(0.00);
            transaction.Buying_Item = form["item_number"].RemoveDashes();
            
            //var checkEmailsForAlreadySignedUp = (from xx in db.Global_Entity_Emails
            //                                     where xx.Email_Address.ToLower() == form.Get("email").ToLower().Trim()
            //                                     select xx.Entity_Id).FirstOrDefault();
            ////if there is no current user that has used the website to buy.
            //if (checkEmailsForAlreadySignedUp == new Guid())
            //{
                db.Global_Entities.InsertOnSubmit(entity);
                db.Global_Entity_Addresses.InsertOnSubmit(address);
                db.Global_Entity_Emails.InsertOnSubmit(email);
                db.Global_Transactions.InsertOnSubmit(transaction);
                db.SubmitChanges();
            //}
            //else //user exists in out records.
            //{

            //    //checks if the address is the same.
            //    var checkUsersAddress = (from xx in db.Global_Entity_Addresses
            //                             where xx.Entity_Id == checkEmailsForAlreadySignedUp
            //                             select xx).ToList();
            //    bool isUsersAddressSame = false;
            //    Guid userAddressId;
            //    for (int i = 0; i < checkUsersAddress.Count; i++) //goes through the addresses.  
            //    {
            //        //checks if all values in the address are correct.
            //        if (address.Address1.ToLower() == checkUsersAddress[i].Address1.ToLower() && address.City.ToLower() == checkUsersAddress[i].City.ToLower() && address.State.ToLower() == checkUsersAddress[i].State.ToLower() && address.Zip == checkUsersAddress[i].Zip)
            //        {
            //            isUsersAddressSame = true;
            //            userAddressId = checkUsersAddress[i].Address_Id;
            //        }
            //    }

            //    var checkifEntityIsSame = (from xx in db.Global_Entities
            //                               where xx.Entity_Id == checkEmailsForAlreadySignedUp
            //                               select xx).FirstOrDefault();
            //    //updates the last name.
            //    if (checkifEntityIsSame.Last_Name.ToLower() != entity.Last_Name.ToLower())
            //    {
            //        checkifEntityIsSame.Last_Name = entity.Last_Name;
            //    }
            //    //updates the first name if not the same.
            //    if (checkifEntityIsSame.First_Name.ToLower() != entity.First_Name.ToLower())
            //    {
            //        checkifEntityIsSame.First_Name = entity.First_Name;
            //    }

            //    //inserts the new address and changes it for the transaction
            //    if (!isUsersAddressSame)
            //    {
            //        db.Global_Entity_Addresses.InsertOnSubmit(address);
            //        transaction.Address_Id_Shipped_To = address.Address_Id;
            //    }
            //    transaction.Entity_Id = checkEmailsForAlreadySignedUp;
                                
            //    db.Global_Transactions.InsertOnSubmit(transaction);
            //}
            //db.SubmitChanges();

            Boomers.Paypal.HandleSend sendingPayPal = new Boomers.Paypal.HandleSend();
            sendingPayPal.Amount = 1.97;
            sendingPayPal.BuyerEmailAddress = email.Email_Address;
            sendingPayPal.CancelUrl = "http://postsecretcollection.com/Orders/Return/" + form["item_number"].RemoveDashes() + "?PayPal="+PaypalSettings.ReturnStatus.Cancel.ToString()+"&transId=" + transaction.Transaction_Id.RemoveDashes();
            sendingPayPal.Code = Boomers.Paypal.PaypalSettings.CurrencyCode.USD;
            sendingPayPal.ItemName = "PostSecret PostCard #" + form["item_number"].RemoveDashes();
            sendingPayPal.Mode = Boomers.Paypal.PaypalSettings.PaypalMode.live;
            sendingPayPal.ReturnUrl = "http://postsecretcollection.com/Orders/Return/" + form["item_number"].RemoveDashes() + "?PayPal=" + PaypalSettings.ReturnStatus.Success.ToString() + "&transId=" + transaction.Transaction_Id.RemoveDashes();
            //sendingPayPal.SellerEmailAddress = "seller_1302661346_biz@gmail.com";
            sendingPayPal.SellerEmailAddress = "cheetahtech@gmail.com";
            sendingPayPal.InvoiceNumber = transaction.Transaction_Id.ToString();
            CardViewModel cvm = new CardViewModel();
            cvm.GetCard(new Guid(form["item_number"].ToString()));
            sendingPayPal.LogoUrl = cvm.Url;
            sendingPayPal.SendToPaypal(Boomers.Paypal.PaypalSettings.PaypalMode.live);
        }


        public ActionResult Return(string id)
        {
            CardViewModel cvm = new CardViewModel();
            ViewData["card"] = cvm.GetCard(new Guid(id));

            GlobalDataContext db = new GlobalDataContext();
            var query = Request.QueryString;
            var isSuccessful = query["PayPal"];
            var transId = query["transId"];
            if (isSuccessful == PaypalSettings.ReturnStatus.Cancel.ToString())
            {
                var getTransaction = (from xx in db.Global_Transactions
                                      where xx.Transaction_Id == new Guid(transId)
                                      select xx).FirstOrDefault();
                //if transaction doesn't exist.
                if (getTransaction == null)
                    return View();

                getTransaction.Status = "Canceled";

                db.SubmitChanges();
                return RedirectToAction("~/Index");
            }
            else if (isSuccessful == PaypalSettings.ReturnStatus.Success.ToString())
            {
                var getTransaction = (from xx in db.Global_Transactions
                                      where xx.Transaction_Id == new Guid(transId)
                                      select xx).FirstOrDefault();
                //if transaction doesn't exist.
                if (getTransaction == null)
                    return View();

                getTransaction.Status = "Returned";

                var getEntity = (from xx in db.Global_Entities
                                 where getTransaction.Entity_Id == xx.Entity_Id
                                 select xx).FirstOrDefault();

                var getEmail = (from xx in db.Global_Entity_Emails
                                where xx.Entity_Id == getEntity.Entity_Id
                                select xx).FirstOrDefault();

                var getAddress = (from xx in db.Global_Entity_Addresses
                                  where xx.Entity_Id == getEntity.Entity_Id
                                  select xx).FirstOrDefault();

                ViewData.Add("entity", getEntity);
                ViewData.Add("email", getEmail);
                ViewData.Add("address", getAddress);

                db.SubmitChanges();
                return View(ViewData);
            }
            return View();
        }

        public ActionResult Buy(string id)
        {
            CardViewModel cvm = new CardViewModel();
            ViewData["card"] = cvm.GetCard(new Guid(id));



            return View(ViewData);
        }
    }
}
//var gateway = new BraintreeGateway
//{
//    Environment = Braintree.Environment.SANDBOX,
//    MerchantId = "the_merchant_id",
//    PublicKey = "a_public_key",
//    PrivateKey = "a_private_key"
//};

//var request = new TransactionRequest
//{
//    Amount = 1000M,
//    CreditCard = new TransactionCreditCardRequest
//    {
//        Number = "4111111111111111",
//        ExpirationDate = "05/2012"
//    }
//};

//Result<Transaction> result = gateway.Transaction.Sale(request);

//if (result.IsSuccess())
//{
//    Transaction transaction = result.Target;
//    Console.WriteLine("Success!: " + transaction.Id);
//}
//else if (result.Transaction != null)
//{
//    Console.WriteLine("Message: " + result.Message);
//    Transaction transaction = result.Transaction;
//    Console.WriteLine("Error processing transaction:");
//    Console.WriteLine("  Status: " + transaction.Status);
//    Console.WriteLine("  Code: " + transaction.ProcessorResponseCode);
//    Console.WriteLine("  Text: " + transaction.ProcessorResponseText);
//}
//else
//{
//    Console.WriteLine("Message: " + result.Message);
//    foreach (ValidationError error in result.Errors.DeepAll())
//    {
//        Console.WriteLine("Attribute: " + error.Attribute);
//        Console.WriteLine("  Code: " + error.Code);
//        Console.WriteLine("  Message: " + error.Message);
//    }
//}