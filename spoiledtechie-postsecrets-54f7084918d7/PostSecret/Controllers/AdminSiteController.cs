﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PostSecret.ViewModels;
using Boomers.Utilities.Guids;
using PostSecret.Code.SupportFramework;

namespace PostSecret.Controllers
{
    public class AdminSiteController : Controller
    {
        public ActionResult Default()
        {
            return View(ViewData);
        }

        [RequiresRole(RoleToCheckFor = "admin")]
        public ActionResult RemoveFlag(int flagID)
        {
            GlobalDataContext db = new GlobalDataContext();
            var getFlag = (from xx in db.Global_Flaggeds
                           where xx.uid == flagID
                           where xx.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                           select xx).FirstOrDefault();
            db.Global_Flaggeds.DeleteOnSubmit(getFlag);
            db.SubmitChanges();
            return Json(new { message = "Flag Removed" }, JsonRequestBehavior.AllowGet);
        }
        [RequiresRole(RoleToCheckFor = "admin")]
        public ActionResult RemoveFlaggedItem(Guid flaggedID, string type)
        {
            Code.PSDataContext db = new Code.PSDataContext();
            if (type == "comment")
            {
                var getFlag = (from xx in db.PS_Comments
                               where xx.uid == flaggedID
                               select xx).FirstOrDefault();
                db.PS_Comments.DeleteOnSubmit(getFlag);
            }
            else 
            {
                var getFlag = (from xx in db.PS_Cards
                               where xx.uid == flaggedID
                               select xx).FirstOrDefault();
                if(getFlag!=null)
                    db.PS_Cards.DeleteOnSubmit(getFlag);
            }
            GlobalDataContext gdb = new GlobalDataContext();
            var getFlagged = (from xx in gdb.Global_Flaggeds
                              where xx.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                              where xx.Flagged_Id == flaggedID
                              select xx).FirstOrDefault();
            gdb.Global_Flaggeds.DeleteOnSubmit(getFlagged);
            gdb.SubmitChanges();
            db.SubmitChanges();
            return Json(new { message = "Flagged Item Removed" }, JsonRequestBehavior.AllowGet);
        }

        

        public ActionResult FlaggedContent()
        {
            GlobalDataContext db = new GlobalDataContext();
            Code.PSDataContext idb = new Code.PSDataContext();
            var flags = (from xx in db.Global_Flaggeds
                         where xx.Application_Id == SupportFramework.DataAccessGlobal.ApplicationId()
                         select new FlaggedViewModel
                         {
                             Uid = xx.uid,
                             DateTime_Flagged = xx.DateTime_Flagged,
                             FlaggedID = xx.Flagged_Id,
                             Type = xx.Type,
                             data = xx.Data
                         }).ToList();
            foreach (var item in flags)
            {
                item.Comment = (from yy in idb.PS_Comments
                                where yy.uid == item.FlaggedID
                                select new CommentViewModel
                                {
                                    CommentHTML = yy.Comment_Html,
                                    CommentID = yy.uid
                                }).FirstOrDefault();
                item.Card = (from yy in idb.PS_Cards
                             from zz in idb.PS_Postcards
                             where yy.Card_Id == zz.uid
                             where yy.uid == item.FlaggedID
                             select new CardViewModel
                             {
                                 uid = yy.uid,

                                 Title = yy.Title,
                                 Url = zz.PostCard_UrlNew,
                             }).FirstOrDefault();

                //item.Story= (from yy in idb.PS_Stories
                //        where yy.uid == item.FlaggedID
                //             select new StoryViewModel
                //        {
                //              gameID = yy.uid.RemoveDashes(),
                //            gameName = yy.game_Name,
                //            gameShortSummary = yy.game_Info
                //        }).FirstOrDefault();
            }
            ViewData["Flagged"] = flags;
            return View(ViewData);
        }

    }
}
