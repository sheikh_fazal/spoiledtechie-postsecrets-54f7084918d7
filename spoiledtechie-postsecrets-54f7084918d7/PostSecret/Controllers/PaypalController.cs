﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


using PostSecret.Code.SupportFramework;

using Boomers.Utilities.Documents;
using Boomers.Utilities.Communications;

namespace PostSecret.Controllers
{
    public class PaypalController : Controller
    {
        //
        // GET: /Paypal/

        public ActionResult IPN()
        {
            string log = "";
            try
            {
                Boomers.Paypal.HandleIPN ipn = new Boomers.Paypal.HandleIPN(Boomers.Paypal.PaypalSettings.PaypalMode.live);
                ipn.FromEmail = "spoiledtechie@gmail.com";
                ipn.BusinessWebsite = "PostSecretCollection.com";
                ipn.ToEmail = "cheetahtech@gmail.com";
                //ipn.ToEmail = "seller_1302661346_biz@gmail.com";
                ipn.FromEmailPassword = "email123";
                ipn.SmtpHost = "localhost";
                ipn.SmtpPort = 25;
                ipn.FromEmailUserName = "email";
                ipn.CheckStatus(Boomers.Paypal.PaypalSettings.PaypalMode.live, "The postcard will be queued for printing and shipping within the hour.<br/><br/>Your Transaction Code is: "+ipn.Invoice+"<br/>Thank you so much for buying the card.<br/><br/>With each purchase, proceeds go towards Suicide Prevention.<br/><br/>Your Friends at PostSecretCollection.com");

                log += DateTime.UtcNow + ":response:" + ipn.Response + "<br/>";
                log += DateTime.UtcNow + ":paymentStatus:" + ipn.PaymentStatus + "<br/>";

                GlobalDataContext db = new GlobalDataContext();

                InsertIPNMessageIntoDB(ipn, db, Request, log);

                if (ipn.Response == "VERIFIED")
                {
                    if (ipn.PaymentStatus == "Completed")
                    {
                        log += DateTime.UtcNow + ":Invoice:" + ipn.Invoice + "<br/>";

                        com.postalmethods.api.PostalWS post = new com.postalmethods.api.PostalWS();
                        com.postalmethods.api.WorkMode workMode = com.postalmethods.api.WorkMode.Development;

                        var getTransaction = (from xx in db.Global_Transactions
                                              where xx.Transaction_Id == new Guid(ipn.Invoice)
                                              select xx).FirstOrDefault();

                        if (getTransaction != null)
                        {
                            log += DateTime.UtcNow + ":" + getTransaction.Address_Id_Shipped_To + "<br/>";

                            var getAddress = (from xx in db.Global_Entity_Addresses
                                              where xx.Address_Id == getTransaction.Address_Id_Shipped_To
                                              select xx).FirstOrDefault();
                            log += DateTime.UtcNow + ":Address:" + getTransaction.Address_Id_Shipped_To + "<br/>";
                            log += DateTime.UtcNow + ":Entity:" + getTransaction.Entity_Id + "<br/>";

                            var getEntity = (from xx in db.Global_Entities
                                             where xx.Entity_Id == getTransaction.Entity_Id
                                             select xx).FirstOrDefault();

                            var getEmail = (from xx in db.Global_Entity_Emails
                                            where xx.Entity_Id == getEntity.Entity_Id
                                            select xx).FirstOrDefault();
                            log += DateTime.UtcNow + ":Item:" + getTransaction.Buying_Item + "<br/>";
                            PostSecret.ViewModels.CardViewModel cvm = new ViewModels.CardViewModel();
                            cvm.GetCard(new Guid(getTransaction.Buying_Item));

                            log += DateTime.UtcNow + ":postCardId:" + cvm.PostCard_Id + "<br/>";
                            log += DateTime.UtcNow + ":savePath:" + cvm.SavePath + "<br/>";

                            FileInfo fileInfo = new FileInfo(cvm.SavePath);
                            post.SendPostcardAndAddressCompleted += new com.postalmethods.api.SendPostcardAndAddressCompletedEventHandler(post_SendPostcardAndAddressCompleted);
                            string postalFront = @"C:\HostingSpaces\pio.scott@gmail.com\images.codingforcharity.org\wwwroot\ps\PostSecretAddressFront.jpg";

                            log += DateTime.UtcNow + ":extension:" + fileInfo.Extension + "<br/>";
                            log += DateTime.UtcNow + ":PageSize:" + com.postalmethods.api.ImageSideScaling.FitToPage + "<br/>";
                            log += DateTime.UtcNow + ":PostalFront:" + postalFront.ToByteArray() + "<br/>";
                            log += DateTime.UtcNow + ":SavePath:" + cvm.SavePath.ToByteArray() + "<br/>";
                            log += DateTime.UtcNow + ":WorkMode:" + com.postalmethods.api.WorkMode.Production + "<br/>";
                            log += DateTime.UtcNow + ":Color:" + com.postalmethods.api.PrintColor.FullColor + "<br/>";
                            log += DateTime.UtcNow + ":CardSize:" + com.postalmethods.api.PostcardSize.Default + "<br/>";
                            log += DateTime.UtcNow + ":FirstName:" + getEntity.First_Name + "<br/>";
                            log += DateTime.UtcNow + ":LastName:" + getEntity.Last_Name + "<br/>";
                            log += DateTime.UtcNow + ":SavePath:" + getAddress.Address1 + "<br/>";
                            log += DateTime.UtcNow + ":SavePath:" + getAddress.Address2 + "<br/>";
                            log += DateTime.UtcNow + ":SavePath:" + getAddress.City + "<br/>";
                            log += DateTime.UtcNow + ":SavePath:" + getAddress.State + "<br/>";
                            log += DateTime.UtcNow + ":SavePath:" + getAddress.Zip.ToString() + "<br/>";
                            log += DateTime.UtcNow + ":SavePath:" + getAddress.Country + "<br/>";

                            post.SendPostcardAndAddressAsync("spoiledtechie", "cheetah1", null, fileInfo.Extension, cvm.SavePath.ToByteArray(), com.postalmethods.api.ImageSideScaling.FitToPage, ".jpg", postalFront.ToByteArray(), com.postalmethods.api.WorkMode.Production, com.postalmethods.api.PrintColor.FullColor, com.postalmethods.api.PostcardSize.Default, com.postalmethods.api.MailingPriority.Default, getEntity.First_Name + " " + getEntity.Last_Name, null, null, getAddress.Address1, getAddress.Address2, getAddress.City, getAddress.State, getAddress.Zip.ToString(), getAddress.Country);
                        }
                        else //transaction doesn't exist and therefor needs to be reviewed.
                        {

                            for (int i = 0; i < Request.Form.Keys.Count; i++)
                            {
                                log += "key: " + Request.Form.GetKey(i) + " value: " + Request.Form.Get(i);
                            }
                            //Email.EmailPassword = "cheetah1";
                            //Email.EmailUsername = "spoiledtechie@gmail.com";
                            //Email.FromEmail = "spoiledtechie@gmail.com";
                            //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "Postsecret Transaction wasn't Found.", log + "<br/><br/>", "PostSecretCollection.com");
                        }
                    }
                    else
                    {

                        for (int i = 0; i < Request.Form.Keys.Count; i++)
                        {
                            log += "key: " + Request.Form.GetKey(i) + " value: " + Request.Form.Get(i);
                        }
                        //Email.EmailPassword = "cheetah1";
                        //Email.EmailUsername = "spoiledtechie@gmail.com";
                        //Email.FromEmail = "spoiledtechie@gmail.com";
                        //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "Postsecret Transaction is not completed.", log + "<br/><br/>", "PostSecretCollection.com");
                    }
                }
                else
                {
                    for (int i = 0; i < Request.Form.Keys.Count; i++)
                    {
                        log += "key: " + Request.Form.GetKey(i) + " value: " + Request.Form.Get(i);
                    }
                    //Email.EmailPassword = "cheetah1";
                    //Email.EmailUsername = "spoiledtechie@gmail.com";
                    //Email.FromEmail = "spoiledtechie@gmail.com";
                    //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "Postsecret Transaction was not verified.", log + "<br/><br/>", "PostSecretCollection.com");
                }



                //for (int i = 0; i < Request.Form.Keys.Count; i++)
                //{
                //    Boomers.Utilities.Documents.TextLogger.LogItem("paypalIPN", "key: " + Request.Form.GetKey(i) + " value: " + Request.Form.Get(i));
                //}

                //Boomers.Utilities.Documents.TextLogger.LogItem("paypalIPN", "stuffing DB");



            }
            catch (Exception e)
            {

                for (int i = 0; i < Request.Form.Keys.Count; i++)
                {
                    log += "key: " + Request.Form.GetKey(i) + " value: " + Request.Form.Get(i);
                }
                //Email.EmailPassword = "cheetah1";
                //Email.EmailUsername = "spoiledtechie@gmail.com";
                //Email.FromEmail = "spoiledtechie@gmail.com";
                //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "Postsecret Payment Insertion Failed.", log + "<br/><br/>" + e.ToString(), "PostSecretCollection.com");
                //Boomers.Utilities.Documents.TextLogger.LogItem("paypalIPN", e.ToString());
            }
            //Boomers.Utilities.Documents.TextLogger.LogItem("paypalIPN", "stuffed DB");
            return View();
        }
        /// <summary>
        /// Inserts the IPN message into the DB.
        /// </summary>
        /// <param name="ipn"></param>
        /// <param name="db"></param>
        /// <param name="request"></param>
        /// <param name="log"></param>
        private static void InsertIPNMessageIntoDB(Boomers.Paypal.HandleIPN ipn, GlobalDataContext db, HttpRequestBase request, string log)
        {
            try
            {
                Global_Paypal_IPN_Transaction paypal = new Global_Paypal_IPN_Transaction();
                paypal.ApplicationId = SupportFramework.DataAccessGlobal.ApplicationId();
                paypal.ApplicationName = System.Web.Security.Membership.ApplicationName.ToLower();

                if (ipn.Invoice != "")
                    paypal.Boomers_Paypal_Id_Invoice_Id = new Guid(ipn.Invoice);
                paypal.Business_Id = ipn.Business;
                paypal.Custom = ipn.Custom;
                paypal.DateTime_Received = DateTime.UtcNow.ToString();
                paypal.Item_Name = ipn.ItemName;
                paypal.Item_Number = ipn.ItemNumber;
                paypal.Memo = ipn.Memo;
                paypal.Notify_Version = ipn.NotifyVersion;
                paypal.Payer_Address = ipn.PayerAddress;
                paypal.Payer_Address_Status = ipn.PayerAddressStatus;
                paypal.Payer_Business_Name = ipn.PayerBusinessName;
                paypal.Payer_City = ipn.PayerCity;
                paypal.Payer_Country = ipn.PayerCountry;
                paypal.Payer_Country_Code = ipn.PayerCountryCode;
                paypal.Payer_Email = ipn.PayerEmail;
                paypal.Payer_First_Name = ipn.PayerFirstName;
                paypal.Payer_Id = ipn.PayerID;
                paypal.Payer_Last_Name = ipn.PayerLastName;
                paypal.Payer_Phone = ipn.PayerPhone;
                paypal.Payer_State = ipn.PayerState;
                paypal.Payer_Status = ipn.PayerStatus;
                paypal.Payer_Zip = ipn.PayerZipCode;
                paypal.Payment_Date = ipn.PaymentDate;
                paypal.Payment_Gross = ipn.PaymentGross;
                paypal.Payment_Status = ipn.PaymentStatus;
                paypal.Payment_Type = ipn.PaymentType;
                paypal.Paypal_Transaction_TXN_Id = ipn.TXN_ID;
                paypal.Paypals_Payment_Fee = ipn.PaymentFee;
                paypal.Pending_Reason = ipn.PendingReason;
                paypal.Post_Url = ipn.PostUrl;
                paypal.Quantity = ipn.Quantity;
                paypal.Quantity_Cart_Items = ipn.QuantityCartItems;
                paypal.Receiver_Email = ipn.ReceiverEmail;
                paypal.Receiver_Id = ipn.ReceiverID;
                paypal.Request_Length = ipn.RequestLength;
                paypal.Response = ipn.Response;
                paypal.Shipping_Method = ipn.ShippingMethod;
                paypal.Tax = ipn.Tax;
                paypal.To_Email = ipn.ToEmail;
                paypal.TXN_Type = ipn.TXN_Type;
                paypal.Verify_Sign = ipn.VerifySign;
                db.Global_Paypal_IPN_Transactions.InsertOnSubmit(paypal);

                db.SubmitChanges();
            }
            catch (Exception e)
            {

                for (int i = 0; i < request.Form.Keys.Count; i++)
                {
                    log += "key: " + request.Form.GetKey(i) + " value: " + request.Form.Get(i);
                }
                //Email.EmailPassword = "cheetah1";
                //Email.EmailUsername = "spoiledtechie@gmail.com";
                //Email.FromEmail = "spoiledtechie@gmail.com";
                //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "Postsecret Payment Insertion into DB Failed.", log + "<br/><br/>" + e.ToString(), "PostSecretCollection.com");
                //Boomers.Utilities.Documents.TextLogger.LogItem("paypalIPN", e.ToString());
            }
        }

        void post_SendPostcardAndAddressCompleted(object sender, com.postalmethods.api.SendPostcardAndAddressCompletedEventArgs e)
        {
            string log = "";
            log += "<br/>cancelled: " + e.Cancelled;
            log += "<br/>error: " + e.Error;
            log += "<br/>result: " + e.Result;
            log += "<br/>userState: " + e.UserState;
            log += "<br/> http://www.postalmethods.com/statuscodes";
            log += "<br/> If a result == a number, then most likely a success.";

            //Email.EmailPassword = "cheetah1";
            //Email.EmailUsername = "spoiledtechie@gmail.com";
            //Email.FromEmail = "spoiledtechie@gmail.com";
            //Email.SendGmail("Scott", "spoiledtechie@gmail.com", "Postsecret Postcard Sent to Printer.", log + "<br/><br/>", "PostSecretCollection.com");
        }

    }
}
