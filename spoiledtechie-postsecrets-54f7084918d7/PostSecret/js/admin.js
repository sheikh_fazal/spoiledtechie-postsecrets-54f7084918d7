﻿function Confirm_Truncate_Table(tableName, databaseName) {
    if (confirm("Truncate " + tableName + " on database " + databaseName + "?") == true) {
        admin.ConfirmTruncateTable(tableName, databaseName, onWSCompleteTruncate);
    }
    else
        return false;
}
function onWSCompleteTruncate() {
    window.location.href = 'Database.aspx';
}


function ResetUsersAccount(username, appID) {
    admin.ResetUserAccountSettings(username, appID, onWSSuccess);
}

//UAdmin Functions
//
//
//
//
function DisconnectProvFromUser(provinceID) {
    admin.DisconnectProvinceFromUser(provinceID, onWSSuccess);
}
function onWSSuccess(results) {
    if (results === true)
        alert("deleted");
}


function OnWSSubmission(results) {

    document.getElementById("Submission").innerHTML = results.message;
}

function CreateRole() {
    $.getJSON("/Admin/CreateRoles", { role: document.getElementById("tbNewRole").value }, OnWSSubmission);
}
function DeleteRole(role) {
    $.getJSON("/Admin/DeleteRole", { id: role }, OnWSSubmission);
}
function SelectRole(role) {
    document.getElementById("RoleSelected").value = role;
    document.getElementById("Submission").innerHTML = role + " selected";
    $.getJSON("/Admin/GetUsersInRole", { id: role }, OnWSUsersInRole);
}
function OnWSUsersInRole(results) {
    document.getElementById("UsersInRole").innerHTML = results.table;
}
function RemoveUserFromRole(user, role) {
    $.getJSON("/Admin/RemoveUserFromRole", { user: user, role: role }, OnWSSubmission);
}
function RemoveFlag(flagID) {
    $.getJSON("/AdminSite/RemoveFlag", { flagId: flagID }, OnWSSubmission);
}
function RemoveFlaggedItem(flaggedID, type) {
    $.getJSON("/AdminSite/RemoveFlaggedItem", { flaggedID: flaggedID, type: type }, OnWSSubmission);
}

function SearchUserName() {
    $.getJSON("/Admin/SearchUsersByUserName", { userName: document.getElementById("tbUsernameSearch").value }, OnWSSearchUsers);
}
function SearchUserEmail() {
    $.getJSON("/Admin/SearchUsersByEmail", { email: document.getElementById("tbUserEmailSearch").value }, OnWSSearchUsers);
}
function OnWSSearchUsers(results) {
    $("#tblUsers tr:gt(0)").remove();
    $.each(results, function (i, user) {
        $.each(user, function (i, value) {
            $("#tblUsers tbody").append('<tr><td><span class="spanLink" id="spnSelectUser' + value.UserName + '" >Select</span></td><td>' + value.UserName + '</td><td><span onclick="javascript:SetUserApproval(\'' + value.UserName + '\');" class="spanLink" id="spnApproveUser' + value.UserName + '">' + value.Approved + '</span></td><td>' + value.Comments + '</td><td>' + value.CreateDate + '</td><td>' + value.Email + '</td><td>' + value.LastLogin + '</td><td>' + value.Locked + '</td><td>' + value.PasswordQuestion + '</td><td>' + value.UserOnline + '</td><td>' + value.Roles + '</td></tr>');
            document.getElementById("spnSelectUser" + value.UserName).setAttribute("onclick", "SelectUserName(this, '" + value.UserName + "');");
        });
    });
    $("#tblUsers tbody tr:odd").addClass('d0');
}

function SetUserApproval(userName) {
    $.getJSON("/Admin/SetUserApproval", { userName: userName }, OnWSUserApprovalReturn);
}
function OnWSUserApprovalReturn(results) {
    $("#spnApproveUser" + results.username).text(results.spnText);
    document.getElementById("Submission").innerHTML = results.message;
}

function SelectUserName(ctrl, userName) {
    document.getElementById("hiddenUserName").value = userName;
    $(ctrl).parent().parent().addClass('tblSelected');
    $.getJSON("/Admin/GetUsersNonRoles", { userName: userName }, OnWSNonRolesReturn);
}
function SelectErrorId(ctrl, errorId) {
    document.getElementById("hiddenErrorId").value = errorId;
    $(ctrl).parent().parent().addClass('tblSelected');
    $.getJSON("/Admin/GetError", { errorId: errorId }, OnWSErrorReturn);
}
function ReviewError(ctrl, errorId) {
    document.getElementById("hiddenErrorId").value = errorId;
    $(ctrl).parent().parent().remove();
    $.getJSON("/Admin/RemoveError", { errorId: errorId }, OnWSErrorRemovedReturn);
}

function OnWSErrorRemovedReturn(results) {
document.getElementById("Submission").innerHTML = results.message;
}
function OnWSErrorReturn(results) {
    document.getElementById("uid").innerHTML = results.Error.uid;
    document.getElementById("url").innerHTML = results.Error.Error_URL;
    document.getElementById("urlPrev").innerHTML = results.Error.Error_Previous_URL;
    document.getElementById("userName").innerHTML = results.Error.User_Name;
    document.getElementById("dateTime").innerHTML = results.Error.Date_Time;
    document.getElementById("failedAt").innerHTML = results.Error.Last_Exception;
    document.getElementById("rawData").innerHTML = results.Error.Error_Source;
    document.getElementById("reviewedDateTime").innerHTML = results.Error.uid;
    document.getElementById("reviewedUserId").innerHTML = results.Error.User_ID;
    document.getElementById("rawData").innerHTML = results.Error.Session_Data;
    }
function OnWSNonRolesReturn(results) {
    $.each(results, function (i, roles) {
        $.each(roles, function (i, value) {
            $("#tblRolesNotIn").html('<tr><td><span class="spanLink" onclick="javascript:AddRoleToUser(\'' + value.Role + '\')">Add</span></td><td>' + value.Role + '</td></tr>');
        });
    });
}
function AddRoleToUser(role) {
    $.getJSON("/Admin/AddRoleToUser", { userName: document.getElementById("hiddenUserName").value, role: role }, OnWSSubmission);
}