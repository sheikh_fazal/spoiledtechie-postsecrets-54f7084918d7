﻿function VoteUp(Id) {
    var img = $('#imgVoteUp'+Id).attr("src");
    $('#imgVoteUp' + Id).attr("src", img.replace("off", "on"));
    $.getJSON("/Utilities/VoteUp", { id: Id }, OnWSVoteUp);
}
function OnWSVoteUp(results) {
    var img = $('#imgVoteUp' + results.guid).attr("src");
    if (results.answer === true) {
        $('#imgVoteUp' + results.guid).attr("src", img.replace("off", "on"));
        $('#imgVoteUp' + results.guid).removeAttr("onclick");
        $('#VoteCountUp' + results.guid).html(results.voteCount);
    }
    else {
        $('#imgVoteUp' + results.guid).attr("src", img.replace("on", "off"));
    }
}

function VoteDown(Id) {
    var img = $('#imgVoteDown').attr("src");
    $('#imgVoteDown').attr("src", img.replace("off", "on"));
    $.getJSON("/Utilities/VoteDown", { id: Id }, OnWSVoteDown);
}
function OnWSVoteDown(results) {
    var img = $('#imgVoteDown').attr("src");
    if (results.answer === true) {
        $('#imgVoteDown').attr("src", img.replace("off", "on"));
        $('#imgVoteDown').removeAttr("onclick");
        $('#VoteCountDown').html(results.voteCount);
    }
    else {
        $('#imgVoteDown').attr("src", img.replace("on", "off"));
    }
}

function VoteUpContent(Id) {
    var img = $('#imgVoteUp' + Id).attr("src");
    $('#imgVoteUp' + Id).attr("src", img.replace("off", "on"));
    $.getJSON("/Utilities/VoteUpContent", { id: Id }, OnWSVoteUp);
}

function UpdateViewCount(Id) {
    $.getJSON("/Utilities/UpdateViewCount", { id: Id });
}

function AddContentSurvey(Id) {
    $.getJSON("/Utilities/AddContentSurvey", { id: Id, content: document.getElementById('taContent').value });
    $("#ulContents").append("<li class=\"bgWhite colorb\">" + "<div>" + document.getElementById('taContent').value + "</div>" + "</li>");
    document.getElementById('taContent').value = "";
}

function AddComment(Id) {
    $.getJSON("/Utilities/AddComment", { id: Id, comment: document.getElementById('taComment').value });
    $("#ulComments").append("<li class=\"bgWhite colorb\">" + "<div>" + document.getElementById('taComment').value + "</div>" + "</li>");
    document.getElementById('taComment').value = "";
}

function LogIn(span) {
    alert("You must LogIn to Do That (No Registration Required!)");
}

function ReportItem(span, Id) {
    span.innerHTML = "Reported";
    $.getJSON("/Utilities/ReportAction", { id: Id });
}
function FlagBroken(span, Id) {
    span.innerHTML = "Flagged";
    $.getJSON("/Utilities/ReportDataAction", { id: Id, data: 'broken-postcard' });
}
function NotCard(span, Id) {
    span.innerHTML = "Thanks";
    $.getJSON("/Utilities/ReportDataAction", { id: Id, data: 'Not-a-Card' });
}

function FlagDuplicateSetup(span, Id) {
    $("#liDup" + Id).html('Url Of Duplicate Card:<input id="txt' + Id + '" type="text" /><input type="button" onclick="javascript:FlagDuplicate(\'' + Id + '\');" value="Submit"/>');
}
function FlagDuplicate(Id) {
    $.getJSON("/Utilities/ReportDataAction", { id: Id, data: 'Dup-Card ' + document.getElementById('txt' + Id).value });
    $("#liDup" + Id).html("Thank You");
}
function FavoriteOff(img, Id) {
    img.src = img.src.replace("on", "off");
    img.setAttribute("onclick", "FavoriteOn(this,'" + Id + "');");
    $.getJSON("/Utilities/FavoriteOffAction", { id: Id }, OnWSFavoriteReturn);
}
function FavoriteOn(img, Id) {
    img.src = img.src.replace("off", "on");
    img.setAttribute("onclick", "FavoriteOff(this,'" + Id + "');");
    $.getJSON("/Utilities/FavoriteOnAction", { id: Id }, OnWSFavoriteReturn);
}

function OnWSFavoriteReturn(results) {
    if (results.answer === true) {
        $('#FavoriteCount').html(results.favoriteCount);
    }
}
function AddSiteMapNode(url, modified) {
    $.getJSON("/Utilities/AddNodeToSiteMap", { url: url, modified: modified });
}

function checkKey(e) {
    switch (e.keyCode) {
        case 37:
            if ($('#aPrevious').attr('href') != null) {
                document.location = $('#aPrevious').attr('href');
            }
            break;
        case 39:
            if ($('#aPrevious').attr('href') != null) {
                document.location = $('#aNext').attr('href');
            }
            break;
    }
}

if ($.browser.mozilla) {
    $(document).keypress(checkKey);
} else {
    $(document).keydown(checkKey);
}