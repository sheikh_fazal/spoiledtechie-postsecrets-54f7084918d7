﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Roles
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Roles</h2>
    <div>
        Create a new Role:<%: Html.TextBox("tbNewRole") %>
        <input value="Create Role" type="button" onclick="javascript:CreateRole();" /><% Html.Label("lblWarningCreateRole"); %>
    </div>
    <div class="divRolesColumns">
        Roles
        <table id="tblRoles">
            <% foreach (var role in Roles.GetAllRoles())
               { %>
            <tr>
                <td>
                    <span class="spanLink" onclick="javascript:SelectRole('<%: role %>')">Select</span>
                </td>
                <td>
                    <%: role %>
                </td>
                <td>
                    <span class="spanLink" onclick="javascript:DeleteRole('<%: role %>')">Delete Role</span>
                </td>
            </tr>
            <%} %>
        </table>
        <%Html.Hidden("RoleSelected"); %>
        <div class="divRolesColumns">
            Users In Role
            <div id="UsersInRole"></div>
           
        </div>
    </div>
    <script type="text/javascript">
    
    $(document).ready(function() {$('#tblRoles').tablesorter({widgets: ['zebra'], widgetZebra: {css: ['GridViewAlternatingRowStyle','GridviewRowStyle']}});});
    $(document).ready(function () { $('#tblUsersInRoles').tablesorter({ widgets: ['zebra'], widgetZebra: { css: ['GridViewAlternatingRowStyle', 'GridviewRowStyle']} }); });
    </script>
</asp:Content>
