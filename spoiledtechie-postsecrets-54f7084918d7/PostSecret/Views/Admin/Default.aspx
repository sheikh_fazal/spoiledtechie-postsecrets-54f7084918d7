﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	DefaultAdmin
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="divContent">
        <div>
            <a href="<% Url.Content("~/admin/Users"); %>">Users</a></div>
        <ul class="ulList">
            <li>
                <%: ViewData["RegUsers"] %>
                Registered Users</li>
            <li>
                <%: ViewData["UsersOnline"] %>
            </li>
            <li class="links">
               <a href="<% Url.Content("~/admin/LockedUsers"); %>" ><%: ViewData["LockedUsers"] %>
                Locked Users</a></li>
            <li>
                <%: ViewData["ApprovedUsers"] %>
                Approved Users</li>
            <li>
               <%: ViewData["UnApprovedUsers"] %>
                Un-Approved Users</li>
            <li>
                <br />
            </li>
            <li>
               <%: ViewData["UsersToday"] %>
                Users Today</li>
            <li>
                <%: ViewData["LoggedIn5Days"] %>
                logged in within 5 days</li>
            <li>
               <%: ViewData["LoggedIn10Days"] %>
                logged in within 10 days</li>
            <li>
                <%: ViewData["LoggedIn30Days"] %>
                logged in within 30 days</li>
        </ul>
    </div>
    <div class="divContent">
        <div>
          <a href="<% Url.Content("~/admin/Roles"); %>">Roles</a></div>
        <ul class="ulList">
            <li>
                <%: ViewData["RolesCount"] %>
                Roles in the Application</li>
        </ul>
    </div>
   
    <div class="divContent">
        <div>
            <a href="<% Url.Content("~/admin/Errors"); %>">Errors</a></div>
        <ul class="ulList">
            <li>
                <%: ViewData["UnsolvedErrors"] %>
                Unsolved Errors</li>
            <li>
                <%: ViewData["TotalErrors"] %>
                Total Errors </li>
            <li>
                <br />
            </li>
            <li>
                 <%: ViewData["7DayErrors"] %>
                Errors in the last 7 days</li>
            <li>
               <%: ViewData["365DayErrors"] %>
                Errors in the last 365 Days</li>
        </ul>
    </div>
    <div class="divContent">
        <div>
           <a href="<% Url.Content("~/admin/Views"); %>">Views</a></div>
        <ul class="ulList">
            <li>
               <%: ViewData["PageViewsToday"] %>
                Page Views Today</li>
            <li>
                <%: ViewData["PageViews5"] %>
                Page Views in 5 days</li>
            <li>
                <%: ViewData["PageViews10"] %>
                Page Views in 10 days</li>
            <li>
                <%: ViewData["PageViews30"] %>
                Page Views in 30 days</li>
            <li>
                <%: ViewData["TotalPageViews"] %>
                Total Page Views</li>
            <li>
                <br />
            </li>
            <li>
                 <%: ViewData["BrowserTypes"] %>
                Browser Types</li>
            <li>
                 <%: ViewData["OsTypes"] %>
                OS Types</li>
            <li>
                <%: ViewData["JavaMonth"] %></li>
        </ul>
    </div>
    <div class="divContent">
        <div>
          <a href="<% Url.Content("~/admin/Database"); %>">Database</a></div>
        <ul class="ulList">
            <li>DB Name:
                 <%: ViewData["DBName"] %></li>
           <li>DB Size:
                   <%: ViewData["DBSize"] %></li>
                 <li>DB Reserved:
                   <%: ViewData["DBReserved"] %></li>
                 <li>DB Data:
                   <%: ViewData["DBData"] %></li>
                 <li>DB Index Size:
                  <%: ViewData["DBIndexSize"] %></li>
                 <li>DB UnUsed Space:
                  <%: ViewData["DBUnusedSpace"] %></li>
        </ul>
        <ul class="ulList">
            <li>DB Name:
                  <%: ViewData["DBSiteName"] %></li>
           <li>DB Size:
                 <%: ViewData["DBSiteSize"] %></li>
                 <li>DB Reserved:
                <%: ViewData["DBSiteReserved"] %></li>
                 <li>DB Data:
                  <%: ViewData["DBSiteData"] %></li>
                 <li>DB Index Size:
                 <%: ViewData["DBSiteIndexSize"] %></li>
                 <li>DB UnUsed Space:
                  <%: ViewData["DBSiteUnusedSpace"] %></li>
        </ul>
    </div> 
    <div class="divContent">
        <div>
           Application Commands</div>
        <ul class="ulList">
            <li>
                <%--<asp:Button ID="btnRestartTheApp" runat="server" Text="Restart the Application" 
                    onclick="btnRestartTheApp_Click" />--%>
            </li>
        </ul>
    </div>


</asp:Content>
