﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Users
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Users</h2>
    <div>
        <ul class="ulList ulListSide">
            <li>Username:
                <%: Html.TextBox("tbUsernameSearch") %>
                <input type="button" value="Search" onclick="javascript:SearchUserName();" />
            </li>
            <li>Email:
                <%: Html.TextBox("tbUserEmailSearch") %>
                <input type="button" value="Search" onclick="javascript:SearchUserEmail();" />
            </li>
        </ul>
    </div>
    <div class="divUsersTable">
        <div class="divUserColumnsTitle">
           <%: ViewData["regUsersCount"] %>
            Registered Users</div>
        <div id="divUsers">
            <table id="tblUsers" class="tbl">
                <thead>
                    <tr>
                        <th>
                        </th>
                        <th>
                            UserName
                        </th>
                        <th>
                            Approve
                        </th>
                        <th>
                            Comments
                        </th>
                        <th>
                            Created
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Last Login
                        </th>
                        <th>
                            Locked
                        </th>
                        <th>
                            Question
                        </th>
                        <th>
                            Online?
                        </th>
                        <th>
                            Roles
                        </th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <%: Html.Hidden("hiddenUserName") %>
    <div>
        <div class="divUserColumns">
            <div class="divUserColumnsTitle">
                Roles User is not in
            </div>
            <table id="tblRolesNotIn">
            </table>
        </div>
        <%--   <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="divUserColumns">
                    <div class="divUserColumnsTitle">
                        Reset User Password</div>
                    <asp:Label ID="lblResetPassUser" runat="server"></asp:Label><br />
                    <asp:Label ID="lblResetPassword" runat="server"></asp:Label><br />
                    <asp:Button ID="btnResetPassword" runat="server" Text="Reset Password" OnClick="btnResetPassword_Click" />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvUsers" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>--%>
    </div>
    <div>
        <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="divUserColumns">
                    <div class="divUserColumnsTitle">
                        Roles for User
                    </div>
                    <asp:GridView ID="gvRolesIn" runat="server" CssClass="GridViewDefault" DataKeyNames="role"
                        AutoGenerateColumns="false" OnRowDeleting="gvRolesIn_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="role" HeaderText="Role" />
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbDelete" runat="server" CausesValidation="false" CommandName="Delete"
                                        OnClientClick="return confirm('Are you sure?')" Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="GridviewFooterStyle" />
                        <RowStyle CssClass="GridviewRowStyle" />
                        <EditRowStyle CssClass="GridviewEditRowStyle" />
                        <SelectedRowStyle CssClass="GridViewSelectedRowStyle" />
                        <PagerStyle CssClass="GridviewPagerStyle" />
                        <HeaderStyle CssClass="GridviewHeaderStyle" />
                        <AlternatingRowStyle CssClass="GridViewAlternatingRowStyle" />
                    </asp:GridView>
                </div>
                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvUsers" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>
