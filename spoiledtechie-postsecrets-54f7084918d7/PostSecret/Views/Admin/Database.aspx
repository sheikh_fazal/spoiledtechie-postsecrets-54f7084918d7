﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Database
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Database</h2>
    <script type="text/javascript">
        $(document).ready(
        function () {
            $.tablesorter.addParser({
                id: "fancyNumber",
                is: function (s) {
                    return /^[0-9]?[0-9,\.]*$/.test(s);
                },
                format: function (s) {
                    return $.tablesorter.formatFloat(s.replace(/,/g, ''));
                },
                type: "numeric"
            });
            // call the tablesorter plugin, the magic happens in the markup
            $("#tableInfoSite").tablesorter({ widgets: ['zebra'], widgetZebra: { css: ['d0', 'd1']} });
            $("#tableInfo").tablesorter({ widgets: ['zebra'], widgetZebra: { css: ['d0', 'd1']} });
        });

    </script>
    <div class="divRolesColumns">
        <div class="divContent">
            <div>
                <a href="<% Url.Content("~/admin/Database"); %>">Database</a></div>
            <ul class="ulList">
                <li>DB Name:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBInfo"] ).database_name %></li>
                <li>Total Rows:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBInfo"] ).TotalRows %></li>
                <li>DB Reserved:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBInfo"] ).reserved %></li>
                <li>DB Size:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBInfo"] ).database_size %></li>
                <li>DB Data:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBInfo"] ).data %></li>
                <li>DB Index Size:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBInfo"] ).index_size %></li>
                <li>DB UnUsed Space:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBInfo"] ).unallocated_space %></li>
                <li>Total Tables:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBInfo"] ).TotalTables %></li>
            </ul>
        </div>
       <%:ViewData["MemInfo"]%>
    </div>
    <div class="divRolesColumns">
        <div class="divContent">
            <div>
                <a href="<% Url.Content("~/admin/Database"); %>">Database</a></div>
            <ul class="ulList">
                     <li>DB Name:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2) ViewData["DBSiteInfo"] ).database_name %></li>
                <li>Total Rows:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2)ViewData["DBSiteInfo"]).TotalRows%></li>
                <li>DB Reserved:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2)ViewData["DBSiteInfo"]).reserved%></li>
                <li>DB Size:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2)ViewData["DBSiteInfo"]).database_size%></li>
                <li>DB Data:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2)ViewData["DBSiteInfo"]).data%></li>
                <li>DB Index Size:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2)ViewData["DBSiteInfo"]).index_size%></li>
                <li>DB UnUsed Space:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2)ViewData["DBSiteInfo"]).unallocated_space%></li>
                <li>Total Tables:
                    <%: ((PostSecret.ViewModels.DatabaseViewModel2)ViewData["DBSiteInfo"]).TotalTables%></li>
            </ul>
        </div>
       <%:ViewData["SiteInformation"]%>
    </div>

</asp:Content>
