﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<IPagedList<PostSecret.ViewModels.ErrorViewModel>>" %>

<%@ Import Namespace="Boomers.Utilities.MVCHelpers.Paging" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Errors
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Errors</h2>
    <div class="divUsersTable">
        <div class="divUserColumnsTitle">
            <%: ViewData.Model.Count%>
            Errors</div>
        <div id="divUsers">
            <table id="tblUsers" class="tbl">
                <thead>
                    <tr>
                        <th>
                        </th>
                        <th>
                        </th>
                        <th>
                            User_ID
                        </th>
                        <th>
                        </th>
                        <th>
                            User_Email
                        </th>
                        <th>
                            Load Date
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% 
                        int i = 0;
                        foreach (var error in ViewData.Model)
                        {
                            i += 1; %>
                    <tr class="d<%: i%2  %>">
                        <td>
                            <span class="spanLink" onclick="javascript:SelectErrorId(this, '<%: error.uid %>')">Select</span>
                        </td>
                        <td><input type="button" onclick="javascript:ReviewError(this, '<%: error.uid %>')" value="Review" />
                        </td>
                        <td>
                            <%: error.User_Name %>
                        </td>
                        <td>
                        </td>
                        <td>
                            <%: error.User_Email %>
                        </td>
                        <td>
                            <%: error.Load_Date %>
                        </td>
                    </tr>
                    <%} %>
                </tbody>
            </table>
            <div class="pager">
                <%= Html.Pager(ViewData.Model.PageSize, ViewData.Model.PageNumber, ViewData.Model.TotalItemCount) %>
            </div>
        </div>
        <%: Html.Hidden("hiddenErrorId") %>
    </div>
    <div>
        uid:
       <div id="uid"></div>
        <br />url:
       <div id="url"></div>
        <br />previous url:
       <div id="urlPrev"></div>
        <br />
        UserName:<div id="userName"></div>
        <br />
        date_time:<div id="dateTime"></div>
        <br />
        Failed_At:<div id="failedAt"></div>
        <br />
        Raw_Data:<div id="rawData"></div>
        <br />
        Reviewed_By_DateTime:<div id="reviewedDateTime"></div>
        <br />
        Reviewed_By_UserID:<div id="reviewedUserId"></div>
        <br />
        <br />
        Submitted Data:<div id="rawData"></div>
        <br />
    </div>
</asp:Content>
