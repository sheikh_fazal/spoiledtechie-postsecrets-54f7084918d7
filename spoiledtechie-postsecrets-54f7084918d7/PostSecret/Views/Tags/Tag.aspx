﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Viewing Posters with the Tag <%: (ViewData["Tag"] as PostSecret.ViewModels.TagViewModel).TagName%> - DePostSecret.com
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript"><!--
    google_ad_client = "ca-pub-6494646249414123";
    /* PostSecretLeaderBoard */
    google_ad_slot = "1867020116";
    google_ad_width = 728;
    google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script><h1>Viewing Tag <%: (ViewData["Tag"] as PostSecret.ViewModels.TagViewModel).TagName%></h1>
<ul class="ulPosterList">
  <% foreach (var poster in (ViewData["Tag"] as PostSecret.ViewModels.TagViewModel).Poster)
       { %>
    <li class="liPosterList blackL">
    <div class="divPosterListThumb">
    <a href="<%: Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid)+"/" + Boomers.Utilities.Text.Extensions.StringExt.ToSearchEngineFriendly(poster.Title)) %>">
   <center>     <img src="<%: poster.UrlThumb %>" class="imgPosterList" /></center></a></div>
        <div class="divPosterList">
            <ul class="ulTitleList">
                <li class="liTitleList"><a href="<%: Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid)+"/" + Boomers.Utilities.Text.Extensions.StringExt.ToSearchEngineFriendly(poster.Title)) %>">
                    <%: System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToUpper(poster.Title)%></a></li>
                
            </ul>
            <div class="divPosterBottomTags">
                <ul class="ulGenericHorList">
                    <%
                        foreach (var tag in poster.Tags)
                        {%>
                    <li>
                        <%:Html.RouteLink(
                       tag.TagName,
                                            "TagsName",
            new
            {
                controller = "Tags",
              action = "Tag",
              id = tag.TagID,
                name = tag.TagName
            })%>
                        <b>x</b>
                        <%: tag.TagCount %></li>
                    <%} %>
                </ul>
            </div>
            <div class="divPosterBottomList">
                <ul class="ulGenericHorList">
                    <li><b>
                        <%: poster.CommentCount%></b> Comments</li>
                    <li>| </li>
                    <li><b>
                        <%: poster.Views%></b> Views</li>
                    <li>| </li>
                    <li><b>
                        <%: poster.VotesUp%></b> Ups</li>
                    <li>| </li>
                    <li><b>
                        <%: poster.VotesDown%></b> Downs</li>
                    <li><b>
                        <%: poster.Favorites%></b> Favorited</li>
                </ul>
            </div>
        </div>
    </li>
    <% } %>
    </ul>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
<div class="SideBarItem corners">
        <div class="SideBarItemHeader">
            Recent Tags</div>
        <% Html.RenderPartial("TagsListPartialView"); %>
    </div>
</asp:Content>
