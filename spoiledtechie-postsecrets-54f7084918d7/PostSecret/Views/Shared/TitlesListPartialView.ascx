﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<%@ Import Namespace="Boomers.Utilities.MVCHelpers.Paging" %>
<center>
    <script type="text/javascript"><!--
        google_ad_client = "ca-pub-6494646249414123";
        /* PostSecretLeaderBoard */
        google_ad_slot = "1867020116";
        google_ad_width = 728;
        google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</center>
<div class="pager">
    <% if (Request.Url.ToString().Contains("PostCards/Top"))
       { %>
    <%= Html.Pager((ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).PageSize, (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).PageNumber, (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).TotalItemCount, "Top")%>
    <%}
       else
       { %>
    <%= Html.Pager((ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).PageSize, (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).PageNumber, (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).TotalItemCount)%>
    <%} %>
</div>
<ul class="ulItemList">
    <% 
        Random r = new Random();
        int b = r.Next((ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).Count - 1);
        int i = 0;
        foreach (var poster in (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>))
        {
    %>
    <li class="liItemList color">
        <div>
            <center>
                <a href="<%: Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid)+"/" + Boomers.Utilities.Text.StringExt.ToSearchEngineFriendly(poster.Title)) %>">
                    <img src="<%: poster.Url %>" class="imgC" />
                </a>
            </center>
        </div>
        <div class="divBuyMeLink">
            <a href="<%: Url.Content("~/Orders/Buy/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid)) %>">
                <img width="80px" src="<%: Url.Content("~/img/buyNow.png") %>" class="hover imgExtras" /></a><span
                    class='st_facebook_hcount' displaytext='Facebook' st_title='<%: poster.Title %>'
                    st_image='<%: poster.Url %>' st_url='<%:Boomers.Utilities.Web.Utilities.GetSiteRoot()+ Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid)+"/" + Boomers.Utilities.Text.StringExt.ToSearchEngineFriendly(poster.Title)) %>'></span>
                    
                    <span
                        class='st_twitter_hcount' displaytext='Tweet' st_title='<%: poster.Title %>'
                        st_image='<%: poster.Url %>' st_url='<%:Boomers.Utilities.Web.Utilities.GetSiteRoot()+ Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid)+"/" + Boomers.Utilities.Text.StringExt.ToSearchEngineFriendly(poster.Title)) %>'></span>
                        <span
                            class='st_tumblr_hcount' displaytext='Tumblr' st_title='<%: poster.Title %>'
                            st_image='<%: poster.Url %>' st_url='<%:Boomers.Utilities.Web.Utilities.GetSiteRoot()+ Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid)+"/" + Boomers.Utilities.Text.StringExt.ToSearchEngineFriendly(poster.Title)) %>'></span>
                            
                            <span><a href="http://pinterest.com/pin/create/button/?url=<%:Boomers.Utilities.Web.Utilities.GetSiteRoot()+ Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid)) %>&media=<%: poster.Url %>" class="pin-it-button" count-layout="horizontal">Pin It</a></span>
                            </div>
        <div>
            <div>
                <ul class="ulGenericHorList">
                    <li>
                        <img src="<%: Url.Content("~/img/vote-arrow-up-off.png") %>" width="40px" onclick="VoteUp('<%: poster.uid%>')"
                            id="imgVoteUp<%: poster.uid%>" class="hover imgExtras" />
                        <b><span id="VoteCountUp<%: poster.uid%>">
                            <%: poster.VotesUp%></span></b> Ups</li>
                    <li>| </li>
                    <li>
                        <% if (Request.IsAuthenticated && poster.Favorites.Where(x => x.User_Id == PostSecret.SupportFramework.DataAccessGlobal.UserID()).FirstOrDefault() == null)
                           { %>
                        <img src="<%: Url.Content("~/img/star-off.png") %>" width="40px" class="hover" onclick="FavoriteOn(this,'<%: poster.uid%>')" />
                        <% }
                           else if (Request.IsAuthenticated && (int)ViewData["Favorite"] > 0)
                           { %>
                        <img src="<%: Url.Content("~/img/star-on.png") %>" width="40px" class="hover" onclick="FavoriteOff(this,'<%: poster.uid%>')" />
                        <%} %>
                        <b>
                            <%: poster.Favorites.Count%></b> Favorited</li>
                    <li>| </li>
                    <li><b>
                        <%: poster.CommentCount %></b> Comments</li>
                    <li>| </li>
                    <li><b>
                        <%: poster.Views %></b> Views</li>
                    <li>| </li>
                    <li><span class="spanLink" onclick="javascript:FlagBroken(this, '<%: poster.uid%>')">
                        Flag as Broken</span></li>
                    <li>| </li>
                    <li><span class="spanLink" onclick="javascript:NotCard(this, '<%: poster.uid%>')">Not
                        a PostCard?</span></li>
                    <li>| </li>
                    <li id="liDup<%: poster.uid%>"><span class="spanLink" onclick="javascript:FlagDuplicateSetup(this, '<%: poster.uid%>')">
                        Flag as Duplicate</span></li>
                </ul>
            </div>
            <div>
                <ul class="ulGenericHorList">
                    <%
                        foreach (var tag in poster.Tags)
                        {%>
                    <li>
                        <%:Html.RouteLink(
                       tag.TagName,
                                            "TagsName",
            new
            {
                controller = "Tags",
              action = "Tag",
              id = tag.TagID,
                name = tag.TagName
            })%>
                        <b>x</b>
                        <%: tag.TagCount %></li>
                    <%} %>
                </ul>
            </div>
            <ul class="ulTitleList">
                <li class="liTitleList">
                    <% if (poster.Title != null)
                       { %>
                    <%: poster.Title%>
                    <% }
                       else
                       {%>
                    <div class="divTitleList">
                        - <a href="<%: Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes(poster.uid))%>">
                            Tell us what this postcard says!</a></div>
                    <%} %>
                </li>
            </ul>
        </div>
    </li>
    <%if (b == i)
      { %>
    <li class="liItemList color">
        <center>
           <script type="text/javascript"><!--
               google_ad_client = "ca-pub-6494646249414123";
               /* PostSecretImbeddedAd */
               google_ad_slot = "8851499130";
               google_ad_width = 336;
               google_ad_height = 280;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
        </center>
    </li>
    <%}
      i += 1;
        } %>
</ul>
<div class="pager">
    <% if (Request.Url.ToString().Contains("PostCards/Top"))
       { %>
    <%= Html.Pager((ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).PageSize, (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).PageNumber, (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).TotalItemCount, "Top")%>
    <%}
       else
       { %>
    <%= Html.Pager((ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).PageSize, (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).PageNumber, (ViewData["Titles"] as IPagedList<PostSecret.ViewModels.CardViewModel>).TotalItemCount)%>
    <%} %>
</div>
