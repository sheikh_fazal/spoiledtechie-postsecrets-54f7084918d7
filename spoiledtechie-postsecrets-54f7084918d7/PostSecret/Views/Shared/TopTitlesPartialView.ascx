﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
 <% foreach (var item in (ViewData["Titles"] as List<PostSecret.ViewModels.TitleViewModel>))
       { %>
    <div>
        <div class="StoriesRandomTitle corners">
            <a href="<%= Url.Content("~/Stories/" + item.ID.ToString().Replace("-","") +"/"+ System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase (item.Title + " Feels Like " + item.Stories.Last().OneLiner).Replace("\"", "").Replace("'","").Replace(".","").Replace(" ","-")) %>"><%: item.Title%></a>
            <ul class="ulStoryLinks"><li>Started by a <%: item.SexType %></li><li>Viewed <%: item.Views %> times</li></ul>
        </div>
        <ul class="ulStoriesRandomStory">
            <% foreach (var story in item.Stories)
               { %>
            <li class="liStoriesRandomStory">
                <div class="corners divStoriesRandomStory">
                    <%: story.OneLiner %></div>
            </li>
            <%} %>
        </ul>
    </div>
    <% } %>