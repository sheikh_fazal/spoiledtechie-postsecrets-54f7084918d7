﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<ul class="ulTagList blackL">
    <%{
          foreach (var item in ViewData["Tags"] as List<PostSecret.ViewModels.TagViewModel>)
          {%>
    <li class="liTagList">
        <%:Html.RouteLink(
                       System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase  ( item.TagName),
                                            "TagsName",
            new
            {
                controller = "Tags",
              action = "Tag",
              id = item.TagID,
                name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(item.TagName).Replace("\"", "").Replace(" ", "-")
            })%>
        <b>x</b>
        <%: item.TagCount%></li>
    <% }
      }%>
</ul>
