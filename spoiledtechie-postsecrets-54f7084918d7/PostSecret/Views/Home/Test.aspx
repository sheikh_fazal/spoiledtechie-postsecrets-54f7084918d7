﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Test.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page
</asp:Content>

<asp:Content ID="head" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <% var obj = Model as IEnumerable<PostSecret.Models.TestModels>;%>
        <%--   <% foreach (var item in obj)
            { %>
        <% if (item.id == obj.ToList().Count - 1)
            { %>
        <div class="container last" style="max-width: 600px;" id="<%: item.id %>">
            <%}
                else
                { %>
            <div class="container" style="max-width: 600px;" id="Div1">
                <% } %>--%>
        <div class="pl-5 pt-2 col-sm-6 col-sm-offset-1" id="Div1">
            <div class="panel panel-default margin-top">
                <div class="post-padding panel-heading">

                    <div class="col-sm-12 col-xs-12">
                        <div class="pl-0 col-sm-1 col-xs-1">
                            <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                <img class="h-100 w-100" src="../../img/buyNow1.png">
                            </a>
                        </div>
                        <div class="pl-3 mt-1 col-sm-10 col-xs-10">
                            <a class="color-black text-decoration-none" href="#">
                                <%--<%: item.title %>--%>
                                    Page Name
                            </a>
                        </div>
                        <div class="no-padding col-sm-1 col-xs-1">
                            <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                    <li><a href="#">Flag as Broken</a></li>
                                    <li><a href="#">Flag as Duplicate</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Not a PostCard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body left">
                    <div class="image-container">
                        <img alt="post image" src="../../img/pak.jpg" class="post-image img-responsive" />
                    </div>
                </div>
                <div class="post-footer-color panel-body">
                    <div class="control-group">
                        <div class="no-padding col-sm-12 col-xs-12">
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-comment-o fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <a class="pointer">
                                    <i class="fa fa-share-alt fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">14</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">03</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">47</span>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <span class="color-black">10</span>
                            </div>
                            <div class="border-comment-bottom pl-2 pr-0 no-margin col-sm-12 col-xs-12">
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 2:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-hide"></i>
                                </div>
                           
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            </div>
                            <div class="py-4 col-sm-12 col-xs-12">
                                <textarea class="comment-section font-size-14 no-padding border-none col-sm-12 col-xs-12" id="comment"
                                    placeholder="Add a review"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4 col-sm-offset-1 User-home-header-position hidden-xs">
            <div class="pl-0 User-home-header-border">
                <a class="User-home-img" href="#" style="width: 50px; height: 50px;">
                    <img class="h-100 w-100" src="../../img/h.png">
                </a>
                <span style="display: inline-block;">
                    <a class="color-black text-decoration-none" href="#">
                        <label class="no-margin pl-3 pointer" style="display: block;">User Name </label>
                    </a>
                    <label class="pl-4 font-size-11 font-normal">Profile Name </label>
                </span>
            </div>
            <div class="px-0 pt-3">
                <label>Stories</label>
                <p>
                    This is my story , testing testing testing testing ....
                This is my story test testing testing testing ....
                This is my story , testing testing testing testing ....
                </p>
            </div>
            <div class="px-0">
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <a class="font-normal color-header-link text-decoration-none" href="#">
                    <label class="pointer">Link1</label>
                </a>
                <br />
                &copy;<label class="pl-1 pt-2 pointer">PostSecret 2018</label>
            </div>
        </div>

        <div class="pl-5 pt-2 col-sm-6 col-sm-offset-1" id="Div1">
            <div class="panel panel-default margin-top">
                <div class="post-padding panel-heading">

                    <div class="col-sm-12 col-xs-12">
                        <div class="pl-0 col-sm-1 col-xs-1">
                            <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                <img class="h-100 w-100" src="../../img/buyNow1.png">
                            </a>
                        </div>
                        <div class="pl-3 mt-1 col-sm-10 col-xs-10">
                            <a class="color-black text-decoration-none" href="#">
                                <%--<%: item.title %>--%>
                                    Page Name
                            </a>
                        </div>
                        <div class="no-padding col-sm-1 col-xs-1">
                            <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                    <li><a href="#">Flag as Broken</a></li>
                                    <li><a href="#">Flag as Duplicate</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Not a PostCard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body left">
                    <div class="image-container">
                        <img alt="post image" src="../../img/pak.jpg" class="post-image img-responsive" />
                    </div>
                </div>
                <div class="post-footer-color panel-body">
                    <div class="control-group">
                        <div class="no-padding col-sm-12 col-xs-12">
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-comment-o fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <a class="pointer">
                                    <i class="fa fa-share-alt fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">14</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">03</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">47</span>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <span class="color-black">10</span>
                            </div>
                            <div class="border-comment-bottom pl-2 pr-0 no-margin col-sm-12 col-xs-12">
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 2:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-hide"></i>
                                </div>
                           
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            </div>
                            <div class="py-4 col-sm-12 col-xs-12">
                                <textarea class="comment-section font-size-14 no-padding border-none col-sm-12 col-xs-12" id="comment"
                                    placeholder="Add a review"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="pl-5 pt-2 col-sm-6 col-sm-offset-1" id="Div1">
            <div class="panel panel-default margin-top">
                <div class="post-padding panel-heading">

                    <div class="col-sm-12 col-xs-12">
                        <div class="pl-0 col-sm-1 col-xs-1">
                            <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                <img class="h-100 w-100" src="../../img/buyNow1.png">
                            </a>
                        </div>
                        <div class="pl-3 mt-1 col-sm-10 col-xs-10">
                            <a class="color-black text-decoration-none" href="#">
                                <%--<%: item.title %>--%>
                                    Page Name
                            </a>
                        </div>
                        <div class="no-padding col-sm-1 col-xs-1">
                            <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                    <li><a href="#">Flag as Broken</a></li>
                                    <li><a href="#">Flag as Duplicate</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Not a PostCard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body left">
                    <div class="image-container">
                        <img alt="post image" src="../../img/pak.jpg" class="post-image img-responsive" />
                    </div>
                </div>
                <div class="post-footer-color panel-body">
                    <div class="control-group">
                        <div class="no-padding col-sm-12 col-xs-12">
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-comment-o fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <a class="pointer">
                                    <i class="fa fa-share-alt fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">14</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">03</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">47</span>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <span class="color-black">10</span>
                            </div>
                            <div class="border-comment-bottom pl-2 pr-0 no-margin col-sm-12 col-xs-12">
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 2:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-hide"></i>
                                </div>
                           
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            </div>
                            <div class="py-4 col-sm-12 col-xs-12">
                                <textarea class="comment-section font-size-14 no-padding border-none col-sm-12 col-xs-12" id="comment"
                                    placeholder="Add a review"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="pl-5 pt-2 col-sm-6 col-sm-offset-1" id="Div1">
            <div class="panel panel-default margin-top">
                <div class="post-padding panel-heading">

                    <div class="col-sm-12 col-xs-12">
                        <div class="pl-0 col-sm-1 col-xs-1">
                            <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                <img class="h-100 w-100" src="../../img/buyNow1.png">
                            </a>
                        </div>
                        <div class="pl-3 mt-1 col-sm-10 col-xs-10">
                            <a class="color-black text-decoration-none" href="#">
                                <%--<%: item.title %>--%>
                                    Page Name
                            </a>
                        </div>
                        <div class="no-padding col-sm-1 col-xs-1">
                            <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                    <li><a href="#">Flag as Broken</a></li>
                                    <li><a href="#">Flag as Duplicate</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Not a PostCard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body left">
                    <div class="image-container">
                        <img alt="post image" src="../../img/pak.jpg" class="post-image img-responsive" />
                    </div>
                </div>
                <div class="post-footer-color panel-body">
                    <div class="control-group">
                        <div class="no-padding col-sm-12 col-xs-12">
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-comment-o fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <a class="pointer">
                                    <i class="fa fa-share-alt fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">14</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">03</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">47</span>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <span class="color-black">10</span>
                            </div>
                            <div class="border-comment-bottom pl-2 pr-0 no-margin col-sm-12 col-xs-12">
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 2:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-hide"></i>
                                </div>
                           
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            </div>
                            <div class="py-4 col-sm-12 col-xs-12">
                                <textarea class="comment-section font-size-14 no-padding border-none col-sm-12 col-xs-12" id="comment"
                                    placeholder="Add a review"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="pl-5 pt-2 col-sm-6 col-sm-offset-1" id="Div1">
            <div class="panel panel-default margin-top">
                <div class="post-padding panel-heading">

                    <div class="col-sm-12 col-xs-12">
                        <div class="pl-0 col-sm-1 col-xs-1">
                            <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                <img class="h-100 w-100" src="../../img/buyNow1.png">
                            </a>
                        </div>
                        <div class="pl-3 mt-1 col-sm-10 col-xs-10">
                            <a class="color-black text-decoration-none" href="#">
                                <%--<%: item.title %>--%>
                                    Page Name
                            </a>
                        </div>
                        <div class="no-padding col-sm-1 col-xs-1">
                            <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                    <li><a href="#">Flag as Broken</a></li>
                                    <li><a href="#">Flag as Duplicate</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Not a PostCard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body left">
                    <div class="image-container">
                        <img alt="post image" src="../../img/pak.jpg" class="post-image img-responsive" />
                    </div>
                </div>
                <div class="post-footer-color panel-body">
                    <div class="control-group">
                        <div class="no-padding col-sm-12 col-xs-12">
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-comment-o fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <a class="pointer">
                                    <i class="fa fa-share-alt fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">14</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">03</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">47</span>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <span class="color-black">10</span>
                            </div>
                            <div class="border-comment-bottom pl-2 pr-0 no-margin col-sm-12 col-xs-12">
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 2:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-hide"></i>
                                </div>
                           
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            </div>
                            <div class="py-4 col-sm-12 col-xs-12">
                                <textarea class="comment-section font-size-14 no-padding border-none col-sm-12 col-xs-12" id="comment"
                                    placeholder="Add a review"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="pl-5 pt-2 col-sm-6 col-sm-offset-1" id="Div1">
            <div class="panel panel-default margin-top">
                <div class="post-padding panel-heading">

                    <div class="col-sm-12 col-xs-12">
                        <div class="pl-0 col-sm-1 col-xs-1">
                            <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                <img class="h-100 w-100" src="../../img/buyNow1.png">
                            </a>
                        </div>
                        <div class="pl-3 mt-1 col-sm-10 col-xs-10">
                            <a class="color-black text-decoration-none" href="#">
                                <%--<%: item.title %>--%>
                                    Page Name
                            </a>
                        </div>
                        <div class="no-padding col-sm-1 col-xs-1">
                            <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                    <li><a href="#">Flag as Broken</a></li>
                                    <li><a href="#">Flag as Duplicate</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Not a PostCard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body left">
                    <div class="image-container">
                        <img alt="post image" src="../../img/pak.jpg" class="post-image img-responsive" />
                    </div>
                </div>
                <div class="post-footer-color panel-body">
                    <div class="control-group">
                        <div class="no-padding col-sm-12 col-xs-12">
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-comment-o fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <a class="pointer">
                                    <i class="fa fa-share-alt fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">14</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">03</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">47</span>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <span class="color-black">10</span>
                            </div>
                            <div class="border-comment-bottom pl-2 pr-0 no-margin col-sm-12 col-xs-12">
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 2:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-hide"></i>
                                </div>
                           
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            </div>
                            <div class="py-4 col-sm-12 col-xs-12">
                                <textarea class="comment-section font-size-14 no-padding border-none col-sm-12 col-xs-12" id="comment"
                                    placeholder="Add a review"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="pl-5 pt-2 col-sm-6 col-sm-offset-1" id="Div1">
            <div class="panel panel-default margin-top">
                <div class="post-padding panel-heading">

                    <div class="col-sm-12 col-xs-12">
                        <div class="pl-0 col-sm-1 col-xs-1">
                            <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                <img class="h-100 w-100" src="../../img/buyNow1.png">
                            </a>
                        </div>
                        <div class="pl-3 mt-1 col-sm-10 col-xs-10">
                            <a class="color-black text-decoration-none" href="#">
                                <%--<%: item.title %>--%>
                                    Page Name
                            </a>
                        </div>
                        <div class="no-padding col-sm-1 col-xs-1">
                            <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                    <li><a href="#">Flag as Broken</a></li>
                                    <li><a href="#">Flag as Duplicate</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Not a PostCard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body left">
                    <div class="image-container">
                        <img alt="post image" src="../../img/pak.jpg" class="post-image img-responsive" />
                    </div>
                </div>
                <div class="post-footer-color panel-body">
                    <div class="control-group">
                        <div class="no-padding col-sm-12 col-xs-12">
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-comment-o fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <a class="pointer">
                                    <i class="fa fa-share-alt fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">14</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">03</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">47</span>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <span class="color-black">10</span>
                            </div>
                            <div class="border-comment-bottom pl-2 pr-0 no-margin col-sm-12 col-xs-12">
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 2:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-hide"></i>
                                </div>
                           
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            </div>
                            <div class="py-4 col-sm-12 col-xs-12">
                                <textarea class="comment-section font-size-14 no-padding border-none col-sm-12 col-xs-12" id="comment"
                                    placeholder="Add a review"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div class="pl-5 pt-2 col-sm-6 col-sm-offset-1" id="Div1">
            <div class="panel panel-default margin-top">
                <div class="post-padding panel-heading">

                    <div class="col-sm-12 col-xs-12">
                        <div class="pl-0 col-sm-1 col-xs-1">
                            <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                <img class="h-100 w-100" src="../../img/buyNow1.png">
                            </a>
                        </div>
                        <div class="pl-3 mt-1 col-sm-10 col-xs-10">
                            <a class="color-black text-decoration-none" href="#">
                                <%--<%: item.title %>--%>
                                    Page Name
                            </a>
                        </div>
                        <div class="no-padding col-sm-1 col-xs-1">
                            <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                    <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                    <li><a href="#">Flag as Broken</a></li>
                                    <li><a href="#">Flag as Duplicate</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Not a PostCard</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body left">
                    <div class="image-container">
                        <img alt="post image" src="../../img/pak.jpg" class="post-image img-responsive" />
                    </div>
                </div>
                <div class="post-footer-color panel-body">
                    <div class="control-group">
                        <div class="no-padding col-sm-12 col-xs-12">
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <a class="pointer">
                                    <i class="fa fa-comment-o fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <a class="pointer">
                                    <i class="fa fa-share-alt fa-2x color-black"></i>
                                </a>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">14</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">03</span>
                            </div>
                            <div class="pl-2 col-sm-2 col-xs-2">
                                <span class="color-black">47</span>
                            </div>
                            <div class="no-padding col-sm-offset-5 col-xs-offset-5 col-sm-1 col-xs-1">
                                <span class="color-black">10</span>
                            </div>
                            <div class="border-comment-bottom pl-2 pr-0 no-margin col-sm-12 col-xs-12">
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 2:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-hide"></i>
                                </div>
                           
                                <div class="no-padding col-sm-11 col-xs-11">
                                    <label class="no-margin font-size-11">
                                        <a class="color-black pointer text-decoration-none" href="#">
                                            <b>User Name 1:</b>
                                        </a>
                                    </label>
                                    <label style="display: inline;" class="font-normal font-size-11">
                                        Comments Comments Comments Comments Comments 
                                    Comments Comments Comments Comments Comments Comments 
                                    </label>
                                </div>
                                <div class="text-center no-padding col-sm-1 col-xs-1">
                                    <i class="fa fa-remove"></i>
                                </div>
                            </div>
                            <div class="py-4 col-sm-12 col-xs-12">
                                <textarea class="comment-section font-size-14 no-padding border-none col-sm-12 col-xs-12" id="comment"
                                    placeholder="Add a review"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%--   <% } %>
     </div>
    </div>--%>
</asp:Content>

