﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Error
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="color">
   <style type="text/css">  
      .sys-template { display:none; }   
      .missingkid { clear:both; }   
   </style>
 <script src="http://ajax.microsoft.com/ajax/beta/0911/Start.js" type="text/javascript"></script>

<script type="text/javascript">
    Sys.require([Sys.components.dataView, Sys.scripts.jQuery], function () {
        $("#missingkids").dataView();

        var statecode = "ZZ";
        var dataurl = "http://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20From%20xml%0D%0A%20Where%20url%3D'http%3A%2F%2Fwww.missingkidsmap.com%2Fread.php%3Fstate%3D" + statecode + "'%0D%0A&format=json&callback=?";
        var data = $.getJSON(dataurl, function (results) {
            Sys.get("$missingkids").set_data(results.query.results.locations.location);
        }
      );
    });

    function getSrc(url) {
        var lastIndex = url.lastIndexOf('=');
        return url.substring(lastIndex + 1);
    }
</script>
<h1>Whoa!!! Hold up!</h1>

<h2>I think something just happened!  A problem just occured.</h2>
<h4>
    While we take our time to fix this error, do you think you can take a brief second to look over the list below.  Its a list of Missing Children accross the United States.
</h4>

<div id="missingkids" class="sys-template">
  <div class="missingkid">
    <img sys:width="80" sys:align="left" sys:src="{binding medpic, convert=getSrc}" />
    <strong>{{firstname + " " + lastname}}</strong>, Age: {{age}} from  
    {{city}}, {{st}}</br>
    Contact: <b>{{policeadd}}</b> at {{policenum}}<br/>
    <br/>
  </div>
</div>

</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
</asp:Content>
