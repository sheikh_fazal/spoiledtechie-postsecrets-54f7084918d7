﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    FAQ about PostSecretCollection.com
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="color">
        <h2>
            FAQ</h2>
        <p>
            We here at PostSecretCollection.com wanted a place where we could go way back and
            look at all the postcards on PostSecret.com. With Franks current implementation,
            we can only see the current post cards. So we needed to change that.</p>
        <p>
            We created a few scripts that went looking for EVERY SINGLE Past Post Card ever
            posted on postsecret.com. On our Initial first pass, we came back with over 40,000
            postcards. While we estimate their aren't that many, we went ahead and downloaded
            them anyway. So in the interest of time, we will rely on you all to tell us if their
            is a duplicate postcard in our archives. To keep the collection up to date, every
            Sunday we will be running it against PostSecret.com.</p>
        <p>
            We also wanted one more thing, to make every post card readable by blind people,
            that they may also experience the joy it comes to sharing secrets that the current
            visuals cannot show. So In the coming weeks, we will be creating a way for people
            to tell us what each post card says.</p>
        <p>
            To clarify something, We will not make any profit on this website. The ads are only
            there to pay for server costs. Any extra money made from book sales or advertisements
            will be donated to a Suicide Prevention Hotline.</p>
        <p>
            All Postcards are for the community, but specific copyright questions, will go to
            Frank. We totally believe Frank is the Curator and will not deny any request made
            by him.</p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
</asp:Content>
