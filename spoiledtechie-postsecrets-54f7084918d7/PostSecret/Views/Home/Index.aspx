﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    PostSecret.com's Biggest UnOfficial Collection
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="color centerFont pageTitle">
        The Most Recent Postcards</div>
    <% Html.RenderPartial("TitlesListPartialView"); %>
    <div class="color">
        <p>
           We will not make any profit on this website. The ads are only
            there to pay for server costs. Any extra money made from book sales or advertisements
            will be donated to a Suicide Prevention Hotline. For more information please read our FAQ.</p>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
    <div class="centerFont sideTitle color">
        Most Recent Tags</div>
    <% Html.RenderPartial("TagsListPartialView"); %>
    <center>
      <script type="text/javascript"><!--
          google_ad_client = "ca-pub-6494646249414123";
          /* PostSecretSkyScraper */
          google_ad_slot = "6985393558";
          google_ad_width = 160;
          google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></center>

</asp:Content>
