﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Test.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="indexTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Home Page - My ASP.NET MVC Application
</asp:Content>

<asp:Content ID="head" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="IndexStoryContent" ContentPlaceHolderID="MainStoryContent" runat="server">
    <div class="pull-right">
        <label>
            Testing Testing Testing Testing
        </label>
    </div>
</asp:Content>
<asp:Content ID="indexContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="posts">
        <% var obj = Model as IEnumerable<PostSecret.Models.TestModels>;%>
        <% foreach (var item in obj)
            { %>
        <% if (item.id == obj.ToList().Count - 1)
            { %>
        <div class="container last" style="max-width: 600px;" id="<%: item.id %>">
            <%}
                else
                { %>
            <div class="container" style="max-width: 600px;" id="Div1">
                <% } %>

                <div class="panel panel-default margin-top">
                    <div class="post-padding panel-heading">
                        <%--<header class="_7b8eu _9dpug">
                            <div class="_82odm _i2o1o">
                                <a class="_pg23k _gvoze" href="/imrankhan.pti/" style="width: 30px; height: 30px;">
                                    <img class="_rewi8" src="../../img/buyNow1.png">
                                </a>
                            </div>
                            <div class="_j56ec">
                                <div class="">
                                    <div class="_eeohz"><a class="_2g7d5 notranslate _iadoq" title="imrankhan.pti" href="/imrankhan.pti/">imrankhan.pti</a></div>
                                </div>
                                <div class="_60iqg"></div>
                            </div>
                        </header>--%>
                        <div class="col-sm-12 col-xs-12">

                            <div class="pl-0 col-sm-1 col-xs-1">
                                <a class="post-header-img" href="#" style="width: 30px; height: 30px;">
                                    <img class="h-100 w-100" src="../../img/buyNow1.png">
                                </a>
                            </div>
                            <div class="pl-1 mt-1 col-sm-10 col-xs-10">
                                <a class="color-black text-decoration-none" href="#">
                                    <%: item.title %>
                                </a>
                            </div>
                            <div class="no-padding col-sm-1 col-xs-1">
                                <div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">
                                    <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                        <span class="fa fa-ellipsis-h fa-2x color-black"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">
                                        <li><a href="#">Flag as Broken</a></li>
                                        <li><a href="#">Flag as Duplicate</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">Not a PostCard</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="panel-body left">
                        <div class="image-container">
                            <img alt="post image" src="<%: item.image %>" class="post-image img-responsive" />
                        </div>
                    </div>
                    <div class="post-footer-color panel-body">
                        <div class="control-group">
                            <div class="pl-0 col-sm-12 col-xs-12">
                                <%-- <button class="btn btn-link" title="Like">
                                    <img class="link-image" alt="Like" src="<%: Url.Content("~/Content/template/img/thumb32x32.png")%>" />
                                    <span><%: item.likes %></span>
                                </button>
                                <button class="btn btn-link" title="Dislike">
                                    <img class="link-image" alt="Dislike" src="<%: Url.Content("~/Content/template/img/thumb-unlike32x32.png")%>" />
                                    <span><%: item.dislikes %></span>
                                </button>
                                <button class="btn btn-link" title="Comment">
                                    <img class="link-image" alt="Comment" src="<%: Url.Content("~/Content/template/img/comments32x32.png")%>" />
                                    <span><%: item.comments %></span>
                                </button>--%>
                                <div class="col-sm-1 col-xs-1">
                                    <a class="pointer">
                                        <i class="fa fa-thumbs-o-up fa-2x color-black"></i>
                                    </a>
                                </div>
                                <div class="col-sm-1 col-xs-1">
                                    <a class="pointer">
                                        <i class="fa fa-thumbs-o-down fa-2x color-black"></i>
                                    </a>
                                </div>
                                <div class="col-sm-1 col-xs-1">
                                    <a class="pointer">
                                        <i class="fa fa-comment-o fa-2x color-black"></i>
                                    </a>
                                </div>
                                <div class="col-sm-offset-8 col-xs-offset-8 col-sm-1 col-xs-1">
                                    <a class="pointer">
                                        <i class="fa fa-share-alt fa-2x color-black"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="pl-0 col-sm-12 col-xs-12">
                                <div class="col-sm-1 col-xs-1">
                                    <span class="color-black"><%: item.likes %></span>
                                </div>
                                <div class="col-sm-1 col-xs-1">
                                    <span class="color-black"><%: item.dislikes %></span>
                                </div>
                                <div class="col-sm-1 col-xs-1">
                                    <span class="color-black"><%: item.comments %></span>
                                </div>
                                <div class="col-sm-offset-8 col-xs-offset-8 col-sm-1 col-xs-1">
                                    <span class="color-black"><%: item.shareLinks %></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <% } %>
        </div>
    </div>
    <%--<script type="text/javascript" src="<%: Url.Content("~/Content/template/scripts/home.js")%>"></script>--%>
</asp:Content>

