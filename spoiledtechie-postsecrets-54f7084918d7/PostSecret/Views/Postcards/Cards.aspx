﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Page <%: ViewData["Page"]  %> of the PostSecret Collections
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div class="color centerFont pageTitle">
        Page <%: ViewData["Page"]  %> of the PostSecret Collections</div>
    <% Html.RenderPartial("TitlesListPartialView"); %>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
<div class="centerFont sideTitle color">
        Most Recent Tags</div>
    <% Html.RenderPartial("TagsListPartialView"); %>
    <center>
        <script type="text/javascript"><!--
            google_ad_client = "ca-pub-6494646249414123";
            /* PostSecretSkyScraper */
            google_ad_slot = "6985393558";
            google_ad_width = 160;
            google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> </center>
</asp:Content>

