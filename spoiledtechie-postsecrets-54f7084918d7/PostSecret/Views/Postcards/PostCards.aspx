﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Title %>
    : Post Secret Post Card
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clear">
        <br />
        <center>
            <script type="text/javascript"><!--
                google_ad_client = "ca-pub-6494646249414123";
                /* PostSecretLeaderBoard */
                google_ad_slot = "1867020116";
                google_ad_width = 728;
                google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
        </center>
        <br />
    </div>
    <ul class="ulGenericHorList">
        <li class="color">PostCard
            <%: ((ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Number + 1).ToString("N0")%>
            of
            <%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).CardCount.ToString("N0")%>
        </li>
        
        <li style="float: right;">
            <ul class="color ulNextPrev">
                <% if ((ViewData["Previous"] as PostSecret.ViewModels.CardViewModel) != null)
                   { %>
                <li><a id="aPrevious" href="<%: Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes((ViewData["Previous"] as PostSecret.ViewModels.CardViewModel).uid)+"/" + Boomers.Utilities.Text.StringExt.ToSearchEngineFriendly((ViewData["Previous"] as PostSecret.ViewModels.CardViewModel).Title)) %>">
                    Previous</a> </li>
                <%} %>
                <%if ((ViewData["Next"] as PostSecret.ViewModels.CardViewModel) != null)
                  { %>
                <li><a id="aNext" href="<%: Url.Content("~/PostCards/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes((ViewData["Next"] as PostSecret.ViewModels.CardViewModel).uid)+"/" + Boomers.Utilities.Text.StringExt.ToSearchEngineFriendly((ViewData["Next"] as PostSecret.ViewModels.CardViewModel).Title)) %>">
                    Next</a></li><%} %>
            </ul>
        </li>
       
    </ul>
    <div class="divPoster">
        <center>
            <img style="max-height: 720px; max-width: 720px;" src="<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Url %>" /></center>
    </div>
    <div class="divLikeTags"><center>
     <span>  <a href="<%: Url.Content("~/Orders/Buy/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes((ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid)) %>">
                    <img width="80px" src="<%: Url.Content("~/img/buyNow.png") %>" class="hover imgExtras" /></a>   </span> <span
                    class='st_facebook_hcount' displaytext='Facebook' 
                   ></span><span
                        class='st_twitter_hcount' displaytext='Tweet' st_title='<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Title %>'
                        ></span><span
                            class='st_tumblr_hcount' displaytext='Tumblr' st_title='<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Title %>'
                            ></span>
                             <span><a href="http://pinterest.com/pin/create/button/?url=<%:Boomers.Utilities.Web.Utilities.GetSiteRoot()+ Url.Content("~/Posters/" + Boomers.Utilities.Guids.GuidExt.RemoveDashes((ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid)) %>&media=<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Url %>" class="pin-it-button" count-layout="vertical">Pin It</a></span>
                            </center>
        </div>
        <% if ((ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Title != null)
       { %>
    <div class="color">
        <%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Title%>
        : Post Secret PostCard<br />
    </div>
    <%} %>
    <div class="color divPosterBottom">
        <div class="PosterBottomBar">
            <ul class="ulGenericHorList">
                <li>
                    <% if (((int)ViewData["Favorite"] == 0) && Request.IsAuthenticated)
                       { %>
                    <img src="<%: Url.Content("~/img/star-off.png") %>" width="40px" class="hover" onclick="FavoriteOn(this,'<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>')" />
                    <% }
                       else if (!Request.IsAuthenticated)
                       { %>
                    <img src="<%: Url.Content("~/img/star-off.png") %>" width="40px" onclick="LogIn(this)"
                        class="hover" />
                    <%}
                       else if ((int)ViewData["Favorite"] > 0)
                       { %>
                    <img src="<%: Url.Content("~/img/star-on.png") %>" width="40px" class="hover" onclick="FavoriteOff(this,'<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>')" />
                    <%} %>
                    <b><span id="FavoriteCount">
                        <%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).FavoriteCount%></b>
                    Favorites</span></li>
                <li>| </li>
                <li>
                    <img src="<%: Url.Content("~/img/vote-arrow-up-off.png") %>" width="40px" onclick="VoteUp('<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>')"
                        id="imgVoteUp<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>"
                        class="hover imgExtras" />
                    <b><span id="VoteCountUp<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>">
                        <%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).VotesUp%></span></b>
                    Ups</li>
                <li>| </li>
                <li><b>
                    <%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Comments.Count %></b>
                    Comments</li>
                <li>| </li>
                <li><b>
                    <%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Views%></b> Views</li>
                <li>| </li>
                <li><span class="spanLink" onclick="javascript:FlagBroken(this, '<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>')">
                    Flag Broken</span></li>
                <li>| </li>
                <li><span class="spanLink" onclick="javascript:NotCard(this, '<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>')">
                    Not a PostCard?</span></li>
                <li>| </li>
                <li id="liDup<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>">
                    <span class="spanLink" onclick="javascript:FlagDuplicateSetup(this, '<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>')">
                        Flag Duplicate</span></li>
            </ul>
        </div>
        
        <div class="divContent">
            <div class="divp">
                What does this postcard say?</div>
            <ul id="ulContents" class="ulContents">
                <% foreach (var content in (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).ContentSurvey)
                   { %>
                <li class="">
                    <ul class="ulContVote">
                        <li>
                            <img src="<%: Url.Content("~/img/vote-arrow-up-off.png") %>" width="30px" onclick="VoteUpContent('<%: content.ContentID %>')"
                                id="imgVoteUp<%:content.ContentID %>" class="hover imgExtras vuImg" />
                        </li>
                        <li id="VoteCountUp<%:content.ContentID %>">
                            <%:content.VotesUp %>
                        </li>
                    </ul>
                    <div class="bgWhite colorb divContentComment">
                        <%: content.CommentHTML%>
                        <div class="divBottomComment">
                            <span>
                                <%: Boomers.Utilities.DatesTimes.DateExt.ToLongRelativeDate(content.DateTimeSubmitted)%></span><span
                                    class="spanLink" onclick="Javascript:ReportItem(this, '<%: content.ContentID %>')">
                                    Inappropriate?</span></div>
                    </div>
                </li>
                <%} %>
            </ul>
            <hr />
            <div>
                <textarea class="taAddContent" id="taContent"></textarea>
                <input type="button" value="Tell Us Whats On The Postcard" class="btnContent" onclick="AddContentSurvey('<%: Boomers.Utilities.Guids.GuidExt.RemoveDashes((ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid) %>')" />
            </div>
        </div>
        <div class="divComments">
            <ul class="ulComments" id="ulComments">
                <% foreach (var comment in (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).Comments)
                   { %>
                <li class="bgWhite colorb">
                    <%: comment.CommentHTML %>
                    <div class="divBottomComment">
                        <span>
                            <%: Boomers.Utilities.DatesTimes.DateExt.ToLongRelativeDate(comment.DateTimeSubmitted) %></span><span
                                class="spanLink" onclick="Javascript:ReportItem(this, '<%: comment.CommentID %>')">
                                Inappropriate?</span></div>
                </li>
                <%} %>
            </ul>
            <hr />
            <div>
                <textarea class="taAddComment" id="taComment"></textarea>
                <input type="button" value="Comment The Card" class="btnComment" onclick="AddComment('<%: Boomers.Utilities.Guids.GuidExt.RemoveDashes((ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid) %>')" />
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
    <div class="color sideTitle">
        Tags</div>
    <% Html.RenderPartial("TagsListPartialView"); %>
    <center>
        <script type="text/javascript"><!--
            google_ad_client = "ca-pub-6494646249414123";
            /* PostSecretSkyScraper */
            google_ad_slot = "6985393558";
            google_ad_width = 160;
            google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></center>
    <script type="text/javascript">
        $(document).ready(function () {
            UpdateViewCount('<%: (ViewData["Poster"] as PostSecret.ViewModels.CardViewModel).uid%>');
        });
    </script>
</asp:Content>
