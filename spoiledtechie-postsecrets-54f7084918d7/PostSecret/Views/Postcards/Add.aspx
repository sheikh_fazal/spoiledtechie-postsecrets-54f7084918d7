﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Add
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <form id="AddForm" action="/PostCards/AddPoster" method="post" enctype="multipart/form-data">
    <%--
    <div class="divContent">
        <div class="divIContent">
            <form action="/Document.mvc/UpdateDocument/" method="post" enctype="multipart/form-data">
            <% if (!Request.IsAuthenticated)
               { %><ul class="ulGenericHorList">
                   <li><span class="spanFontLarger bold">Your Nickname </span>
                       <%: Html.TextBox("nickName", null, new { @class = "tbOthers" })%>
                       <%: Html.ValidationMessage("nickName")%>
                   </li>
                   <li><span class="spanFontLarger bold">Email </span><span class="spanFontSmaller">(optional)</span>
                       <%: Html.TextBox("email", null, new { @class = "tbOthers" })%></li>
               </ul>
            <hr />
            <%} %>
            <div class="divAddForm">
                <span class="spanFontLarger bold">Title </span>
                <%: Html.TextBox("tbTitle", null, new { @class = "tbTitle" })%></div>
            <div class="divAddForm">
                <span class="spanFontLarger bold">Description </span>
                <%: Html.TextBox("tbDescription", null, new { @class = "tbTitle" })%></div>
            <div class="divAddForm">
                <span class="spanFontLarger bold">Choose File </span>
                <input type="file" id="filePicture" name="filePicture" />
                OR <span class="spanFontLarger bold">Link to File </span>
                <%: Html.TextBox("tbFileLink", null, new { @class = "tbFileLink" })%>
            </div>
            <div class="divAddForm">
                <span class="spanFontLarger bold">Tags </span>
                <%: Html.TextBox("tagsRecall", null, new { @class = "tbTags" })%>
            </div>
            </form>
        </div>
    </div>
      <h1 class="hdr">
        Add a Poster</h1>
    <h2 class="hdr">
        A Poster Added is a Poster Celebrated!</h2>
        <div class="floatRight color">DePostSecret.com</div>--%>
    <div class="divContent">
        <div class="divIContent">
            
            <% if (!Request.IsAuthenticated)
               { %><ul class="ulGenericHorList">
                   <li><span class="spanFontLarger bold">Your Nickname </span>
                       <%: Html.TextBox("nickName", null, new { @class = "tbOthers required" })%>
                     
                   </li>
                   <li><span class="spanFontLarger bold">Email </span><span class="spanFontSmaller">(optional)</span>
                       <%: Html.TextBox("email", null, new { @class = "tbOthers" })%></li>
               </ul>
            <hr />
            <%} %>
            <div class="divAddForm">
                <span class="spanFontLarger bold">Choose File </span>
                <input type="file" id="file1" name="filePicture" />
                OR <span class="spanFontLarger bold">Link to File </span>
                <%: Html.TextBox("tbFileLink", null, new { @class = "tbFileLink" })%>
            </div>
            <div class="divAddForm">
                <span class="spanFontLarger bold">Tags </span>
                <%: Html.TextBox("tagsRecall", null, new { @class = "tbTags required" })%>
            </div>
          
        </div>
    </div>
    <h1 class="hdr">
        Title
        <%: Html.TextBox("tbTitle", null, new { @class = "tbTitle required" })%></h1>
    <h2 class="hdr">
        Description
        <%: Html.TextBox("tbDescription", null, new { @class = "tbDesc required" })%>
    </h2>
    <h2 class="hdr"><input type="submit" value="Create Poster" /></h2>
    <div class="floatRight color">
        DePostSecret.com</div>
    <script type="text/javascript">
        $("#tagsRecall").autocomplete('<%=Url.Action("LookupTags", "Utilities") %>');

        $(function () {
            $("#tbTitle").watermark("Dumb");
            $("#tbTitle").click(function () {
                $("#tbTitle")[0].focus();
            });
        });
        $(function () {
            $("#tbDescription").watermark("You Have No Idea What Dumb Really Means");
            $("#tbDescription").click(
			function () {
			    $("#tbDescription")[0].focus();
			}
		);
        });
        $(function () {
            $("#tagsRecall").watermark("At least one Tag (Guns, Drugs, Injury) seperated by a Comma");
            $("#tagsRecall").click(
			function () {
			    $("#tagsRecall")[0].focus();
			}
		);
        });
    </script>
     <script type="text/javascript">
         $(document).ready(function () {
             $("#AddForm").validate({ messages: {
                 nickName: "*",
                 tagsRecall: "*",
                 tbTitle: "*",
                 tbDescription:"*"
             },
                 errorClass: "error"
             });
         });
    </script>
      </form>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
    <div class="divContent">
        <div class="divIContent">
           To Create a Poster

           <ul class="ulListToDo"><li class="liListToDo">
           Add your nickname so we can call you something
           </li>
           <li class="liListToDo">You can enter in your email if you wish.  Its totally optional and NEVER shown</li>
           <li>
           Now choose a picture by entering the link or picking one off your computer
           </li>
           <li class="liListToDo">Add at least one tag.  We are hoping to relate posters to each other this way.</li>
           <li class="liListToDo">
           Add a Title to the Poster. Down below.  Its Something that just makes sense to the picture
           </li>
           <li class="liListToDo">
           Add a Description. A punchline that a person wouldn't actually think of right off the bat.
           </li>

           </ul>
           
           </div>
    </div>
</asp:Content>
