﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/AdminSite/SiteAdmin.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Flagged Content
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Flagged Content</h2>
    <table>
        <thead>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                    Flag ID
                </td>
                <td>
                    Type
                </td>
                <td>
                    Date Flagged
                </td>
                <td>
                    Game/Comment ID
                </td>
                <td>
                    Name
                </td>
                <td>
                    Summary
                </td>
                <td>
                    Data
                </td>
            </tr>
        </thead>
        <% foreach (var item in (List<PostSecret.ViewModels.FlaggedViewModel>)(ViewData["Flagged"]))
           { %>
        <tr>
            <td>
                <span class="spanLink" onclick="javascript:RemoveFlag('<%: item.Uid %>');">Remove Flag</span>
            </td>
            <td>
                <span class="spanLink" onclick="javascript:RemoveFlaggedItem('<%: item.FlaggedID %>', '<%:item.Type %>');">
                    Censor Flag (Remove It)</span>
            </td>
            <td>
                <%: item.FlaggedID %>
            </td>
            <td>
                <%: item.Type %>
            </td>
            <td>
                <%: item.DateTime_Flagged %>
            </td>
            <%if (item.Card != null)
              { %>
            <td>
                <%: item.Card.uid%>
            </td>
            <td>
                <%: item.Card.Title%>
            </td>
            <td>
                <img src="<%: item.Card.Url%>" />
            </td>
            <%} %>
            <%if (item.Comment != null)
              { %>
            <td>
                <%: item.Comment.CommentID %>
            </td>
            <td>
                <%: item.Comment.CommentHTML%>
            </td>
            <%} %>
            <%if (item.data != null)
              { %>
            <td>
                <%try
                  { %>
                <a href="<%: item.data.Split(' ')[1] %>">
                    <%: item.data.Split(' ')[1]%></a>
                <%}
                  catch { } %>
            </td>
            <%} %>
        </tr>
        <%} %>
    </table>
</asp:Content>
