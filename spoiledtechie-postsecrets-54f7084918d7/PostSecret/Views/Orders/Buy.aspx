﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Buy This PostSecret Card
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="divMainBuyPage color">
        <h2>
            Buying the PostSecret Card below</h2>
        <div style="padding:5px; text-align:center; font-size:1.2em;">We will print the card below and send it straight to your front door.</div>
        <div style="max-width: 400px; float: left; min-height: 400px; margin-right: 20px;">
            <img src="<%: (ViewData["card"] as PostSecret.ViewModels.CardViewModel).Url %>" style="max-width: 400px;" />
        </div>
        <div style="background-color: #505050; border-top:1px solid #FFFFFF; float:left; padding:10px;">
            <%--<form id="Paypal" name="Paypal" action="https://www.sandbox.paypal.com/cgi-bin/webscr"
            method="post">--%>
            <form id="Paypal" name="Paypal" action="<%: Url.Content("~/Orders/PostPaypal")%>"
            method="post">
            <div style=" font-size:1.4em;font-weight:bold; text-align:center;">
                Cost: $1.97</div>
            <div style=" font-size:1.2em; font-weight:bold; text-align:center;">
                Shipping Information</div>
            <center>
                <table>
                    <tr>
                        <td>
                            First Name:
                        </td>
                        <td>
                            <%: Html.TextBox("first_name")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name:
                        </td>
                        <td>
                            <%: Html.TextBox("last_name")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td>
                            <%: Html.TextBox("email") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Street Address 1:
                        </td>
                        <td>
                            <%: Html.TextBox("address1")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Street Address 1:
                        </td>
                        <td>
                            <%: Html.TextBox("address2")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            City:
                        </td>
                        <td>
                            <%: Html.TextBox("city") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            State:
                        </td>
                        <td>
                            <%: Html.TextBox("state") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Zip Code:
                        </td>
                        <td>
                            <%: Html.TextBox("zip") %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif"
                                align="left" style="margin-right: 7px;" />
                        </td>
                    </tr>
                </table>
            </center>
            <%--<%=Html.Hidden("business", "seller_1302661346_biz@gmail.com")%>--%><!--Paypal or sandbox Merchant account -->
            <%--<%=Html.Hidden("currency_code", "USD")%>--%>
            <%--<%=Html.Hidden("return", "http://postsecretcollection.com/Orders/Return/c0f75f411ec34141b0751bc64c005d43")%>--%><!--this page will be your redirection page -->
            <%--<%=Html.Hidden("cancel_return", "http://http://utopiapimp.com/Orders/Buy/c0f75f411ec34141b0751bc64c005d43")%>--%>
            <%--<%=Html.Hidden("cmd", "_xclick")%>--%>
            <%--<%=Html.Hidden("notify_url", "http://www.yourwebsite.com/paypal.aspx")%>--%><!--this should be your domain web page where you going to receive all your transaction variables -->
            <%--<%=Html.Hidden("item_name", "PostSecret PostCard")%>--%>
            <%--<%=Html.Hidden("amount", "5.00")%>--%>
            <%=Html.Hidden("item_number", (ViewData["card"] as PostSecret.ViewModels.CardViewModel).uid)%>
            </form>
        </div>
        <div style="clear:both; text-align:center; font-size:1.2em;">** All proceeds will be donated towards suicide prevention. **</div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
</asp:Content>
