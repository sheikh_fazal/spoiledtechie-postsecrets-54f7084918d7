﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Thank You for Buying this PostSecret Card
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

   <div class="divMainBuyPage color">
        <h2>
            You Bought the Card Below, Thank You!</h2>
        <div style="padding:5px; text-align:center; font-size:1.4em;">Thank you for your Order, once we receive your payment your card will be sent.</div><br />
        <div style="max-width: 400px; float: left; min-height: 400px; margin-right: 20px;">
            <img src="<%: (ViewData["card"] as PostSecret.ViewModels.CardViewModel).Url %>" style="max-width: 400px;" />
        </div>
        <div style="background-color: #505050; border-top:1px solid #FFFFFF; float:left; padding:10px;">
           
            <div style=" font-size:1.4em;font-weight:bold; text-align:center;">
                Cost: $1.97</div>
            <div style=" font-size:1.2em; font-weight:bold; text-align:center;">
                Shipping Information</div>
            <center>
                <table>
                    <tr>
                        <td>
                            <b>First Name:</b>
                        </td>
                        <td>
                            <%: (ViewData["entity"] as PostSecret.Code.SupportFramework.Global_Entity).First_Name %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b> Last Name:</b>
                        </td>
                        <td>
                            <%: (ViewData["entity"] as PostSecret.Code.SupportFramework.Global_Entity).Last_Name%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b> Email:</b>
                        </td>
                        <td>
                            <%: (ViewData["email"] as PostSecret.Code.SupportFramework.Global_Entity_Email).Email_Address%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Street Address 1:</b>
                        </td>
                        <td>
                            <%: (ViewData["address"] as PostSecret.Code.SupportFramework.Global_Entity_Address).Address1%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Street Address 1:</b>
                        </td>
                        <td>
                            <%: (ViewData["address"] as PostSecret.Code.SupportFramework.Global_Entity_Address).Address2%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>City:</b>
                        </td>
                        <td>
                            <%: (ViewData["address"] as PostSecret.Code.SupportFramework.Global_Entity_Address).City%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>State:</b>
                        </td>
                        <td>
                            <%: (ViewData["address"] as PostSecret.Code.SupportFramework.Global_Entity_Address).State%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <b>Zip Code:</b>
                        </td>
                        <td>
                            <%: (ViewData["address"] as PostSecret.Code.SupportFramework.Global_Entity_Address).Zip%>
                        </td>
                    </tr>
                    
                </table>
            </center>
            
        </div>
        <div style="clear:both; text-align:center; font-size:1.2em;">** All proceeds will be donated towards suicide prevention. **</div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
</asp:Content>
