using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Boomers.Utilities.Communications;
using Boomers.Utilities.Guids;

namespace PostSecret.Code
{
    public partial class PSDataContext : System.Data.Linq.DataContext
    {
        partial void OnCreated()
        {
            this.CommandTimeout = int.MaxValue;
        }
    }
}

namespace PostSecret.SupportFramework
{
    /// <summary>
    /// Summary description for SQLStatementsCS
    /// </summary>
    public class DataAccessGlobal
    {
        public DataAccessGlobal()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        /// <summary>
        /// The connection string of the system.
        /// </summary>
        /// <returns></returns>
        public static string ConnectionStringID()
        { return ConfigurationManager.ConnectionStrings["UPConnectionString"].ConnectionString; }

        /// <summary>
        /// gets the UserID of the username.
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public static System.Guid UserID(string userName)
        {
            try
            {
                MembershipUser CurrentUser = Membership.GetUser(userName);
                return new System.Guid(CurrentUser.ProviderUserKey.ToString());
            }
            catch { return new Guid(); }
        }
        public static bool ChangeUserName(string oldUserName, string newUserName)
        {

            Code.AdminDataContext db = new Code.AdminDataContext();
            var checkUser = (from xx in db.aspnet_Users
                             from yy in db.vw_aspnet_Applications
                             where yy.LoweredApplicationName == Membership.ApplicationName.ToLower()
                             where xx.ApplicationId == yy.ApplicationId
                             where xx.LoweredUserName == newUserName.ToLower()
                             select xx.UserId).FirstOrDefault();
            if (checkUser != new Guid())
                return false;

            try
            {
                var getUser = (from xx in db.aspnet_Users
                               from yy in db.vw_aspnet_Applications
                               where yy.LoweredApplicationName == Membership.ApplicationName.ToLower()
                               where xx.ApplicationId == yy.ApplicationId
                               where xx.LoweredUserName == oldUserName.ToLower()
                               select xx).FirstOrDefault();
                getUser.UserName = newUserName;
                getUser.LoweredUserName = newUserName.ToLower();
                db.SubmitChanges();

                FormsAuthentication.SetAuthCookie(newUserName, false);

                return true;
            }
            catch { return false; }

        }

        public static System.Guid UserID()
        {
            if (HttpContext.Current.Session != null)
                if (HttpContext.Current.Session["userID"] != null)
                    return new Guid((string)HttpContext.Current.Session["userID"]);

            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                MembershipUser CurrentUser = Membership.GetUser();
                //HttpContext.Current.Session["userID"] = CurrentUser.ProviderUserKey.ToString();
                if (CurrentUser == null)
                    return new Guid();
                return new System.Guid(CurrentUser.ProviderUserKey.ToString());
            }
            else
                return new Guid();
        }
        public static Guid ApplicationId()
        {
            if (HttpRuntime.Cache["ApplicationID"] == null)
            {
                Code.AdminDataContext db = new Code.AdminDataContext();
                var getID = (from xx in db.aspnet_Applications
                             where xx.LoweredApplicationName == Membership.ApplicationName.ToLower()
                             select xx.ApplicationId).FirstOrDefault();
                HttpRuntime.Cache.Add("ApplicationID", getID.ToString(), null, System.Web.Caching.Cache.NoAbsoluteExpiration, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.NotRemovable, null);
            }
            return new Guid(HttpRuntime.Cache["ApplicationID"].ToString());
        }
     

        public static bool IsUserAdmin()
        {
            if (HttpContext.Current.Session["IsAdmin"] != null)
                return (bool)HttpContext.Current.Session["IsAdmin"];
            else
            {
                HttpContext.Current.Session["IsAdmin"] = Roles.IsUserInRole("admin");
                return (bool)HttpContext.Current.Session["IsAdmin"];
            }
        }

        public static string UserEmail()
        {
            if (HttpContext.Current.Session != null)
                if (HttpContext.Current.Session["userEmail"] != null)
                    return (string)HttpContext.Current.Session["userEmail"];

            MembershipUser CurrentUser = Membership.GetUser();
            if (CurrentUser == null)
                return string.Empty;
            if (HttpContext.Current.Session != null)
                HttpContext.Current.Session["userEmail"] = CurrentUser.Email;
            return CurrentUser.Email;
        }
        /// <summary>
        /// gets USer Email
        /// </summary>
        /// <param name="userName">Username for user</param>
        /// <returns>User Email</returns>
        public static string UserEmail(string userName)
        {
            MembershipUser CurrentUser = Membership.GetUser(userName);
            return CurrentUser.Email;
        }
        /// <summary>
        /// gets User Email from User ID.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>User name</returns>
        public static string UserEmail(Guid userID)
        {
            MembershipUser CurrentUser = Membership.GetUser(userID);
            if (CurrentUser == null)
                return "anonymous";
            return CurrentUser.Email;
        }
        /// <summary>
        /// gets Username from Login ID
        /// </summary>
        /// <param name="LoginID">Guid Login ID</param>
        /// <returns>Username</returns>
        public static string UserName(string userID)
        {
            if (userID.IsValidGuid())
            {
                MembershipUser CurrentUser = Membership.GetUser(new Guid(userID));
                if (CurrentUser == null)
                    return "anonymous";
                return CurrentUser.UserName;
            }
            else
                return null;
        }
        /// <summary>
        /// gets the user name if the current logged in user.
        /// </summary>
        /// <returns></returns>
        public static string UserName()
        {
            if (HttpContext.Current.User.Identity.Name == null)
                return "anonymous";
            else
            {
                if (HttpContext.Current.Session != null)
                    if (HttpContext.Current.Session["userName"] != null)
                        return HttpContext.Current.Session["userName"].ToString();
                HttpContext.Current.Session["userName"] = HttpContext.Current.User.Identity.Name;
                return HttpContext.Current.User.Identity.Name;
            }
        }
        public static void ChangeUserEmail(string userName, string userEmail)
        {
            MembershipUser mu = Membership.GetUser(userName);
            mu.Email = userEmail;
            Membership.UpdateUser(mu);
        }
        /// <summary>
        /// Caputes any data needed to a table.
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public static int DataCapture(string Data)
        {
            Code.SupportFramework.GlobalDataContext db = new Code.SupportFramework.GlobalDataContext();
            Code.SupportFramework.Global_Data_Capture gdc = new Code.SupportFramework.Global_Data_Capture();
            gdc.Raw_Data = Data;
            gdc.Date_Time = DateTime.UtcNow;
            gdc.User_ID = UserID();
            gdc.Application_Id = SupportFramework.DataAccessGlobal.ApplicationId();
            db.Global_Data_Captures.InsertOnSubmit(gdc);
            db.SubmitChanges();
            return gdc.uid;
        }

        public static void setError(string LastException, string ErrorURLPrevious, string ErrorURL, string ErrorMessage, string ErrorTrace, string ErrorTarget, string ErrorSource, string TraceError, Guid userID, string EmailofUser, DateTime LoadDate)
        {
            Code.SupportFramework.GlobalDataContext db = new Code.SupportFramework.GlobalDataContext();
            Code.SupportFramework.Global_Errors_Log UEL = new Code.SupportFramework.Global_Errors_Log();
            UEL.Date_Time = DateTime.UtcNow;
            UEL.Application_Id = SupportFramework.DataAccessGlobal.ApplicationId();
            UEL.Error_Message = ErrorMessage;
            UEL.Error_Previous_URL = ErrorURLPrevious;
            UEL.Error_Source = ErrorSource;
            UEL.Error_Target = ErrorTarget;
            UEL.Error_Trace = ErrorTrace;
            UEL.Error_URL = ErrorURL;
            UEL.Last_Exception = LastException;
            UEL.Load_Date = LoadDate.ToString();
            UEL.Trace_Error = TraceError;
            UEL.User_Email = EmailofUser;
            UEL.User_ID = userID;
            UEL.Version = AssemblyID.GetVersion();
            if (HttpContext.Current.Session != null)
                if (HttpContext.Current.Session["SubmittedData"] != null)
                    UEL.Session_Data = HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Session["SubmittedData"].ToString().Replace(Environment.NewLine, " "));

            db.Global_Errors_Logs.InsertOnSubmit(UEL);
            db.SubmitChanges();
        }
    }
}