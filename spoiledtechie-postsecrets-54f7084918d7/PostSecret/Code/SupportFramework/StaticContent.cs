﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI.HtmlControls;

namespace SupportFramework.Static
{
    public static  class Versioned
    {
       static  string _LastBuild = null;
        public static string LastBuild
        {
            get
            {
                if (String.IsNullOrEmpty(_LastBuild))
                {
                    _LastBuild = DateTime.UtcNow.ToString("yyyy.M.d.HH");
                }
                return _LastBuild;
            }
        }
    }
    /// <summary>
    /// Summary description for StaticContent
    /// </summary>
    public class JavaScript
    {
        string _JsVersion = null;
        public string JsVersion
        {
            get
            {
                if (String.IsNullOrEmpty(_JsVersion))
                {
                    try
                    {

                        string jsFile = System.Web.HttpContext.Current.Server.MapPath("~/js/Master.js");
                        FileInfo fi = new FileInfo(jsFile);
                        DateTime lastWriteTime = fi.LastWriteTime;
                        _JsVersion = lastWriteTime.ToString("yyyyMMddHHmmss");
                    }
                    catch
                    {
                        return "1";
                    }
                }
                return _JsVersion;
            }
        }
        HtmlGenericControl _JsDocument = null;

        public HtmlGenericControl JsDefault
        {
            get
            {
                if (_JsDocument == null)
                {
                    HtmlGenericControl js = new HtmlGenericControl();
                    js.TagName = "script";
                    SupportFramework.Static.JavaScript st = new SupportFramework.Static.JavaScript();
                    js.Attributes["src"] = String.Format("http://codingforcharity.org/utopiapimp/js/Master.js?v={0}", st.JsVersion);
                    js.Attributes["type"] = "text/javascript";
                    _JsDocument = js;
                }
                return _JsDocument;
            }
        }
    }
    public class CSS
    {
        string _CssVersion = null;
        public string CssVersion
        {
            get
            {
                if (String.IsNullOrEmpty(_CssVersion))
                {
                    try
                    {
                        string cssFile = System.Web.HttpContext.Current.Server.MapPath("~/css/default.css");
                        FileInfo fi = new FileInfo(cssFile);
                        DateTime lastWriteTime = fi.LastWriteTime;
                        _CssVersion = lastWriteTime.ToString("yyyyMMddHHmmss");
                    }
                    catch
                    {
                        return "1";
                    }
                }
                return _CssVersion;
            }
        }
        HtmlLink _CssDocument = null;

        public HtmlLink CssDefault
        {
            get
            {
                if (_CssDocument == null)
                {
                    HtmlLink css = new HtmlLink();
                    SupportFramework.Static.CSS st = new SupportFramework.Static.CSS();
                    css.Href = String.Format("http://codingforcharity.org/utopiapimp/css/default.css?v={0}", st.CssVersion);
                    css.Attributes["rel"] = "stylesheet";
                    css.Attributes["type"] = "text/css";
                    css.Attributes["media"] = "all";
                    _CssDocument = css;
                }
                return _CssDocument;
            }
        }



    }
    public class URLClass
    {
        string domainName = null;
        public string GetDomain
        {
            get
            {
                if (String.IsNullOrEmpty(domainName))
                    domainName = HttpContext.Current.Request.Url.Host.ToLower();

                return domainName;
            }

        }
    }
}