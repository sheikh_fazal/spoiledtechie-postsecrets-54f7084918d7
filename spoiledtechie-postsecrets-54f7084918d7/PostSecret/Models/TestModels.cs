﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.Models
{
    /// <summary>
    /// create by ahmad for testing/demo purpose only
    /// </summary>
    public class TestModels
    {
        public int id { get; set; }
        public string title { get; set; }
        public string image { get; set; }
        public int likes { get; set; }
        public int dislikes { get; set; }
        public int comments { get; set; }
        public int votes { get; set; }
        public int shareLinks { get; set; }
        public int brokenFlag { get; set; }
        public int notPostCard { get; set; }
        public int duplicateFlag { get; set; }
    }
}