﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PostSecret
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "PostCardsName", // Route name
"PostCards/{id}/{title}", // URL with parameters
                //new Regex(@"Stories/[a-zA-Z0-9]{32}", RegexOptions.Compiled)
new { controller = "PostCards", action = "PostCards" }, // Parameter defaults
new { id = "[a-zA-Z0-9]+", title = @"[a-zA-Z0-9\?\-\s]+" }
);
            routes.MapRoute(
      "PostCardsNameDescripton", // Route name
"PostCards/{id}/{title}/{description}", // URL with parameters
                //new Regex(@"Stories/[a-zA-Z0-9]{32}", RegexOptions.Compiled)
new { controller = "PostCards", action = "PostCards" }, // Parameter defaults
new { id = "[a-zA-Z0-9]+", title = @"[a-zA-Z0-9\?\-\s]+", description = @"[a-zA-Z0-9\?\-\s]+" }
);



            routes.MapRoute(
              "Buy", // Route name
              "Orders/Buy/{id}", // URL with parameters
                //new Regex(@"Stories/[a-zA-Z0-9]{32}", RegexOptions.Compiled)
              new { controller = "Orders", action = "Buy" }, // Parameter defaults
              new { id = "[a-zA-Z0-9]{32}" }
          );

            routes.MapRoute(
           "Return", // Route name
           "Orders/Return/{id}", // URL with parameters
                //new Regex(@"Stories/[a-zA-Z0-9]{32}", RegexOptions.Compiled)
           new { controller = "Orders", action = "Return" }, // Parameter defaults
           new { id = "[a-zA-Z0-9]{32}" }
       );

            routes.MapRoute(
              "PostCards", // Route name
              "PostCards/{id}", // URL with parameters
                //new Regex(@"Stories/[a-zA-Z0-9]{32}", RegexOptions.Compiled)
              new { controller = "PostCards", action = "PostCards" }, // Parameter defaults
              new { id = "[a-zA-Z0-9]{32}" }
          );
            routes.MapRoute(
        "TagsName", // Route name
        "Tag/{id}/{name}", // URL with parameters
                //new Regex(@"Stories/[a-zA-Z0-9]{32}", RegexOptions.Compiled)
        new { controller = "Tags", action = "Tag" }, // Parameter defaults
        new { id = "[0-9]*", name = @"([a-zA-Z0-9\?\-]+)" }
        );
            routes.MapRoute(
              "Tags", // Route name
              "Tag/{id}", // URL with parameters
                //new Regex(@"Stories/[a-zA-Z0-9]{32}", RegexOptions.Compiled)
              new { controller = "Tags", action = "Tag" }, // Parameter defaults
              new { id = "[0-9]*" }
          );
            routes.MapRoute(
                     "PageCards", // Route name
                     "PostCards/Cards", // URL with parameters
                     new { controller = "PostCards", action = "Cards" } // Parameter defaults
                 );
            routes.MapRoute(
                     "Top", // Route name
                     "PostCards/Top", // URL with parameters
                     new { controller = "PostCards", action = "Top" } // Parameter defaults
                 );
            routes.MapRoute(
             "PaypalIPN", // Route name
             "Paypal/IPN", // URL with parameters
                //new Regex(@"Stories/[a-zA-Z0-9]{32}", RegexOptions.Compiled)
             new { controller = "Paypal", action = "IPN" } // Parameter defaults

         );

            //routes.MapRoute(
            //               "Home", // Route name
            //               "{action}", // URL with parameters
            //               new { controller = "Home", action = "Index" } // Parameter defaults
            //           );
            routes.MapRoute(
                           "Home", // Route name
                           "{action}", // URL with parameters
                           new { controller = "Home", action = "Test" } // Parameter defaults
                       );
            routes.MapRoute(
            "Default", // Route name
            "{controller}/{action}/{id}", // URL with parameters
            new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
        );
            routes.MapRoute(
  "Admin", // Route name
  "Admin/{action}", // URL with parameters
  new { controller = "Admin", action = "Default", id = UrlParameter.Optional } // Parameter defaults
);
            routes.MapRoute(
             "AdminSite", // Route name
             "AdminSite/{action}", // URL with parameters
             new { controller = "AdminSite", action = "Default", id = UrlParameter.Optional } // Parameter defaults
         );



        }
        protected void Application_Error(object sender, EventArgs e)
        {
            var error = Server.GetLastError();
            string errorPrevURL = string.Empty;
            if (HttpContext.Current.Request.UrlReferrer != null)
                errorPrevURL = Server.HtmlEncode(HttpContext.Current.Request.UrlReferrer.ToString().Replace(Environment.NewLine, " "));
            SupportFramework.DataAccessGlobal.setError(error.ToString(), errorPrevURL, HttpContext.Current.Request.Url.ToString(), error.Message.ToString(), error.StackTrace.ToString(), error.TargetSite.ToString(), error.Source.ToString(), error.StackTrace.ToString(), SupportFramework.DataAccessGlobal.UserID(), SupportFramework.DataAccessGlobal.UserEmail(), DateTime.UtcNow);
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);
            //RouteDebug.RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);
        }
    }
}