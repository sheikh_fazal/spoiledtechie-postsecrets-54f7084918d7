/*
   Tuesday, January 11, 201111:06:15 PM
   User: 
   Server: .\SQLEXPRESS2010
   Database: 
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.PS_Rss_Downloaded
	(
	uid int NOT NULL IDENTITY (1, 1),
	Id varchar(MAX) NOT NULL,
	TimeStamp datetime NOT NULL,
	Url varchar(MAX) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.PS_Rss_Downloaded ADD CONSTRAINT
	PK_PS_Rss_Downloaded PRIMARY KEY CLUSTERED 
	(
	uid
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.PS_Rss_Downloaded SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PS_Rss_Downloaded', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PS_Rss_Downloaded', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PS_Rss_Downloaded', 'Object', 'CONTROL') as Contr_Per 