﻿$(function () {
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
            var lastDivId = $('.last').attr('id');
            alert("Last Div " + lastDivId);

            $.ajax({
                method: 'POST',
                datatype: "json",
                data: {},
                contentType: "application/json; charset=utf-8",
                url: 'Home/nextRecords?id=' + lastDivId,
                success: function (response) {
                    var data = JSON.parse(response);
                    var listContiner = $('#list-container');
                    var lastItem = data.length;

                    var divHtml = '';

                    $.each(data, function (i, item) {
                        console.log(data[i].id + data[i].message);
                        if (i === (lastItem - 1)) {
                            $('#' + lastDivId).removeAttr('class');
                            divHtml += '<div id="' + data[i].id + '" class="last">';
                            divHtml += '<h1>' + data[i].id + '</h1>';
                            divHtml += '<p>' + data[i].message + '</p>';
                            divHtml += '</div>';
                        } else {
                            divHtml += '<div id="' + data[i].id + '">';
                            divHtml += '<h1>' + data[i].id + '</h1>';
                            divHtml += '<p>' + data[i].message + '</p>';
                            divHtml += '</div>';
                        }
                    });

                    listContiner.append(divHtml);
                }
            });
        }
    });
});