﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class ErrorViewModel
    {
        public Guid Application_Id { get; set; }
        public DateTime? Date_Time { get; set; }
        public string Error_Message { get; set; }
        public string Error_Previous_URL { get; set; }
        public string Error_Source { get; set; }
        public string Error_Target { get; set; }
        public string Error_Trace { get; set; }
        public string Error_URL { get; set; }
        public string Last_Exception { get; set; }
        public string Load_Date { get; set; }
        public int Reviewed { get; set; }
        public string Session_Data { get; set; }
        public string Trace_Error { get; set; }
        public int uid { get; set; }
        public string User_Email { get; set; }
        public string User_Name { get; set; }
        public Guid? User_ID { get; set; }
        public string Version { get; set; }
    }
}