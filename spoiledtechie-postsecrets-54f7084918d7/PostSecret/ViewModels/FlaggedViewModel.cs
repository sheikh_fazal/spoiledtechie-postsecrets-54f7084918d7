﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class FlaggedViewModel
    {
        public int Uid { get; set; }
        public DateTime DateTime_Flagged { get; set; }
        public Guid FlaggedID { get; set; }
        public string Type { get; set; }
        public CommentViewModel Comment { get; set; }
        public CardViewModel Card { get; set; }
        public string data { get; set; }
    }
}