﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class VoteViewModel
    {
        public int uid { get; set; }
        public Guid ItemGuid { get; set; }
        public int Vote { get; set; }
        public Guid? UserID { get; set; }
        public string IPAddress { get; set; }
        public DateTime DateTimeSubmitted { get; set; }
    }
}