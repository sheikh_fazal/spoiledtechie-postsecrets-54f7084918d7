﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class FavoriteViewModel
    {
        public Guid Item_Guid { get; set; }
        public int uid { get; set; }
        public Guid User_Id { get; set; }
    }
}