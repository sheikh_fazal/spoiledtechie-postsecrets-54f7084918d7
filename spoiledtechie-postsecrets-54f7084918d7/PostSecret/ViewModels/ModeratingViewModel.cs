﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class ModeratingViewModel
    {
        public Guid ID { get; set; }
        public string Title { get; set; }
        public string OneLiner { get; set; }
        public List<Moderations> Mods {get;set;}
    }
    public class Moderations
    {
        public int uid { get; set; }
        public Guid ID_Moderating { get; set; }
        public int Vote { get; set; }
        public string IPAddress { get; set; }
        public Guid UserID { get; set; }
    }
}