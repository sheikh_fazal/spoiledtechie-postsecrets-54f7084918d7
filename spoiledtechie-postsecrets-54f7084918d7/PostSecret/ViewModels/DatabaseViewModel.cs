﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class DatabaseViewModel
    {
        public string database_name;
        public string database_size;
        public string unallocated_space;
        public string reserved;
        public string data;
        public string index_size;
        public string unused;


    }
    public class DatabaseViewModel2
    {
        public string database_name;
        public string database_size;
        public string unallocated_space;
        public string reserved;
        public string data;
        public string index_size;
        public string unused;
        public string TotalRows { get; set; }
        public string TotalTables { get; set; }

    }
    public class DataTables
    {
        public string name;
    }
    public class TableInfo
    {
        public string name;
        public string rows;
        public string reserved;
        public string data;
        public string index_size;
        public string unused;
    }
    public class DatabaseInfos
    {
        public string database_name;
        public string database_size;
        public string unallocated_space;
        public string reserved;
        public string data;
        public string index_size;
        public string unused;

    }
}