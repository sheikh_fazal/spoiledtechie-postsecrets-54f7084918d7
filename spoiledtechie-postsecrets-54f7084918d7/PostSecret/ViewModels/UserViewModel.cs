﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class UserViewModel
    {
        public string UserName { get; set; }
        public Guid UserID { get; set; }
        public int Points { get; set; }
        public string Approved { get; set; }
        public string Comments { get; set; }
        public string CreateDate { get; set; }
        public string Email { get; set; }
        public string LastLogin { get; set; }
        public string Locked { get; set; }
        public string PasswordQuestion { get; set; }
        public string[] Roles { get; set; }
        public int UserErrors { get; set; }
        public bool UserOnline { get; set; }
        public int UserPageViews { get; set; }
    }
}