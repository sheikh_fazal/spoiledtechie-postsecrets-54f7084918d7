﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class TagViewModel
    {
        public int TagID { get; set; }
        public string TagName { get; set; }
        public DateTime DateTime_Submitted { get; set; }
        public int TagCount { get; set; }
        public List<CardViewModel> Poster{ get; set; }
    }
}