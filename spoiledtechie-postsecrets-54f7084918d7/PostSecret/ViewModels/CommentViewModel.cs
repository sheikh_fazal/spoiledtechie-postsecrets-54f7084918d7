﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class CommentViewModel
    {
        public Guid ContentID { get; set; }
        public Guid CommentID { get; set; }
        public Guid CommentOwnerID { get; set; }
        public string CommentHTML { get; set; }
        public Guid UserID { get; set; }
        public int VotesUp { get; set; }
        public int VotesDown { get; set; }
        public DateTime DateTimeSubmitted { get; set; }
    }
}