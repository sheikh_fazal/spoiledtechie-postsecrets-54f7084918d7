﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PostSecret.ViewModels
{
    public class ImagesViewModel
    { 
        
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string SavePath { get; set; }
        public DateTime DateTimeAdded { get; set; }
        public string IpAddress { get; set; }
        public Guid? UserIDAdded { get; set; }
    }
}