﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

using Boomers.Utilities.Text;

namespace PostSecret.ViewModels
{
    public class CardViewModel
    {
        Code.PSDataContext _database;

        public CardViewModel()
        {
            _database = new Code.PSDataContext();
        }

        private void PopulateCard(CardViewModel card)
        {
            this.CardCount = card.CardCount;
            this.CommentCount = card.CommentCount;
            this.Comments = card.Comments;
            this.ContentSurvey = card.ContentSurvey;
            this.DateTimeAdded = card.DateTimeAdded;
            this.FavoriteCount = card.FavoriteCount;
            this.Favorites = card.Favorites;
            this.IPAddress = card.IPAddress;
            this.NickName = card.NickName;
            this.Number = card.Number;
            this.PostCard_Id = card.PostCard_Id;
            this.SavePath = card.SavePath;
            this.Tags = card.Tags;
            this.Title = card.Title;
            this.uid = card.uid;
            this.Url = card.Url;
            this.UserId = card.UserId;
            this.Views = card.Views;
            this.Votes = card.Votes;
            this.VotesDown = card.VotesDown;
            this.VotesUp = card.VotesUp;
        }
        public CardViewModel GetCard(Guid id)
        {

            PopulateCard((from xx in _database.PS_Cards
                          from aa in _database.PS_Postcards
                          where xx.Card_Id == aa.uid
                          where xx.uid == id
                          select new CardViewModel
                          {
                              CardCount = (from qq in _database.PS_Cards
                                           select qq.uid).Count(),
                              uid = xx.uid,
                              Number = (from qq in _database.PS_Cards
                                        where qq.DateTime_Added > xx.DateTime_Added
                                        select qq.uid).Count(),
                              FavoriteCount = xx.Favorites,
                              Favorites = (from nn in _database.PS_Favorites
                                           where nn.Item_Guid == xx.uid
                                           select new FavoriteViewModel
                                           {
                                               Item_Guid = nn.Item_Guid,
                                               uid = nn.uid,
                                               User_Id = nn.User_ID
                                           }).ToList(),
                              Title = xx.Title,
                              Url = aa.PostCard_UrlNew,
                              SavePath = aa.Save_Location,
                              NickName = xx.NickName,
                              PostCard_Id = xx.Card_Id,
                              UserId = xx.User_ID,
                              IPAddress = xx.IP_Address,
                              DateTimeAdded = (DateTime)xx.DateTime_Added,
                              VotesUp = xx.Votes_Up,
                              VotesDown = xx.Votes_Down,
                              Views = xx.Views,
                              Votes = (from nn in _database.PS_Votes
                                       where nn.Item_Guid == xx.uid
                                       select new VoteViewModel
                                       {
                                           IPAddress = nn.User_IP_Address,
                                           Vote = nn.Vote,
                                           ItemGuid = nn.Item_Guid,
                                           UserID = nn.User_ID,
                                       }).ToList(),
                              Tags = (from zz in _database.PS_Tags
                                      from cc in _database.PS_Tags_Pulls
                                      where zz.Tag_ID == cc.uid
                                      where zz.Item_ID == xx.uid
                                      select new TagViewModel
                                      {
                                          TagName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cc.Tag_Name).ToSearchEngineFriendly(),
                                          TagID = zz.Tag_ID,
                                          TagCount = (from yy in _database.PS_Tags
                                                      where yy.Tag_ID == zz.Tag_ID
                                                      select yy.Tag_ID).Count()
                                      }).Distinct().ToList(),
                              Comments = (from cc in _database.PS_Comments
                                          where cc.Comment_To_ID == xx.uid
                                          orderby cc.DateTime_Submitted
                                          select new CommentViewModel
                                          {
                                              VotesUp = cc.Votes_Up,
                                              VotesDown = cc.Votes_Down,
                                              CommentID = cc.uid,
                                              CommentHTML = cc.Comment_Html,
                                              DateTimeSubmitted = cc.DateTime_Submitted
                                          }).ToList(),
                              ContentSurvey = (from cc in _database.PS_Comments
                                               where cc.Content_To_Id == xx.uid
                                               orderby cc.Votes_Up descending
                                               select new CommentViewModel
                                               {
                                                   VotesUp = cc.Votes_Up,
                                                   VotesDown = cc.Votes_Down,
                                                   ContentID = cc.uid,
                                                   CommentHTML = cc.Comment_Html,
                                                   DateTimeSubmitted = cc.DateTime_Submitted
                                               }).ToList(),
                          }).FirstOrDefault());
            return this;
        }
        /// <summary>
        /// Renames the card pictures and puts postsecret_postcard in the name of all the cards.
        /// </summary>
        public void RenameAllNonPostSecretNamedCards()
        {
            string newHeaderName = "postsecret_postcard";

            var cards = (from xx in _database.PS_Postcards
                         where !xx.PostCard_UrlNew.Contains("postsecret_postcard")
                         select xx);
            foreach (var card in cards)
            {
                try
                {

                   FileInfo fileInfoSavePath = new FileInfo(card.Save_Location);

                    string directorySavePath = fileInfoSavePath.FullName;
                    string newFileName = newHeaderName + "_" + fileInfoSavePath.Name;
                    string fileInfoUrl = card.PostCard_UrlNew;

                    directorySavePath = directorySavePath.Replace(fileInfoSavePath.Name, newFileName);

                    FileInfo fileInfoSavePath1 = new FileInfo(directorySavePath);

                    //FileInfo fileInfoUrl = new FileInfo("http://images.codingforcharity.org/ps/2009/8/9/bulls.jpg");

                    if (fileInfoSavePath1.Exists)
                    {
                        //string directorySavePath = fileInfoSavePath.FullName;
                        //string newFileName = newHeaderName + "_" + fileInfoSavePath.Name;
                        //string fileInfoUrl = card.PostCard_UrlNew;

                        //directorySavePath = directorySavePath.Replace(fileInfoSavePath.Name, newFileName);
                        fileInfoUrl = fileInfoUrl.Replace(fileInfoSavePath.Name, newFileName);
                        //try
                        //{
                        //    fileInfoSavePath.CopyTo(directorySavePath);
                        //}
                        //catch (Exception exception)
                        //{
                        //    Boomers.Utilities.Documents.TextLogger.LogItem("postsecret", "Failed To Copy " + card.uid + " " + exception.ToString());
                        //}

                        try
                        {
                            card.PostCard_UrlNew = fileInfoUrl;
                            card.Save_Location = directorySavePath;
                            _database.SubmitChanges();
                            //if (File.Exists(directorySavePath))
                            //{
                            //    fileInfoSavePath.Delete();
                            //}
                        }
                        catch (Exception exception)
                        {
                            Boomers.Utilities.Documents.TextLogger.LogItem("postsecret", "Failed Insert/Delete " + card.uid + " " + exception.ToString());
                        }
                    }
                }
                catch (Exception exception)
                {
                    Boomers.Utilities.Documents.TextLogger.LogItem("postsecret", "Failed Find Card " + card.uid + " " + exception.ToString());
                }
            }
        }

        public int orderbyId { get; set; }
        public Guid uid { get; set; }
        public int Number { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string SavePath { get; set; }
        public Guid PostCard_Id { get; set; }
        public DateTime DateTimeAdded { get; set; }
        public Guid? UserId { get; set; }
        public string IPAddress { get; set; }
        public string NickName { get; set; }
        public int CommentCount { get; set; }
        public int CardCount { get; set; }
        public int VotesUp { get; set; }
        public int VotesDown { get; set; }
        public int Views { get; set; }
        public int FavoriteCount { get; set; }
        public List<CommentViewModel> ContentSurvey { get; set; }
        public List<CommentViewModel> Comments { get; set; }
        public List<TagViewModel> Tags { get; set; }
        public List<FavoriteViewModel> Favorites { get; set; }
        public List<VoteViewModel> Votes { get; set; }
    }
}