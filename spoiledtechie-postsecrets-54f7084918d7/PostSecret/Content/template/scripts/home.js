﻿$(function () {
    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= $(document).height() - $(window).height() - 10) {
            var lastDivId = $('.last').attr('id');
            console.log("Last Div " + lastDivId);

            $.ajax({
                method: 'POST',
                datatype: "json",
                data: {},
                contentType: "application/json; charset=utf-8",
                url: 'Home/nextRecords?id=' + lastDivId,
                success: function (response) {
                    var data = JSON.parse(response);
                    var listContiner = $('.posts');
                    var lastItem = data.length;

                    var divHtml = '';

                    $.each(data, function (i, item) {
                        console.log(data[i].id + data[i].message);
                        if (i === (lastItem - 1)) {
                            $('#' + lastDivId).removeClass('last');
                            divHtml += '<div id="' + data[i].id + '" class="container last">';
                            divHtml += '<div class="panel panel-default margin-top">';
                            divHtml += '<div class="panel-heading" style="width: 100%;">';
                            divHtml += '<div class="" style="float: left;">' + data[i].title + '</div>';
                            divHtml += '<div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">';
                            divHtml += '<button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">';
                            divHtml += '<span class="fa fa-ellipsis-h fa-lg"></span></button>';
                            divHtml += '<ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">';
                            divHtml += '<li><a href="#">Flag as Broken</a></li>';
                            divHtml += '<li><a href="#">Flag as Duplicate</a></li>';
                            divHtml += '<li role="separator" class="divider"></li>';
                            divHtml += '<li><a href="#">Not a PostCard</a></li></ul>';
                            divHtml += '</div></div>';
                            divHtml += '<div class="panel-body left">';
                            divHtml += '<div class="image-container">';
                            divHtml += '<img alt="post image" src="' + data[i].image + '" class="post-image img-responsive" /></div>';
                            divHtml += '<div class="control-group">';
                            divHtml += '<button class="btn btn-link" title="Like">';
                            divHtml += '<img class="link-image" alt="Like" src="../../Content/template/img/thumb32x32.png" />';
                            divHtml += '<span>' + data[i].likes + '</span>';
                            divHtml += '</button>';
                            divHtml += '<button class="btn btn-link" title="Dislike">';
                            divHtml += '<img class="link-image" alt="Dislike" src="../../Content/template/img/thumb-unlike32x32.png" />';
                            divHtml += '<span>' + data[i].dislikes + '</span></button>';
                            divHtml += '<button class="btn btn-link" title="Comment">';
                            divHtml += '<img class="link-image" alt="Comment" src="../../Content/template/img/comments32x32.png" />';
                            divHtml += '<span>' + data[i].comments + '</span>';
                            divHtml += '</button></div></div></div></div>';
                        }
                        else {
                            divHtml += '<div id="' + data[i].id + '" class="container">';
                            divHtml += '<div class="panel panel-default margin-top">';
                            divHtml += '<div class="panel-heading" style="width: 100%;">';
                            divHtml += '<div class="" style="float: left;">' + data[i].title + '</div>';
                            divHtml += '<div class="dropdown" style="float: right; right: auto; padding-right: 0; margin-right: 0;">';
                            divHtml += '<button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown">';
                            divHtml += '<span class="fa fa-ellipsis-h fa-lg"></span></button>';
                            divHtml += '<ul class="dropdown-menu dropdown-menu-right pull-right" style="overflow: auto;">';
                            divHtml += '<li><a href="#">Flag as Broken</a></li>';
                            divHtml += '<li><a href="#">Flag as Duplicate</a></li>';
                            divHtml += '<li role="separator" class="divider"></li>';
                            divHtml += '<li><a href="#">Not a PostCard</a></li></ul>';
                            divHtml += '</div></div>';
                            divHtml += '<div class="panel-body left">';
                            divHtml += '<div class="image-container">';
                            divHtml += '<img alt="post image" src="' + data[i].image + '" class="post-image img-responsive" /></div>';
                            divHtml += '<div class="control-group">';
                            divHtml += '<button class="btn btn-link" title="Like">';
                            divHtml += '<img class="link-image" alt="Like" src="../../Content/template/img/thumb32x32.png" />';
                            divHtml += '<span>' + data[i].likes + '</span>';
                            divHtml += '</button>';
                            divHtml += '<button class="btn btn-link" title="Dislike">';
                            divHtml += '<img class="link-image" alt="Dislike" src="../../Content/template/img/thumb-unlike32x32.png" />';
                            divHtml += '<span>' + data[i].dislikes + '</span></button>';
                            divHtml += '<button class="btn btn-link" title="Comment">';
                            divHtml += '<img class="link-image" alt="Comment" src="../../Content/template/img/comments32x32.png" />';
                            divHtml += '<span>' + data[i].comments + '</span>';
                            divHtml += '</button></div></div></div></div>';
                        }
                    });

                    listContiner.append(divHtml);
                }
            });
        }
    });
});