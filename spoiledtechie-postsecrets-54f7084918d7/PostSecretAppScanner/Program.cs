﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace PostSecretAppScanner
{
    class Program
    {
        Regex guid = new Regex("[a-zA-Z0-9].jpg", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly string site = "http://postsecret.s3.amazonaws.com/";
        private static readonly string extension = ".jpg";
        static void Main(string[] args)
        {
            Console.WriteLine("Guid to start");
            string start = Console.ReadLine();
            //http://postsecret.s3.amazonaws.com/de07d891135afe0df49aba1deba15c03.jpg

            string iteration = start;

            //using (StreamWriter outfile = new StreamWriter(@"c:\temp\postsecret\guids.txt"))
            //{
                Parallel.For(0, Int64.MaxValue, i =>
                {
                    try
                    {
                        //for (Double k = 0; k < 5316911983139663491615228241121400000d; k++)
                        //{
                        Console.WriteLine(site + iteration + extension);
                        //outfile.WriteLine(iteration);
                        iteration = NextLetter(iteration);

                        try
                        {
                            WebClient client = new WebClient();
                            client.DownloadFile(site + iteration + extension, "c:/temp/postsecret/" + iteration + ".jpg");
                        }
                        catch (Exception e)
                        {
                            //outfile.WriteLine(e.Message);
                            Console.WriteLine(e.Message);
                        }
                    }catch{}
                    //do some work.
                    //}
                });
                //outfile.Close();
                Console.ReadLine();
           // }
        }

        private static readonly string alphabet = "abcdef0123456789";
        /// <summary>
        /// Return an incremented alphabtical string
        /// </summary>
        /// <param name="letter">The string to be incremented</param>
        /// <returns>the incremented string</returns>
        public static string NextLetter(string letter)
        {
            char lastLetterInString = letter[letter.Length - 1];

            // if the last letter in the string is the last letter of the alphabet
            if (alphabet.IndexOf(lastLetterInString) == alphabet.Length - 1)
            {
                //replace the last letter in the string with the first leter of the alphbat and get the next letter for the rest of the string
                return NextLetter(letter.Substring(0, letter.Length - 1)) + alphabet[0];
            }
            else
            {
                // replace the last letter in the string with the proceeding letter of the alphabet
                return letter.Remove(letter.Length - 1).Insert(letter.Length - 1, (alphabet[alphabet.IndexOf(letter[letter.Length - 1]) + 1]).ToString());
            }
        }
    }
}
